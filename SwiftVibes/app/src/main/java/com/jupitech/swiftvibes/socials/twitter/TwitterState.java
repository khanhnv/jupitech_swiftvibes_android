package com.jupitech.swiftvibes.socials.twitter;

import android.app.Activity;

public interface TwitterState {
	
	public void onCreate(Activity activity);
	
	public void onResume();
	
}
