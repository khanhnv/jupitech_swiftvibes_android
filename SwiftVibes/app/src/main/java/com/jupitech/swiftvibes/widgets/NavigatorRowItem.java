package com.jupitech.swiftvibes.widgets;

import android.content.Context;
import android.widget.ImageView;

import com.jupitech.swiftvibes.R;


public class NavigatorRowItem extends BaseLinearLayout {

	public ImageView mIcon;
	public CustomTextView mNameText;

	public NavigatorRowItem(Context context) {
		super(context);

		initLayout(context, R.layout.item_navigator);

		mIcon = (ImageView) findViewById(R.id.icon);

		mNameText = (CustomTextView) findViewById(R.id.name_text);
	}

}
