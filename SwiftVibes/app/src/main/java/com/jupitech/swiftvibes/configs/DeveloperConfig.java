package com.jupitech.swiftvibes.configs;

public class DeveloperConfig {

	/*
	 * used to set developer mode.
	 */
	public static final boolean DEVELOPER_MODE = false;

	/*
	 * used to set developer mode. Enable strict mode
	 */
	public static final boolean DEVELOPER_ENABLE_STRICT_MODE = true;

	/*
	 * used to enable send log crash
	 */
	public static final boolean DEVELOPER_ENABLE_SEND_LOG_CRASH = false;
}
