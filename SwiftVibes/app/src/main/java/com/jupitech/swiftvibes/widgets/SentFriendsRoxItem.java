package com.jupitech.swiftvibes.widgets;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.jupitech.swiftvibes.R;

/**
 * Created by brucenguyen on 10/14/14.
 */
public class SentFriendsRoxItem extends BaseLinearLayout{

    public TextView mNameText;
    public ImageView mCheckImage;

    public SentFriendsRoxItem(Context context) {
        super(context);
        initLayout(context, R.layout.item_sent_friends);

        mNameText = (TextView)findViewById(R.id.name_text);

        mCheckImage = (ImageView)findViewById(R.id.check_image);
    }
}
