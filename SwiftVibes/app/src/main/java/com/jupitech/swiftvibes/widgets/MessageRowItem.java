package com.jupitech.swiftvibes.widgets;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jupitech.swiftvibes.R;

/**
 * Created by brucenguyen on 10/17/14.
 */
public class MessageRowItem extends BaseLinearLayout {

    public ImageView mThumbImage;
    public ImageView mStatusImage;
    public TextView mMeText;
    public TextView mNameText;
    public TextView mMessageText;
    public LinearLayout mContainer;


    public MessageRowItem(Context context) {
        super(context);

        initLayout(context, R.layout.item_message);


        mThumbImage = (ImageView) findViewById(R.id.image_view);
        mStatusImage = (ImageView) findViewById(R.id.status_image);

        mMeText = (TextView) findViewById(R.id.me_text);
        mNameText = (TextView) findViewById(R.id.name_text);
        mMessageText = (TextView) findViewById(R.id.message_text);

        mContainer = (LinearLayout)findViewById(R.id.container);

    }
}
