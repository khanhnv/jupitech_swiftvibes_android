package com.jupitech.swiftvibes.listeners;

import com.jupitech.swiftvibes.models.PostModel;

/**
 * Created by brucenguyen on 10/21/14.
 */
public interface OnItemLongClick {

    public void onItemLongClick(int position, PostModel post);
}
