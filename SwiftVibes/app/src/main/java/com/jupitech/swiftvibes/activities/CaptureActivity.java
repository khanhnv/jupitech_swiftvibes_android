package com.jupitech.swiftvibes.activities;

import android.os.Bundle;

import com.jupitech.swiftvibes.AppApplication;
import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.base.BaseActionBarActivity;
import com.jupitech.swiftvibes.activities.fragments.capture.CaptureFragment;
import com.jupitech.swiftvibes.services.AppIntentService;

public class CaptureActivity extends BaseActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);

        initModels();
        initViews();

        switchContent(R.id.container, new CaptureFragment());

        AppIntentService.startActionFetchFriend(AppApplication.get());
    }

    @Override
    protected void initModels() {

    }

    @Override
    protected void initViews() {

    }


}
