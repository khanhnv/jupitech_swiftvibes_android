package com.jupitech.swiftvibes.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by brucenguyen on 10/16/14.
 */
public class PostModel implements Serializable{

    @SerializedName("message_id")
    private String messageId;

    @SerializedName("from_id")
    private String fromId;

    @SerializedName("message")
    private String message;

    @SerializedName("created_on")
    private String createdOn;

    @SerializedName("code")
    private String code;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("status")
    private String status;

    @SerializedName("received_id")
    private String receivedId;

    @SerializedName("from_name")
    private String fromName;

    @SerializedName("to_name")
    private String toName;

    @SerializedName("last_comment_id")
    private String lastCommentId;

    @SerializedName("by_me")
    private String byMe;

    private boolean isMe;
    private boolean isHeard;
    private boolean isNotOpen;


    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReceivedId() {
        return receivedId;
    }

    public void setReceivedId(String receivedId) {
        this.receivedId = receivedId;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getLastCommentId() {
        return lastCommentId;
    }

    public void setLastCommentId(String lastCommentId) {
        this.lastCommentId = lastCommentId;
    }

    public String getByMe() {
        return byMe;
    }

    public void setByMe(String byMe) {
        this.byMe = byMe;
    }

    public boolean isMe() {
        return isMe;
    }

    public void setMe(boolean isMe) {
        this.isMe = isMe;
    }

    public boolean isHeard() {
        return isHeard;
    }

    public void setHeard(boolean isHeard) {
        this.isHeard = isHeard;
    }

    public boolean isNotOpen() {
        return isNotOpen;
    }

    public void setNotOpen(boolean isNotOpen) {
        this.isNotOpen = isNotOpen;
    }

    @Override
    public String toString() {
        return "PostModel{" +
                "messageId='" + messageId + '\'' +
                ", fromId='" + fromId + '\'' +
                ", message='" + message + '\'' +
                ", createdOn='" + createdOn + '\'' +
                ", code='" + code + '\'' +
                ", userId='" + userId + '\'' +
                ", status='" + status + '\'' +
                ", receivedId='" + receivedId + '\'' +
                ", fromName='" + fromName + '\'' +
                ", toName='" + toName + '\'' +
                ", lastCommentId='" + lastCommentId + '\'' +
                ", byMe='" + byMe + '\'' +
                ", isMe=" + isMe +
                ", isHeard=" + isHeard +
                ", isNotOpen=" + isNotOpen +
                '}';
    }
}
