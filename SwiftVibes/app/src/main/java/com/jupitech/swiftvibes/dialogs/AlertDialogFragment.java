package com.jupitech.swiftvibes.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.listeners.AlertListener;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.widgets.CustomTextView;


public class AlertDialogFragment extends DialogFragment {

    private static final String EXTRA_TITLE = "title";
    private static final String EXTRA_MESSAGE = "message";
    private static final String EXTRA_LABEL_LEFT = "label_left";
    private static final String EXTRA_LABEL_RIGHT = "label_right";

    private boolean mShowOnlyOneButton = false;

    private AlertListener mAlertListener;

    public AlertListener getAlertListener() {
        return mAlertListener;
    }

    public void setAlertListener(AlertListener mAlertListener) {
        this.mAlertListener = mAlertListener;
    }

    public AlertDialogFragment() {
    }

    public static AlertDialogFragment newInstance(String title, String message,
                                                  String lableLeftButton, String lableRightButton) {
        AlertDialogFragment alertDialog = new AlertDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_MESSAGE, message);
        bundle.putString(EXTRA_TITLE, title);
        bundle.putString(EXTRA_LABEL_LEFT, lableLeftButton);
        bundle.putString(EXTRA_LABEL_RIGHT, lableRightButton);
        alertDialog.setArguments(bundle);
        alertDialog.setAlertListener(null);
        return alertDialog;
    }

    public static AlertDialogFragment newInstance(String title, String message,
                                                  String lableLeftButton, String lableRightButton,
                                                  AlertListener listener) {
        AlertDialogFragment alertDialog = new AlertDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_MESSAGE, message);
        bundle.putString(EXTRA_TITLE, title);
        bundle.putString(EXTRA_LABEL_LEFT, lableLeftButton);
        bundle.putString(EXTRA_LABEL_RIGHT, lableRightButton);
        alertDialog.setArguments(bundle);
        alertDialog.setAlertListener(listener);
        return alertDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        final String message = bundle.getString(EXTRA_MESSAGE);
        final String title = bundle.getString(EXTRA_TITLE);
        final String lableLeft = bundle.getString(EXTRA_LABEL_LEFT);
        final String lableRight = bundle.getString(EXTRA_LABEL_RIGHT);

        final Dialog dialog = new Dialog(getActivity(),
                R.style.CustomDialogTheme);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);

        View view = LayoutInflater.from(getActivity()).inflate(
                R.layout.dialog_fragment, null);

        CustomTextView titleText = (CustomTextView) view
                .findViewById(R.id.title_text);
        titleText.setText(title);

        CustomTextView messageText = (CustomTextView) view
                .findViewById(R.id.message_text);
        messageText.setText(message);

        CustomTextView leftText = (CustomTextView) view
                .findViewById(R.id.left_text);
        leftText.setText(lableLeft);
        leftText.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mAlertListener != null) {
                    mAlertListener.onLeftClick(dialog);
                } else {
                    dialog.dismiss();
                }
            }
        });

        CustomTextView rightText = (CustomTextView) view
                .findViewById(R.id.right_text);
        rightText.setText(lableRight);
        rightText.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mAlertListener != null) {
                    mAlertListener.onRightClick(dialog);
                } else {
                    dialog.dismiss();
                }
            }
        });

		/*
		 * set dialog with one button default is hide right button
		 */
        if (mShowOnlyOneButton) {
            rightText.setVisibility(View.VISIBLE);
            leftText.setVisibility(View.GONE);
            LinearLayout.LayoutParams oldParams = (LinearLayout.LayoutParams) rightText
                    .getLayoutParams();
            LinearLayout.LayoutParams newParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, oldParams.height);
            rightText.setLayoutParams(newParams);
        }
        dialog.setContentView(view);
        return dialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof AlertListener) {
            try {
                mAlertListener = (AlertListener) activity;
            } catch (ClassCastException ce) {
                Log.e(activity.toString() + " must implement AlertListener");
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mAlertListener = null;
    }

    public void showOnlyOneButton(boolean showOnlyOneButton) {
        this.mShowOnlyOneButton = showOnlyOneButton;
    }

}
