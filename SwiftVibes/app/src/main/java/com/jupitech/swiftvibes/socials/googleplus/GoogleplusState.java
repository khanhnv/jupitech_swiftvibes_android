package com.jupitech.swiftvibes.socials.googleplus;

import android.content.Intent;

public interface GoogleplusState {

	void onCreate();

	void onStart();

	void onStop();
	
	void onActivityResult(int requestCode, int resultCode, Intent data);
}
