package com.jupitech.swiftvibes.utils.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;

import com.jupitech.swiftvibes.utils.Log;


public class BitmapLargeManager {

    private static BitmapLargeManager instance;
    private Context mContext;
    private LruCache<Integer, Bitmap> mMemoryCache; // key is resource id
    private AsyncTask<Integer, Void, Boolean> mLoadBitmapTask;

    public BitmapLargeManager(Context context) {
        this.mContext = context;
        initMemoryCache();
    }

    public static BitmapLargeManager getInstance(Context context) {
        if (instance == null) {
            instance = new BitmapLargeManager(context);
        }
        return instance;
    }

    private void initMemoryCache() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        Log.d("maxMemory = " + maxMemory);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<Integer, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(Integer key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getRowBytes() * bitmap.getHeight() / 1024;
            }
        };
    }

    /**
     * Load bitmap from cache
     *
     * @param resId resource id of bitmap
     * @return
     */
    public Bitmap getBitmapFromCache(int resId) {
        return mMemoryCache.get(resId);
    }

    /**
     * Remove bitmap from cache
     *
     * @param resId rersource id of bitmap
     */
    public void removeBitmap(int resId) {
        if (mMemoryCache.get(resId) != null) {
            mMemoryCache.remove(resId);
            System.gc();
        }
    }

    /**
     * Load large bitmap
     *
     * @param resId    resource id of bitmap
     * @param listener called when load bitmap finish
     */
    public void loadLargeBimap(final int resId, final int width,
                               final int height, final LoadBitmapListener listener) {

        if (mLoadBitmapTask != null) {
            Log.e("Loading other bitmap...");
            return;
        }
        mLoadBitmapTask = new AsyncTask<Integer, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Integer... params) {
                try {
                    Bitmap largeBitmap;
                    int size = params.length;
                    for (int i = 0; i < size; i++) {
                        if (mMemoryCache.get(params[i]) != null) {
                            // exist bitmap in cache
                            continue;
                        }
                        largeBitmap = BitmapUtil
                                .decodeBitmapFromResource(
                                        mContext.getResources(), params[i],
                                        width, height);
                        mMemoryCache.put(params[i], largeBitmap);
                    }

                } catch (OutOfMemoryError e) {
                    Log.e(e.toString());
                    return false;
                } catch (Exception e) {
                    Log.e(e.toString());
                    return false;
                }
                return true;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                mLoadBitmapTask = null;
                if (listener != null) {
                    listener.onLoadFinish(resId, result);
                }
            }

        };
        mLoadBitmapTask.execute(resId);
    }

    /**
     * Called when finish load large bitmap
     */
    public interface LoadBitmapListener {
        void onLoadFinish(int id, boolean isSuccess);
    }
}