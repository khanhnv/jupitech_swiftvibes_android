package com.jupitech.swiftvibes.providers.calllog;

public class MyCallLog {

	private long duration, date;
	private String phoneNumber, cachedName;
	private int type, cachedNumberType;

	public MyCallLog() {
		super();
	}

	public MyCallLog(long duration, long date, String phoneNumber, String cachedName, int type, int cachedNumberType) {
		super();
		this.duration = duration;
		this.date = date;
		this.phoneNumber = phoneNumber;
		this.cachedName = cachedName;
		this.type = type;
		this.cachedNumberType = cachedNumberType;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCachedName() {
		return cachedName;
	}

	public void setCachedName(String cachedName) {
		this.cachedName = cachedName;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getCachedNumberType() {
		return cachedNumberType;
	}

	public void setCachedNumberType(int cachedNumberType) {
		this.cachedNumberType = cachedNumberType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cachedName == null) ? 0 : cachedName.hashCode());
		result = prime * result + cachedNumberType;
		result = prime * result + (int) (date ^ (date >>> 32));
		result = prime * result + (int) (duration ^ (duration >>> 32));
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + type;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		MyCallLog other = (MyCallLog) obj;
		if (cachedName == null) {
			if (other.cachedName != null) return false;
		} else if (!cachedName.equals(other.cachedName)) return false;
		if (cachedNumberType != other.cachedNumberType) return false;
		if (date != other.date) return false;
		if (duration != other.duration) return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null) return false;
		} else if (!phoneNumber.equals(other.phoneNumber)) return false;
		if (type != other.type) return false;
		return true;
	}

	@Override
	public String toString() {
		return "MyCallLog [duration=" + duration + ", date=" + date + ", phoneNumber=" + phoneNumber + ", cachedName="
				+ cachedName + ", type=" + type + ", cachedNumberType=" + cachedNumberType + "]";
	}

}
