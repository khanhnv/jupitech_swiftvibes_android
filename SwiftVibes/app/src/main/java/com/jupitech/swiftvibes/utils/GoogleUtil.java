package com.jupitech.swiftvibes.utils;

import java.util.UUID;

public class GoogleUtil {


    /**
     * Random generate purchase string to verify when purchase
     *
     * @return
     */
    public static String generatePurchaseString() {
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }
}
