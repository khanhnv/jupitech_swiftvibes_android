package com.jupitech.swiftvibes.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by brucenguyen on 10/17/14.
 */
public class CommentModel {

    @SerializedName("comment_id")
    private String commentId;

    @SerializedName("message_id")
    private String messageId;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("url")
    private String url;

    @SerializedName("received_id")
    private String receivedId;

    @SerializedName("code")
    private String code;

    @SerializedName("created_on")
    private String createdOn;

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getReceivedId() {
        return receivedId;
    }

    public void setReceivedId(String receivedId) {
        this.receivedId = receivedId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        return "CommentModel{" +
                "commentId='" + commentId + '\'' +
                ", messageId='" + messageId + '\'' +
                ", userId='" + userId + '\'' +
                ", url='" + url + '\'' +
                ", receivedId='" + receivedId + '\'' +
                ", code='" + code + '\'' +
                ", createdOn='" + createdOn + '\'' +
                '}';
    }
}
