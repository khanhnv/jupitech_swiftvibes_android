package com.jupitech.swiftvibes.activities;

import android.os.Bundle;
import android.view.View;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.base.BaseActionBarActivity;

public class FriendActivity extends BaseActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend);
        initModels();
        initViews();
    }

    @Override
    protected void initModels() {

    }

    @Override
    protected void initViews() {
        initActionBar();

        setBackgroundRightImage(0);
        setBackgroundLeftImage(R.drawable.btn_back);
        mLeftImage.setOnClickListener(this);
        setVisibleRightImage(false);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch(v.getId()){
            case R.id.left_image:
                finish();
                break;
        }
    }
}
