package com.jupitech.swiftvibes.activities;


import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.base.BaseActionBarActivity;
import com.jupitech.swiftvibes.activities.fragments.contact.ContactFragment;
import com.jupitech.swiftvibes.activities.fragments.friends.FriendsFragment;
import com.jupitech.swiftvibes.activities.fragments.home.HeardFragment;
import com.jupitech.swiftvibes.activities.fragments.home.HomeFragment;
import com.jupitech.swiftvibes.activities.fragments.home.NavigationDrawerFragment;
import com.jupitech.swiftvibes.activities.fragments.home.RecordFragment;
import com.jupitech.swiftvibes.activities.fragments.tutorial.TutorialFragment;
import com.jupitech.swiftvibes.configs.Key;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.utils.intent.LaunchIntent;


public class NavigationActivity extends BaseActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    private CharSequence mTitle;

    FrameLayout mContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        initModels();
        initViews();
    }

    @Override
    protected void initModels() {

    }

    @Override
    protected void initViews() {
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        findViewById(R.id.left_image).setOnClickListener(this);
        findViewById(R.id.right_image).setOnClickListener(this);

        mContainer = (FrameLayout) findViewById(R.id.container);
        mContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Log.d("Down.....");
                        return true;
                    case MotionEvent.ACTION_UP:
                        Log.d("Up.....");
                        HeardFragment heardFragment = (HeardFragment) mFragmentManager.findFragmentByTag(Key.FRAGMENT_HEARD);
                        if (heardFragment != null) {
                            heardFragment.finishFragment();
//                            switchContent(R.id.container, new HomeFragment());
                        }

                        RecordFragment recordFragment = (RecordFragment) mFragmentManager.findFragmentByTag(Key.FRAGMENT_RECORD);
                        if (recordFragment != null) {
                            recordFragment.finishFragment();
//                            switchContent(R.id.container, new HomeFragment());
                        }
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        Log.d("MOVE.....");
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments

        switch (position) {
            case 0:
                switchContent(R.id.container, new HomeFragment(), true);
                break;
            case 1:
                switchContent(R.id.container, new FriendsFragment(), true);
                break;
//            case 1:
//                switchContent(R.id.container, new SettingFragment());
//                break;
            case 2:
                switchContent(R.id.container, new TutorialFragment(), true);
                break;
            case 3:
                LaunchIntent.start(this, CaptureActivity.class);
                break;
            case 4:
                switchContent(R.id.container, new ContactFragment(), true);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.left_image:
                mNavigationDrawerFragment.openMenuLeft();
                break;
            case R.id.right_image:
                LaunchIntent.start(this, CaptureActivity.class);
                overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
                break;
        }
    }
}
