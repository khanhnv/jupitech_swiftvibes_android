package com.jupitech.swiftvibes.activities.fragments.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jupitech.swiftvibes.BusProvider;
import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.apis.loopj.RestClient;
import com.jupitech.swiftvibes.apis.parse.JSONConvert;
import com.jupitech.swiftvibes.configs.ApiConfig;
import com.jupitech.swiftvibes.models.CommentModel;
import com.jupitech.swiftvibes.models.NotificationEvent;
import com.jupitech.swiftvibes.models.PostModel;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.utils.ToastUtil;
import com.jupitech.swiftvibes.utils.media.PlayBack;
import com.loopj.android.http.TextHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.otto.Subscribe;

import org.apache.http.Header;

public class HeardFragment extends BaseFragment {

    private ImageView mImageView;
    private ImageView mHeardImage;

    private static final String ARG_POST = "post";
    PostModel mPostModel;

    public static HeardFragment newInstance(PostModel post) {
        HeardFragment fragment = new HeardFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_POST, post);
        fragment.setArguments(args);
        return fragment;
    }

    public HeardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPostModel = (PostModel) getArguments().getSerializable(ARG_POST);
        }

        initModels();
    }

    @Override
    protected void initModels() {
        mPostModel = mApplication.getPost();
    }

    @Override
    protected void initViews(View view) {

        mHeardImage = (ImageView) view.findViewById(R.id.heard_image);
        mHeardImage.setOnClickListener(this);

        mImageView = (ImageView) view.findViewById(R.id.image_view);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_heard, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);

        displayContent();
    }

    @Override
    public void onPause() {
        super.onPause();

        BusProvider.getInstance().unregister(this);
        stopAudio();
    }

    @Override
    public void onClick(View v) {

    }


    public void displayContent() {

        String imageUrl = String.format(ApiConfig.GET_IMAGE, mPostModel.getCode());
        ImageLoader.getInstance().displayImage(imageUrl, mImageView);

        String receiveId = mPostModel.getReceivedId();

        RestClient.getComment(receiveId, new TextHttpResponseHandler() {
            @Override
            public void onFailure(String responseBody, Throwable error) {
                super.onFailure(responseBody, error);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                CommentModel commentModel = JSONConvert.parseCommentResponse(responseBody);
                if (commentModel != null) {
                    playAudio(commentModel);
                } else {
                    ToastUtil.show("Could not loaded audio. Please try again!");
                    finishFragment();
                }
            }
        });


    }

    public void playAudio(CommentModel commentModel) {
        String path = String.format(ApiConfig.GET_AUDIO, commentModel.getCode());
        Log.d("AudioPath: " + path);
        PlayBack.getInstance().prepare(path);
        PlayBack.getInstance().play();
    }

    public void stopAudio() {
        PlayBack.getInstance().release();
    }


    @Subscribe
    public void onNotificationEvent(NotificationEvent event) {
        finishFragment();
    }
}
