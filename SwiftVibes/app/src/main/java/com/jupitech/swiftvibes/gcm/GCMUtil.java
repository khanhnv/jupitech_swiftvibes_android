package com.jupitech.swiftvibes.gcm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

public class GCMUtil {

	public static final int MAX_ATTEMPTS = 5;
	public static final int BACKOFF_MILLI_SECONDS = 500;

	/**
	 * Intent used to handle registration broadcast result.
	 */
	public static final String ACTION_BROADCAST_REGISTRATION_RESULT = "ACTION_REGISTRATION_RESULT";

	/**
	 * Intent used to handle unregister broadcast result.
	 */
	public static final String ACTION_BROADCAST_UNREGISTRATION_RESULT = "ACTION_UNREGISTRATION_RESULT";

	/**
	 * Intent used to broadcast a message.
	 */
	public static final String ACTION_BROADCAST_MESSAGE = "ACTION_DISPLAY_MESSAGE";

	/**
	 * Intent's extra that contains the registration result information.
	 */
	public static final String EXTRA_REGISTRATION_SUCCESS = "REGISTRATION_SUCCESS";
	public static final String EXTRA_REGISTRATION_TYPE = "REGISTRATION_TYPE";
	public static final String EXTRA_REGISTRATION_CONTENT = "REGISTRATION_CONTENT";
	public static final String EXTRA_MESSAGE = "message";

	/** Registration types */
	public static final int TYPE_REGISTER = 0;

	/** server error reason */
	public static final int TYPE_UNREGISTER = 1;

	/** server error reason */
	public static final int TYPE_REGISTER_UNREGISTER_ERROR = 2;

	/** Registration error codes. */
	public static final String ERR_SERVICE_NOT_AVAILABLE = "SERVICE_NOT_AVAILABLE";
	public static final String ERR_ACCOUNT_MISSING = "ACCOUNT_MISSING";
	public static final String ERR_AUTHENTICATION_FAILED = "AUTHENTICATION_FAILED";
	public static final String ERR_TOO_MANY_REGISTRATIONS = "TOO_MANY_REGISTRATIONS";
	public static final String ERR_INVALID_SENDER = "INVALID_SENDER";
	public static final String ERR_PHONE_REGISTRATION_ERROR = "PHONE_REGISTRATION_ERROR";
	public static final String ERR_INVALID_PARAMETERS = "INVALID_PARAMETERS";
	public static final String ERR_REGISTRATION_LIMIT = "REGISTRATION_LIMIT";

	/**
	 * Send broadcast registration result
	 * 
	 * @param context
	 * @param type
	 * @param isSuccess
	 * @param content
	 *            : registration id if isSuccess is true, error code if
	 *            isSuccess is false
	 */
	public static void broadcastRegistrationResult(Context context, int type,
			boolean isSuccess, String content) {
		Intent intent = new Intent(ACTION_BROADCAST_REGISTRATION_RESULT);
		intent.putExtra(EXTRA_REGISTRATION_TYPE, type);
		intent.putExtra(EXTRA_REGISTRATION_SUCCESS, isSuccess);
		intent.putExtra(EXTRA_REGISTRATION_CONTENT, content);
		LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
		// context.sendBroadcast(intent);
	}

	/**
	 * Send broadcast unregister result
	 * 
	 * @param context
	 */
	public static void broadcastUnRegistratoinResult(Context context) {
		Intent intent = new Intent(ACTION_BROADCAST_UNREGISTRATION_RESULT);
		LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
		// context.sendBroadcast(intent);
	}

	/**
	 * Notifies UI to display a message.
	 * <p>
	 * This method is defined in the common helper because it's used both by the
	 * UI and the background service.
	 * 
	 * @param context
	 *            application's context.
	 * @param message
	 *            message to be displayed.
	 */
	public static void broadcastMessage(Context context, String message) {
		Intent intent = new Intent(ACTION_BROADCAST_MESSAGE);
		intent.putExtra(EXTRA_MESSAGE, message);
		LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
		// context.sendBroadcast(intent);
	}

	/**
	 * 
	 * @param context
	 * @param extras
	 */
	public static void broadcastMessage(Context context, Bundle extras) {
		Intent intent = new Intent(ACTION_BROADCAST_MESSAGE);
		intent.putExtras(extras);
		LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
	}

	/**
	 * Function to process pushed message from GCM
	 * 
	 * @param bundle
	 */
	public static void processGcmMessage(Context context, Bundle bundle) {

		broadcastMessage(context, bundle);
	}
}
