package com.jupitech.swiftvibes.models;

public class LocationInfoModel {

	private String lat;
	private String lng;
	private String country;

	public LocationInfoModel() {
		this.lat = "";
		this.lng = "";
		this.country = "";
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getCountry() {
		if (country.equals("")) {
			country = "Unknow";
		}
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
