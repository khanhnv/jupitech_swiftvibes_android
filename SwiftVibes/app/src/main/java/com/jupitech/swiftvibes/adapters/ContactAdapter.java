package com.jupitech.swiftvibes.adapters;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.models.ContactModel;
import com.jupitech.swiftvibes.widgets.CustomTextView;
import com.jupitech.swiftvibes.widgets.NavigatorRowItem;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

public class ContactAdapter extends ArrayAdapter<ContactModel> {

	public ContactAdapter(Context context, int resource,
                          List<ContactModel> objects) {
		super(context, resource, objects);
	}

    public ContactAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ContactModel item = getItem(position);
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = new NavigatorRowItem(getContext());

			holder = new ViewHolder();

			holder.mIcon = ((NavigatorRowItem) convertView).mIcon;
			holder.mNameText = ((NavigatorRowItem) convertView).mNameText;

			convertView.setTag(holder);

		} else {

			holder = (ViewHolder) convertView.getTag();
		}

        ;
        Bitmap bitmap = BitmapFactory.decodeStream(openPhoto(Long.parseLong(item.getContactId())));
        if(bitmap != null){
            holder.mIcon.setImageBitmap(bitmap);
        }else{
            holder.mIcon.setImageResource(R.drawable.ic_launcher);
        }
		holder.mNameText.setText(item.getContactName());

		return convertView;
	}

	static class ViewHolder {
		public ImageView mIcon;
		public CustomTextView mNameText;
	}

    public InputStream openPhoto(long contactId) {
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
        Cursor cursor = getContext().getContentResolver().query(photoUri,
                new String[]{ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
        if (cursor == null) {
            return null;
        }
        try {
            if (cursor.moveToFirst()) {
                byte[] data = cursor.getBlob(0);
                if (data != null) {
                    return new ByteArrayInputStream(data);
                }
            }
        } finally {
            cursor.close();
        }
        return null;
    }

}
