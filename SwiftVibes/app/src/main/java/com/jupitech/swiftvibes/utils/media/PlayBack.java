package com.jupitech.swiftvibes.utils.media;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;

import com.jupitech.swiftvibes.AppApplication;
import com.jupitech.swiftvibes.BusProvider;
import com.jupitech.swiftvibes.models.NotificationEvent;

import java.io.IOException;

/**
 * Created by brucenguyen on 8/25/14.
 */
public class PlayBack {

    public static final String kNotificationPlayingHasStopped = "kNotificationPlayingHasStopped";
    public static final String kKeyPlayingTime = "playingTime";
    public static final String kKeyTotalTime = "TotalTime";

    private static PlayBack instance;

    private MediaPlayer mediaPlayer;

    private boolean isPrepare = false;
    private int playingCurrentTime;
    private int playingDuration;

//    private Timer playingTimer;

    private String playbackFile;

    public PlayBack() {
//        mediaPlayer = new MediaPlayer();
//        mediaPlayer.setOnPreparedListener(preparedListener);
//        mediaPlayer.setOnCompletionListener(completionListener);
//        mediaPlayer.setOnErrorListener(errorListener);
    }

    OnCompletionListener completionListener = new OnCompletionListener() {

        @Override
        public void onCompletion(MediaPlayer mp) {
            stop();
        }
    };

    OnErrorListener errorListener = new OnErrorListener() {

        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
//			stop();
            return true;
        }
    };

    OnPreparedListener preparedListener = new OnPreparedListener() {

        @Override
        public void onPrepared(MediaPlayer mp) {

        }
    };

    public static PlayBack getInstance() {
        if (instance == null) {
            instance = new PlayBack();
        }
        return instance;
    }

    public int getPlayingCurrentTime() {
        return playingCurrentTime;
    }

    public int getPlayingDuration() {
        return playingDuration;
    }

    public void setPlayingDuration(int playingDuration) {
        this.playingDuration = playingDuration;
    }

    public String getPlaybackFile() {
        return playbackFile;
    }

    public void setPlaybackFile(String playbackFile) {
        this.playbackFile = playbackFile;
    }

    public void prepare(String path) {
        isPrepare = true;
        playingCurrentTime = 0;
        playbackFile = path;

//        if (mediaPlayer == null) {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnPreparedListener(preparedListener);
        mediaPlayer.setOnCompletionListener(completionListener);
        mediaPlayer.setOnErrorListener(errorListener);
//        }

//        mediaPlayer.reset();
        try {
            Uri uri = Uri.parse(path);
            mediaPlayer.setDataSource(AppApplication.get(), uri);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void unPrepare() {
//        if (playingTimer != null) {
//            playingTimer.cancel();
//            playingTimer = null;
//        }

        try {
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                mediaPlayer.release();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        mediaPlayer = null;
        playbackFile = null;
    }

    public boolean isPlaying() {
        return mediaPlayer != null && mediaPlayer.isPlaying();
    }

    public boolean play() {
        if (mediaPlayer == null)
            return false;
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.seekTo(0);
            playingCurrentTime = 0;
            return true;
        }

//        playingTimer = new Timer();
//        playingTimer.schedule(new TimerTask() {
//
//            @Override
//            public void run() {
//                updatePlayingAt();
//            }
//        }, 0, 200);

        try {
            mediaPlayer.start();
            return true;
        } catch (IllegalStateException e) {
            return false;
        }
    }

    public void updatePlayingAt() {
        if (mediaPlayer == null || playingCurrentTime == mediaPlayer.getCurrentPosition()) {
            return;
        }
        if (mediaPlayer != null) {
            try {
                playingCurrentTime = mediaPlayer.getCurrentPosition();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }

    public void pause() {
        if (!isPlaying()) {
            mediaPlayer.start();
        } else {
            mediaPlayer.pause();
        }
    }

    public void stop() {
        try {
//            playingTimer.cancel();
//            playingTimer = null;
            int duration = 0;
            if (mediaPlayer != null) {
                duration = mediaPlayer.getDuration();

                playingCurrentTime = mediaPlayer.getCurrentPosition();
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
            }
            Bundle extras = new Bundle();
            extras.putInt(kKeyPlayingTime, playingCurrentTime);
            extras.putInt(kKeyTotalTime, duration);
            BusProvider.getInstance().post(
                    new NotificationEvent(kNotificationPlayingHasStopped,
                            extras));

            playbackFile = null;
            playingCurrentTime = 0;
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void release() {
        try {
            stop();
            mediaPlayer.release();
            isPrepare = false;
            mediaPlayer = null;
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public int getCurrentPosition() {
        return mediaPlayer.getCurrentPosition();
    }

    public int playingDuration() {
        if (mediaPlayer != null)
            return mediaPlayer.getDuration();
        return 0;
    }

    public boolean isPrepare() {
        return isPrepare;
    }

    public void setPlayingCurrentTime(int playingCurrentTime) {
        this.playingCurrentTime = playingCurrentTime;
        if (playingCurrentTime >= 0
                && playingCurrentTime <= mediaPlayer.getDuration()) {
            mediaPlayer.seekTo(playingCurrentTime);
        }
    }

    public void offsetCurrentTime(int offset) {
        if (offset < 0) {
            backForward(offset);
        } else {
            fastForward(offset);
        }
    }

    /**
     * Offset > 0
     *
     * @param offset
     */
    public void fastForward(int offset) {
        int current = getCurrentPosition();
        if (current + offset <= playingDuration()) {
            mediaPlayer.seekTo(current + offset);
        } else {
            mediaPlayer.seekTo(playingDuration());
        }
    }

    /**
     * offset < 0
     *
     * @param offset
     */
    public void backForward(int offset) {
        int current = getCurrentPosition();
        if (current + offset >= 0) {
            mediaPlayer.seekTo(current + offset);
        } else {
            mediaPlayer.seekTo(0);
        }
    }
}
