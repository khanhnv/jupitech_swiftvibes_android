package com.jupitech.swiftvibes.models;

import java.io.Serializable;

public class PlaceModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6963930717816943895L;
	public float lat;
	public float lng;
	public String icon;
	public String id;
	public String name;
	public String vicinity;

	public float getLat() {
		return lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}

	public float getLng() {
		return lng;
	}

	public void setLng(float lng) {
		this.lng = lng;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVicinity() {
		return vicinity;
	}

	public void setVicinity(String vicinity) {
		this.vicinity = vicinity;
	}

	@Override
	public String toString() {
		return "PlaceModel [lat=" + lat + ", lng=" + lng + ", icon=" + icon
				+ ", id=" + id + ", name=" + name + ", vicinity=" + vicinity
				+ "]";
	}

}
