package com.jupitech.swiftvibes.activities;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.base.BaseActionBarActivity;
import com.jupitech.swiftvibes.models.DeviceInfoModel;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.utils.hardware.DeviceUtil;
import com.jupitech.swiftvibes.utils.intent.LaunchIntent;


public class SplashActivity extends BaseActionBarActivity {

	CountDownTimer mCountDownTimer;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_splash);
		initModels();
		initViews();
	}

	@Override
	protected void onResume() {
		super.onResume();
		startCountDownTimer();
	}

	@Override
	protected void onPause() {
		super.onPause();
		stopCountDownTimer();
	}

	@Override
	public void onClick(View v) {
	}

	@Override
	protected void initModels() {
		DeviceInfoModel model = DeviceUtil.getDeviceInfo(mSelf);
		mPreferenceManager.setDeviceInfo(model);
        Log.d(model.toString());
        Log.d("DeviceID: " + DeviceUtil.getDeviceId(getApplicationContext()));
    }

	@Override
	protected void initViews() {
	}

	private void startCountDownTimer() {
		if (mCountDownTimer == null) {
			mCountDownTimer = new CountDownTimer(3000, 1000) {

				@Override
				public void onTick(long millisUntilFinished) {
				}

				@Override
				public void onFinish() {
					LaunchIntent.start(mSelf, TutorialActivity.class);
					finish();
                    overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
				}
			};
		}
		mCountDownTimer.start();
	}

	private void stopCountDownTimer() {
		if (mCountDownTimer != null) {
			mCountDownTimer.cancel();
			mCountDownTimer = null;
		}
	}

}
