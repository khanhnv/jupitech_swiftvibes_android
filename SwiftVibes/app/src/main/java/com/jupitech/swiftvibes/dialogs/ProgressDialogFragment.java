package com.jupitech.swiftvibes.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class ProgressDialogFragment extends DialogFragment {

	private static final String EXTRA_TITLE = "title";
	private static final String EXTRA_MESSAGE = "message";

	public static ProgressDialogFragment newInstance(String title,
			String message) {
		ProgressDialogFragment dialogFragment = new ProgressDialogFragment();
		Bundle bundle = new Bundle();
		bundle.putString(EXTRA_TITLE, title);
		bundle.putString(EXTRA_MESSAGE, message);
		dialogFragment.setArguments(bundle);
		return dialogFragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Bundle bundle = getArguments();
		String title = bundle.getString(EXTRA_TITLE);
		String message = bundle.getString(EXTRA_MESSAGE);
		ProgressDialog dialog = new ProgressDialog(getActivity());
		if (title != null)
			dialog.setTitle(title);
		if (message != null)
			dialog.setMessage(message);
		dialog.setCancelable(false);
		return dialog;
	}
}
