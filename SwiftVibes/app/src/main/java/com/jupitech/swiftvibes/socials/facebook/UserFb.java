package com.jupitech.swiftvibes.socials.facebook;

import com.google.gson.annotations.SerializedName;

public class UserFb {

	@SerializedName("id")
	private String id;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("username")
	private String userName;

	@SerializedName("timezone")
	private int timeZone;

	@SerializedName("email")
	private String email;

	@SerializedName("locale")
	private String locale;

	@SerializedName("link")
	private String link;

	@SerializedName("name")
	private String name;

	@SerializedName("gender")
	private String gender;

	@SerializedName("updated_time")
	private String updatedTime;
	
	@SerializedName("birthday")
	private String birthDay;

	public UserFb() {
		id = "";
		firstName = "";
		lastName = "";
		userName = "";
		timeZone = 0;
		email = "";
		locale = "";
		link = "";
		name = "";
		gender = "";
		updatedTime = "";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(int timeZone) {
		this.timeZone = timeZone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	@Override
	public String toString() {
		return "UserFb [id=" + id + ", firstName=" + firstName + ", lastName="
				+ lastName + ", userName=" + userName + ", timeZone="
				+ timeZone + ", email=" + email + ", locale=" + locale
				+ ", link=" + link + ", name=" + name + ", gender=" + gender
				+ ", updatedTime=" + updatedTime + ", birthDay=" + birthDay
				+ "]";
	}

	public static class FavoriteTeam {

		private String id;
		private String name;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return "FavoriteTeam [id=" + id + ", name=" + name + "]";
		}

	}

}
