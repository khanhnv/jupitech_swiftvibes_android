package com.jupitech.swiftvibes.providers.calllog;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.CallLog.Calls;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Call Log
 * 
 * @author dizaii
 * 
 */
public class CallLogFunction {

	private ContentResolver resolver;

	public CallLogFunction(Context context) {
		resolver = context.getContentResolver();
	}

	/**
	 * Get all call log
	 * 
	 * @return The array list of call log
	 */
	public ArrayList<MyCallLog> getCallLog() {
		MyCallLog callLog = null;
		ArrayList<MyCallLog> lstCallLog = new ArrayList<MyCallLog>();
		Cursor cursor = null;
		String projection[] = { Calls.NUMBER, // phone_number
				Calls.TYPE, // type of call (incoming, outgoing, missed
				Calls.CACHED_NAME, // name associated with phone
				Calls.CACHED_NUMBER_TYPE, // cached number type ( home,
				// work, ...)
				Calls.DATE, // date of call occured
				Calls.DURATION // duration of call
		};
		String order = Calls.DATE + " DESC";
		cursor = resolver.query(Calls.CONTENT_URI, projection, null, null, order);
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					callLog = new MyCallLog();
					callLog.setPhoneNumber(cursor.getString(cursor.getColumnIndex(Calls.NUMBER)));
					callLog.setType(cursor.getInt(cursor.getColumnIndex(Calls.TYPE)));
					callLog.setCachedName(cursor.getString(cursor.getColumnIndex(Calls.CACHED_NAME)));
					callLog.setCachedNumberType(cursor.getInt(cursor.getColumnIndex(Calls.CACHED_NUMBER_TYPE)));
					callLog.setDate(cursor.getLong(cursor.getColumnIndex(Calls.DATE)));
					callLog.setDuration(cursor.getLong(cursor.getColumnIndex(Calls.DURATION)));
					lstCallLog.add(callLog);
					callLog = null;
				} while (cursor.moveToNext());
			}
		}
		return lstCallLog;
	}

	public void insertToCallLog(MyCallLog callLog) {
		Calendar calendar = Calendar.getInstance();
		ContentValues values = new ContentValues();
		values.put(Calls.TYPE, callLog.getType());
		values.put(Calls.CACHED_NAME, callLog.getCachedName());
		values.put(Calls.NUMBER, callLog.getPhoneNumber());
		values.put(Calls.DATE, calendar.getTimeInMillis());
		values.put(Calls.DURATION, callLog.getDuration());
		values.put(Calls.CACHED_NUMBER_TYPE, 0);
		resolver.insert(Calls.CONTENT_URI, values);
	}

	public boolean deleteCallLog(String phoneNumber) {
		return resolver.delete(Calls.CONTENT_URI, Calls.NUMBER + " = ?", new String[] { phoneNumber }) > 0;
	}

	public boolean deleteLastCallLog() {
		Cursor cursor = null;
		boolean result = false;
		cursor = resolver.query(Calls.CONTENT_URI, null, null, null, Calls.DATE + " DESC");

		if (cursor != null) {
			if (cursor.moveToFirst()) {
				String id = cursor.getString(cursor.getColumnIndex(Calls._ID));
				int value = resolver.delete(Calls.CONTENT_URI, Calls._ID + " = ?", new String[] { id });
				result = value > 0 ? true : false;
			}
			cursor.close();
		}
		return result;
	}
}
