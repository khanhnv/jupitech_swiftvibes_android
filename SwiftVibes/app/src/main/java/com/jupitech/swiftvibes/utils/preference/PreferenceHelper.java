package com.jupitech.swiftvibes.utils.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Class use to manager shared preference use to in application
 *
 * @author khanhnv
 */
public class PreferenceHelper {

    private static PreferenceHelper instance;
    private SharedPreferences preferences;

    /**
     * Contractor
     *
     * @param context
     */
    private PreferenceHelper(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Use to initialization and get object MyPreferenceII
     *
     * @param context
     */
    public static PreferenceHelper getInstance(Context context) {
        if (instance == null) {
            instance = new PreferenceHelper(context);
        }

        return instance;
    }

    /**
     * Set an int value in the preferences editor
     *
     * @param key   : name of preference to modify
     * @param value : the new value of preference
     * @return : MyPreferenceII to continue edit
     */
    public PreferenceHelper putInt(String key, int value) {
        preferences.edit().putInt(key, value).commit();
        return instance;
    }

    /**
     * Retrieve an int value from the preferences.
     *
     * @param key          : the name of preference to retrieve
     * @param defaultValue : value to return if this preference does not exist
     * @return : return the preference value if it exist, else return default
     * value
     */
    public int getInt(String key, int defaultValue) {
        return preferences.getInt(key, defaultValue);
    }

    /**
     * Set a long value in preference editor
     *
     * @param key   : name of preference to modify
     * @param value : the new value of preference
     * @return : MyPreferenceII to continue edit
     */
    public PreferenceHelper putLong(String key, long value) {
        preferences.edit().putLong(key, value).commit();
        return instance;
    }

    /**
     * Retrieve an long value from the preferences
     *
     * @param key          : the name of preference to retrieve
     * @param defaultValue : value to return if this preference does not exist
     * @return : return the preference value if exist, else return default value
     */
    public long getLong(String key, long defaultValue) {
        return preferences.getLong(key, defaultValue);
    }

    /**
     * Set a String value in preference editor
     *
     * @param key   : name of preference to modify
     * @param value : the new value of preference
     * @return : return MyPreferenceII to continue edit
     */
    public PreferenceHelper putString(String key, String value) {
        preferences.edit().putString(key, value).commit();
        return instance;
    }

    /**
     * Retrieve a String value from the preference
     *
     * @param key          : the name of preference to retrieve
     * @param defaultValue : value to return if preference does not exist
     * @return : return the preference value if exist, else return default value
     */
    public String getString(String key, String defaultValue) {
        return preferences.getString(key, defaultValue);
    }

    /**
     * Set a boolean value in preference editor
     *
     * @param key   : name of preference to modify
     * @param value : the new value of preference
     * @return : return MyPreferenceII to continue edit
     */
    public PreferenceHelper putBoolean(String key, boolean value) {
        preferences.edit().putBoolean(key, value).commit();
        return instance;
    }

    /**
     * Retrieve a boolean value from the preferences
     *
     * @param key          : the name of preference to retrieve
     * @param defaultValue : value to return if preference does not exist
     * @return : return the preference value if exist, else return default value
     */
    public boolean getBoolean(String key, boolean defaultValue) {
        return preferences.getBoolean(key, defaultValue);
    }

    /**
     * Set a float value in preference editor
     *
     * @param key   : name of preference to modify
     * @param value : the new value of preference
     * @return : return MyPreferenceII to continue edit
     */
    public PreferenceHelper putFloat(String key, float value) {
        preferences.edit().putFloat(key, value).commit();
        return instance;
    }

    /**
     * Retrieve a float value from the preferences
     *
     * @param key          : the name of preference to retrieve
     * @param defaultValue : value to return if preference dose not exist
     * @return : return the preference value if exist, else return default value
     */
    public float getFloat(String key, float defaultValue) {
        return preferences.getFloat(key, defaultValue);
    }

    /**
     * clear all preferences
     */
    public void clear() {
        preferences.edit().clear().commit();
    }

}
