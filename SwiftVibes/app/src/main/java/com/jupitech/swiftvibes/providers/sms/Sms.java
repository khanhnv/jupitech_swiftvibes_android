package com.jupitech.swiftvibes.providers.sms;

import android.net.Uri;

public class Sms {

	private int type;
	private long threadId;
	private String address;
	private String personId;
	private String person;
	private long date;
	private boolean read;
	private boolean seen;
	private int status;
	private String subject;
	private String body;

	public Sms() {
		super();
		type = TextBasedSmsColumns.MESSAGE_TYPE_INBOX;
		threadId = -1;
		address = "Unknown";
		personId = "";
		person = "";
		date = System.currentTimeMillis();
		read = false;
		seen = false;
		status = TextBasedSmsColumns.STATUS_NONE;
		subject = "";
		body = "";
	}

	public Sms(int type, long threadId, String address, String personId,
			String person, long date, boolean read, boolean seen, int status,
			String subject, String body) {
		super();
		this.type = type;
		this.threadId = threadId;
		this.address = address;
		this.personId = personId;
		this.person = person;
		this.date = date;
		this.read = read;
		this.seen = seen;
		this.status = status;
		this.subject = subject;
		this.body = body;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the threadId
	 */
	public long getThreadId() {
		return threadId;
	}

	/**
	 * @param threadId
	 *            the threadId to set
	 */
	public void setThreadId(long threadId) {
		this.threadId = threadId;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the personId
	 */
	public String getPersonId() {
		return personId;
	}

	/**
	 * @param personId
	 *            the personId to set
	 */
	public void setPersonId(String personId) {
		this.personId = personId;
	}

	/**
	 * @return the person
	 */
	public String getPerson() {
		return person;
	}

	/**
	 * @param person
	 *            the person to set
	 */
	public void setPerson(String person) {
		this.person = person;
	}

	/**
	 * @return the date
	 */
	public long getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(long date) {
		this.date = date;
	}

	/**
	 * @return the read
	 */
	public boolean isRead() {
		return read;
	}

	/**
	 * @param read
	 *            the read to set
	 */
	public void setRead(boolean read) {
		this.read = read;
	}

	/**
	 * @return the seen
	 */
	public boolean isSeen() {
		return seen;
	}

	/**
	 * @param seen
	 *            the seen to set
	 */
	public void setSeen(boolean seen) {
		this.seen = seen;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 *            the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body
	 *            the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "Sms [type=" + type + ", threadId=" + threadId + ", address="
				+ address + ", personId=" + personId + ", person=" + person
				+ ", date=" + date + ", read=" + read + ", seen=" + seen
				+ ", status=" + status + ", subject=" + subject + ", body="
				+ body + "]";
	}

	/**
	 * Base columns for tables that contain text based SMSs.
	 */
	public interface TextBasedSmsColumns {
		/**
		 * The type of the message
		 * <P>
		 * Type: INTEGER
		 * </P>
		 */
		public static final String TYPE = "type";

		public static final int MESSAGE_TYPE_ALL = 0;
		public static final int MESSAGE_TYPE_INBOX = 1;
		public static final int MESSAGE_TYPE_SENT = 2;
		public static final int MESSAGE_TYPE_DRAFT = 3;
		public static final int MESSAGE_TYPE_OUTBOX = 4;
		public static final int MESSAGE_TYPE_FAILED = 5; // for failed outgoing
															// messages
		public static final int MESSAGE_TYPE_QUEUED = 6; // for messages to send
															// later

		/**
		 * The thread ID of the message
		 * <P>
		 * Type: INTEGER
		 * </P>
		 */
		public static final String THREAD_ID = "thread_id";

		/**
		 * The address of the other party
		 * <P>
		 * Type: TEXT
		 * </P>
		 */
		public static final String ADDRESS = "address";

		/**
		 * The person ID of the sender
		 * <P>
		 * Type: INTEGER (long)
		 * </P>
		 */
		public static final String PERSON_ID = "person";

		/**
		 * The date the message was sent
		 * <P>
		 * Type: INTEGER (long)
		 * </P>
		 */
		public static final String DATE = "date";

		/**
		 * Has the message been read
		 * <P>
		 * Type: INTEGER (boolean)
		 * </P>
		 */
		public static final String READ = "read";

		/**
		 * Indicates whether this message has been seen by the user. The "seen"
		 * flag will be used to figure out whether we need to throw up a
		 * statusbar notification or not.
		 */
		public static final String SEEN = "seen";

		/**
		 * The TP-Status value for the message, or -1 if no status has been
		 * received
		 */
		public static final String STATUS = "status";

		public static final int STATUS_NONE = -1;
		public static final int STATUS_COMPLETE = 0;
		public static final int STATUS_PENDING = 32;
		public static final int STATUS_FAILED = 64;

		/**
		 * The subject of the message, if present
		 * <P>
		 * Type: TEXT
		 * </P>
		 */
		public static final String SUBJECT = "subject";

		/**
		 * The body of the message
		 * <P>
		 * Type: TEXT
		 * </P>
		 */
		public static final String BODY = "body";

		/**
		 * The id of the sender of the conversation, if present
		 * <P>
		 * Type: INTEGER (reference to item in content://contacts/people)
		 * </P>
		 */
		public static final String PERSON = "person";

		/**
		 * The protocol identifier code
		 * <P>
		 * Type: INTEGER
		 * </P>
		 */
		public static final String PROTOCOL = "protocol";

		/**
		 * Whether the <code>TP-Reply-Path</code> bit was set on this message
		 * <P>
		 * Type: BOOLEAN
		 * </P>
		 */
		public static final String REPLY_PATH_PRESENT = "reply_path_present";

		/**
		 * The service center (SC) through which to send the message, if present
		 * <P>
		 * Type: TEXT
		 * </P>
		 */
		public static final String SERVICE_CENTER = "service_center";

		/**
		 * Has the message been locked?
		 * <P>
		 * Type: INTEGER (boolean)
		 * </P>
		 */
		public static final String LOCKED = "locked";

		/**
		 * Error code associated with sending or receiving this message
		 * <P>
		 * Type: INTEGER
		 * </P>
		 */
		public static final String ERROR_CODE = "error_code";

		/**
		 * Meta data used externally.
		 * <P>
		 * Type: TEXT
		 * </P>
		 */
		public static final String META_DATA = "meta_data";
	}

	/**
	 * The content:// style URL for this table
	 */
	public static final Uri SMS_CONTENT_URI = Uri.parse("content://sms");

	/**
	 * The content:// style URL for this table
	 */
	public static final Uri INBOX_CONTENT_URI = Uri
			.parse("content://sms/inbox");

	/**
	 * The content:// style URL for this table
	 */
	public static final Uri SENT_CONTENT_URI = Uri.parse("content://sms/sent");

	/**
	 * The content:// style URL for this table
	 */
	public static final Uri DRAFT_CONTENT_URI = Uri
			.parse("content://sms/draft");

	/**
	 * The content:// style URL for this table
	 */
	public static final Uri OUTBOX_CONTENT_URI = Uri
			.parse("content://sms/outbox");

	/**
	 * The content:// style URL for this table
	 */
	public static final Uri CONVERSATIONS_CONTENT_URI = Uri
			.parse("content://sms/conversations");

	/**
	 * The default sort order for this table
	 */
	public static final String DEFAULT_SORT_ORDER = "date DESC";
}
