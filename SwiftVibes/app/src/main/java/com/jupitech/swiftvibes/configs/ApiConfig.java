package com.jupitech.swiftvibes.configs;

public class ApiConfig {

    public static final String APP_URL = "http://dev.swiftvibes.com/api/";

    public static final String ADD_USER = APP_URL + "add_user";

    public static final String VERIFICATION_CODE = APP_URL + "verification_user";

    public static final String GET_FRIEND_NEWS = APP_URL + "get_friends_news";

    public static final String SENT_MESSAGE = APP_URL + "add_message";

    public static final String GET_ALL_MESSAGE = APP_URL + "get_all_messages";

    public static final String GET_THUMNAIL_IMAGE = APP_URL + "hatt?code=%s&thumbs=1";

    public static final String GET_IMAGE = APP_URL + "hatt?code=%s";

    public static final String GET_AUDIO = APP_URL + "hatt?code=%s";

    public static final String GET_COMMENT= APP_URL + "get_comment";

    public static final String ADD_COMMENT = APP_URL + "add_comment";

}
