package com.jupitech.swiftvibes.providers;

/**
 * Struct with country information
 * 
 * @author erikd
 */
public class Country {
    
    /**
     * Unknown country
     */
    public static final Country UNKNOWN = new Country("??", "??", "??", "UNKNOWN", "00", "?");
    
    /**
     * The telephony country number. For example '31' for The Netherlands
     */
    public final String number;
    
    /**
     * The MCC country code. For example '204' for The Netherlands
     */
    public final String mcc;
    
    /**
     * ISO country code. For example 'NL' for The Netherlands
     */
    public final String iso;
    
    /**
     * The name of the country. For example 'The Netherlands'
     */
    public final String name;
    
    /**
     * The international exit code (i.e. the 'real' value of '+')
     */
    public final String exit;
    
    /**
     * The single digit access code to dial within a country. This number is left out
     * when calling abroad.
     */
    public final String trunk;
    
    /**
     * Constructor
     * 
     * @param number
     * @param mcc
     * @param iso
     * @param name
     * @param prefix
     */
    Country(String number, String mcc, String iso, String name, String exit, String trunk) {
        this.number = number;
        this.mcc = mcc;
        this.iso = iso;
        this.name = name;
        this.exit = exit;
        this.trunk = trunk;
    }
    
    static final Country[] COUNTRIES;
    
    public static Country getByMCC(String mcc) {
        for (int i = 0; i < COUNTRIES.length; i++) {
            if (COUNTRIES[i].mcc.equals(mcc)) {
                return COUNTRIES[i];
            }
        }
        
        return UNKNOWN;
    }
    
    public static Country getByISO(String iso){
    	for (int i = 0; i < COUNTRIES.length; i++) {
            if (COUNTRIES[i].iso.equalsIgnoreCase(iso)) {
                return COUNTRIES[i];
            }
        }
        
        return UNKNOWN;
    }
    
    private static final String[][] TABLE = {
        // TEL  MCC    ISO   NAME                   EXIT    TRUNK
        { "93", "412", "AF", "Afghanistan",         "00",   "0"}, 
        {"355", "276", "AL", "Albania",             "00",   "0"}, 
        {"213", "603", "DZ", "Algeria",             "00",   "0"}, 
        {"684", "544", "AS", "American Samoa (US)", "00",   "0"}, 
        {"376", "213", "AD", "Andorra",             "00",   "0"}, 
        {"244", "631", "AO", "Angola",              "00",   "0"}, 
        {"809", "365", "AI", "Anguilla",            "011",  "0"}, 
        {"268", "344", "AG", "Antigua and Barbuda", "011",  "0"}, 
        { "54", "722", "AR", "Argentine Republic",  "00",   "0"}, 
        {"374", "283", "AM", "Armenia",             "00",   "0"}, 
        {"297", "363", "AW", "Aruba (Netherlands)", "00",   "0"}, 
        { "61", "505", "AU", "Australia",           "0011", "0"}, 
        { "43", "232", "AT", "Austria",             "00",   "0"}, 
        {"994", "400", "AZ", "Azerbaijani Republic","00",   "0"}, 
        {"242", "364", "BS", "Bahamas",             "011",  "1"}, 
        {"973", "426", "BH", "Bahrain",             "00",   "0"}, 
        {"880", "470", "BD", "Bangladesh",          "00",   "0"}, 
        {"246", "342", "BB", "Barbados",            "011",  "1"}, 
        {"375", "257", "BY", "Belarus",             "810*", "8"}, 
        { "32", "206", "BE", "Belgium",             "00",   "0"}, 
        {"501", "702", "BZ", "Belize",              "00",   ""}, 
        {"229", "616", "BJ", "Benin",               "00",   ""}, 
        {"809", "350", "BM", "Bermuda (UK)",        "00",   "0"}, 
        {"975", "402", "BT", "Bhutan",              "00",   ""}, 
        {"591", "736", "BO", "Bolivia",             "00",   "0"}, 
        {"387", "218", "BA", "Bosnia and Herzegovina", "00", "0"}, 
        {"267", "652", "BW", "Botswana",            "00",   ""}, 
        { "55", "724", "BR", "Brazil",              "00",   "0"}, 
        {"284", "348", "VG", "British Virgin Islands (UK)", "011", "1"}, 
        {"673", "528", "BN", "Brunei Darussalam",   "00",   ""}, 
        {"359", "284", "BG", "Bulgaria",            "00",   "0"}, 
        {"226", "613", "BF", "Burkina Faso",        "00",   ""}, 
        {"257", "642", "BI", "Burundi",             "00",   ""}, 
        {"855", "456", "KH", "Cambodia",            "00",   "0"}, 
        {"237", "624", "CM", "Cameroon",            "00",   "0"}, 
        {  "1", "302", "CA", "Canada",              "011",  "1"}, 
        {"238", "625", "CV", "Cape Verde",          "00",   "0"}, 
        {"345", "346", "KY", "Cayman Islands (UK)", "011",  "1"}, 
        {"236", "623", "CF", "Central African Republic", "00", ""}, 
        {"235", "622", "TD", "Chad",                "00",   ""}, 
        { "56", "730", "CL", "Chile",               "00",   "0"}, 
        { "86", "460", "CN", "China",               "00",   "0"}, 
        {"886", "461", "CN", "Taiwan",              "00",   "0"}, 
        { "57", "732", "CO", "Colombia",            "00",   "0"}, 
        {"269", "654", "KM", "Comoros",             "00",   ""}, 
        {"242", "629", "CG", "Republic of the Congo", "00", "0"}, 
        {"682", "548", "CK", "Cook Islands (NZ)",   "00",   "0"}, 
        {"506", "712", "CR", "Costa Rica",          "00",   "0"}, 
        {"225", "612", "CI", "C�te d'Ivoire",       "00",   "0"}, 
        {"385", "219", "HR", "Croatia",             "00",   "0"}, 
        { "53", "368", "CU", "Cuba",                "00",   "0"}, 
        {"357", "280", "CY", "Cyprus",              "00",   "0"}, 
        {"420", "230", "CZ", "Czech Republic",      "00",   "0"}, 
        {"242", "630", "CD", "Democratic Republic of the Congo", "00", "0"}, 
        { "45", "238", "DK", "Denmark",             "00",   "0"}, 
        {"253", "638", "DJ", "Djibouti", "00", "0"}, 
        {"767", "366", "DM", "Dominica", "00", "0"}, 
        {"809", "370", "DO", "Dominican Republic", "00", "0"}, 
        {   "", "514", "TL", "East Timor", "00", "0"}, 
        {"593", "740", "EC", "Ecuador", "00", "0"}, 
        { "20", "602", "EG", "Egypt", "00", "0"}, 
        {"503", "706", "SV", "El Salvador", "00", "0"}, 
        {"240", "627", "GQ", "Equatorial Guinea", "00", "0"}, 
        {"291", "657", "ER", "Eritrea", "00", "0"}, 
        {"372", "248", "EE", "Estonia", "00", "0"}, 
        {"251", "636", "ET", "Ethiopia", "00", "0"}, 
        {"500", "750", "FK", "Falkland Islands (Malvinas)", "00", "0"}, 
        {"298", "288", "FO", "Faroe Islands (Denmark)", "00", "0"}, 
        {"679", "542", "FJ", "Fiji", "00", "0"}, 
        {"358", "244", "FI", "Finland", "00", "0"}, 
        { "33", "208", "FR", "France", "00", "0"}, 
        {"594", "742", "GF", "French Guiana (France)", "00", "0"}, 
        {"596", "547", "PF", "French Polynesia (France)", "00", "0"}, 
        {"241", "628", "GA", "Gabonese Republic", "00", "0"}, 
        {"220", "607", "GM", "Gambia", "00", "0"}, 
        {"995", "282", "GE", "Georgia", "00", "0"}, 
        { "49", "262", "DE", "Germany", "00", "0"}, 
        {"233", "620", "GH", "Ghana", "00", "0"}, 
        {"350", "266", "GI", "Gibraltar (UK)", "00", "0"}, 
        { "30", "202", "GR", "Greece", "00", "0"}, 
        {"299", "290", "GL", "Greenland (Denmark)", "00", "0"}, 
        {"473", "352", "GD", "Grenada", "00", "0"}, 
        {"596", "340", "GP", "Guadeloupe (France)", "00", "0"}, 
        {"671", "535", "GU", "Guam (US)", "00", "0"}, 
        {"502", "704", "GT", "Guatemala", "00", "0"}, 
        {"224", "611", "GN", "Guinea", "00", "0"}, 
        {"245", "632", "GW", "Guinea-Bissau", "00", "0"}, 
        {"592", "738", "GY", "Guyana", "00", "0"}, 
        {"509", "372", "HT", "Haiti", "00", "0"}, 
        {"504", "708", "HN", "Honduras", "00", "0"}, 
        {"852", "454", "HK", "Hong Kong (PRC)", "00", "0"}, 
        { "36", "216", "HU", "Hungary", "00", "0"}, 
        {"354", "274", "IS", "Iceland", "00", "0"}, 
        { "91", "404", "IN", "India", "00", "0"}, 
        { "91", "405", "IN", "India", "00", "0"}, 
        { "91", "406", "IN", "India", "00", "0"}, 
        { "62", "510", "ID", "Indonesia", "00", "0"}, 
        { "98", "432", "IR", "Iran", "00", "0"}, 
        {"964", "418", "IQ", "Iraq", "00", "0"}, 
        {"353", "272", "IE", "Ireland", "00", "0"}, 
        {"972", "425", "IL", "Israel", "00", "0"}, 
        { "39", "222", "IT", "Italy", "00", "0"}, 
        {"876", "338", "JM", "Jamaica", "00", "0"}, 
        { "81", "441", "JP", "Japan", "00", "0"}, 
        { "81", "440", "JP", "Japan", "00", "0"}, 
        {"962", "416", "JO", "Jordan", "00", "0"}, 
        {  "7", "401", "KZ", "Kazakhstan", "00", "0"}, 
        {"254", "639", "KE", "Kenya", "00", "0"}, 
        {"686", "545", "KI", "Kiribati", "00", "0"}, 
        {"850", "467", "KP", "Korea, North", "00", "0"}, 
        { "82", "450", "KR", "Korea, South", "00", "0"}, 
        {"965", "419", "KW", "Kuwait", "00", "0"}, 
        {"996", "437", "KG", "Kyrgyz Republic", "00", "0"}, 
        {"856", "457", "LA", "Laos", "00", "0"}, 
        {"371", "247", "LV", "Latvia", "00", "0"}, 
        {"961", "415", "LB", "Lebanon", "00", "0"}, 
        {"266", "651", "LS", "Lesotho", "00", "0"}, 
        {"231", "618", "LR", "Liberia", "00", "0"}, 
        {"218", "606", "LY", "Libya", "00", "0"}, 
        {"423", "295", "LI", "Liechtenstein", "00", "0"}, 
        {"370", "246", "LT", "Lithuania", "00", "0"}, 
        {"352", "270", "LU", "Luxembourg", "00", "0"}, 
        {"853", "455", "MO", "Macau (PRC)", "00", "0"}, 
        {"389", "294", "MK", "Republic of Macedonia", "00", "0"}, 
        {"261", "646", "MG", "Madagascar", "00", "0"}, 
        {"265", "650", "MW", "Malawi", "00", "0"}, 
        { "60", "502", "MY", "Malaysia", "00", "0"}, 
        {"960", "472", "MV", "Maldives", "00", "0"}, 
        {"223", "610", "ML", "Mali", "00", "0"}, 
        {"356", "278", "MT", "Malta", "00", "0"}, 
        {"692", "551", "MH", "Marshall Islands", "00", "0"}, 
        {"596", "340", "MQ", "Martinique (France)", "00", "0"}, 
        {"222", "609", "MR", "Mauritania", "00", "0"}, 
        {"230", "617", "MU", "Mauritius", "00", "0"}, 
        { "52", "334", "MX", "Mexico", "00", "0"}, 
        {"691", "550", "FM", "Federated States of Micronesia", "00", "0"}, 
        {"373", "259", "MD", "Moldova", "00", "0"}, 
        { "33", "212", "MC", "Monaco", "00", "0"}, 
        {"976", "428", "MN", "Mongolia", "00", "0"}, 
        {"381", "297", "ME", "Montenegro (Republic of)", "00", "0"}, 
        {"473", "354", "MS", "Montserrat (UK)", "00", "0"}, 
        {"212", "604", "MA", "Morocco", "00", "0"}, 
        {"258", "643", "MZ", "Mozambique", "00", "0"}, 
        { "95", "414", "MM", "Myanmar", "00", "0"}, 
        {"264", "649", "NA", "Namibia", "00", "0"}, 
        {"674", "536", "NR", "Nauru", "00", "0"}, 
        {"977", "429", "NP", "Nepal", "00", "0"}, 
        { "31", "204", "NL", "Netherlands", "00", "0"}, 
        {"599", "362", "AN", "Netherlands Antilles (Netherlands)", "00", "0"}, 
        {"687", "546", "NC", "New Caledonia (France)", "00", "0"}, 
        { "64", "530", "NZ", "New Zealand", "00", "0"}, 
        {"505", "710", "NI", "Nicaragua", "00", "0"}, 
        {"227", "614", "NE", "Niger", "00", "0"}, 
        {"234", "621", "NG", "Nigeria", "00", "0"}, 
        {"1670", "534", "MP", "Northern Mariana Islands (US)", "00", "0"}, 
        { "47", "242", "NO", "Norway", "00", "0"}, 
        {"968", "422", "OM", "Oman", "00", "0"}, 
        { "92", "410", "PK", "Pakistan", "00", "0"}, 
        {"680", "552", "PW", "Palau", "00", "0"}, 
        {"972", "423", "PS", "Palestine", "00", "0"}, 
        {"507", "714", "PA", "Panama", "00", "0"}, 
        {"675", "537", "PG", "Papua New Guinea", "00", "0"}, 
        {"595", "744", "PY", "Paraguay", "00", "0"}, 
        { "51", "716", "PE", "Per�", "00", "0"}, 
        { "63", "515", "PH", "Philippines", "00", "0"}, 
        { "48", "260", "PL", "Poland", "00", "0"}, 
        {"351", "268", "PT", "Portugal", "00", "0"}, 
        {"1787", "330", "PR", "Puerto Rico (US)", "00", "0"}, 
        {"974", "427", "QA", "Qatar", "00", "0"}, 
        {"262", "647", "RE", "R�union (France)", "00", "0"}, 
        { "40", "226", "RO", "Romania", "00", "0"}, 
        {  "7", "250", "RU", "Russian Federation", "00", "0"}, 
        {"250", "635", "RW", "Rwandese Republic", "00", "0"}, 
        {"869", "356", "KN", "Saint Kitts and Nevis", "00", "0"}, 
        {"", "358", "LC", "Saint Lucia", "00", "0"}, 
        {"508", "308", "PM", "Saint Pierre and Miquelon (France)", "00", "0"}, 
        {"", "360", "VC", "Saint Vincent and the Grenadines", "00", "0"}, 
        {"685", "549", "WS", "Samoa", "00", "0"}, 
        {"378", "292", "SM", "San Marino", "00", "0"}, 
        {"239", "626", "ST", "S�o Tom� and Pr�ncipe", "00", "0"}, 
        {"966", "420", "SA", "Saudi Arabia", "00", "0"}, 
        {"221", "608", "SN", "Senegal", "00", "0"}, 
        {"381", "220", "RS", "Serbia (Republic of)", "00", "0"}, 
        {"248", "633", "SC", "Seychelles", "00", "0"}, 
        {"232", "619", "SL", "Sierra Leone", "00", "0"}, 
        { "65", "525", "SG", "Singapore", "00", "0"}, 
        {"421", "231", "SK", "Slovakia", "00", "0"}, 
        {"386", "293", "SI", "Slovenia", "00", "0"}, 
        {"677", "540", "SB", "Solomon Islands", "00", "0"}, 
        {"252", "637", "SO", "Somalia", "00", "0"}, 
        { "27", "655", "ZA", "South Africa", "00", "0"}, 
        { "34", "214", "ES", "Spain", "00", "0"}, 
        { "94", "413", "LK", "Sri Lanka", "00", "0"}, 
        {"249", "634", "SD", "Sudan", "00", "0"}, 
        {"597", "746", "SR", "Suriname", "00", "0"}, 
        {"268", "653", "SZ", "Swaziland", "00", "0"}, 
        { "46", "240", "SE", "Sweden", "00", "0"}, 
        { "41", "228", "CH", "Switzerland", "00", "0"}, 
        {"963", "417", "SY", "Syria", "00", "0"}, 
        {"886", "466", "TW", "Taiwan", "00", "0"}, 
        {  "7", "436", "TJ", "Tajikistan", "00", "0"}, 
        {"255", "640", "TZ", "Tanzania", "00", "0"}, 
        { "66", "520", "TH", "Thailand", "00", "0"}, 
        {"228", "615", "TG", "Togolese Republic", "00", "0"}, 
        {"676", "539", "TO", "Tonga", "00", "0"}, 
        {"1868", "374", "TT", "Trinidad and Tobago", "00", "0"}, 
        {"216", "605", "TN", "Tunisia", "00", "0"}, 
        { "90", "286", "TR", "Turkey", "00", "0"}, 
        {"993", "438", "TM", "Turkmenistan", "00", "0"}, 
        {"",    "376", "TC", "Turks and Caicos Islands (UK)", "00", "0"}, 
        {"256", "641", "UG", "Uganda", "00", "0"}, 
        {"380", "255", "UA", "Ukraine", "00", "0"}, 
        {"971", "424", "AE", "United Arab Emirates", "00", "0"}, 
        {"971", "430", "AE", "United Arab Emirates", "00", "0"}, 
        {"971", "431", "AE", "United Arab Emirates", "00", "0"}, 
        { "44", "235", "GB", "United Kingdom", "00", "0"}, 
        { "44", "234", "GB", "United Kingdom", "00", "0"}, 
        {  "1", "310", "US", "United States of America", "011", "1"}, 
        {  "1", "311", "US", "United States of America", "011", "1"}, 
        {  "1", "312", "US", "United States of America", "011", "1"}, 
        {  "1", "313", "US", "United States of America", "011", "1"}, 
        {  "1", "314", "US", "United States of America", "011", "1"}, 
        {  "1", "315", "US", "United States of America", "011", "1"}, 
        {  "1", "316", "US", "United States of America", "011", "1"}, 
        {"1340", "332", "VI", "United States Virgin Islands (US)", "00", "0"}, 
        {"598", "748", "UY", "Uruguay", "00", "0"}, 
        {  "7", "434", "UZ", "Uzbekistan", "00", "0"}, 
        {"678", "541", "VU", "Vanuatu", "00", "0"}, 
        { "39", "225", "VA", "Vatican City State", "00", "0"}, 
        { "58", "734", "VE", "Venezuela", "00", "0"}, 
        { "84", "452", "VN", "Vietnam", "00", "0"}, 
        {"681", "543", "WF", "Wallis and Futuna (France)", "00", "0"}, 
        {"381", "421", "YE", "Yemen", "00", "0"}, 
        {"260", "645", "ZM", "Zambia", "00", "0"}, 
        {"263", "648", "ZW", "Zimbabwe", "00", "0"}
    };
    
    static {
        COUNTRIES = new Country[TABLE.length];
        for (int i = 0; i < TABLE.length; i++) {
            COUNTRIES[i] = new Country(TABLE[i][0], TABLE[i][1], TABLE[i][2], TABLE[i][3], TABLE[i][4], TABLE[i][5]);
        }
    }
}
