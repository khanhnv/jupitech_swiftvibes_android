package com.jupitech.swiftvibes.models;

import android.widget.Checkable;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.jupitech.swiftvibes.AppApplication;
import com.jupitech.swiftvibes.databases.sqlite.Connection;
import com.jupitech.swiftvibes.utils.Log;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

@DatabaseTable(tableName = "tbl_user")
public class UserModel implements Serializable, Checkable {

    /**
     *
     */
    private static final long serialVersionUID = -618862669350194758L;

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private int id;

    @DatabaseField(defaultValue = "")
    private String firstName;

    @DatabaseField(defaultValue = "")
    private String lastName;

    @DatabaseField
    private boolean isLogined;

    @SerializedName("user_id")
    @DatabaseField(defaultValue = "")
    private String userId;

    @SerializedName("name")
    @DatabaseField(defaultValue = "")
    private String name;

    @SerializedName("phone")
    @DatabaseField(defaultValue = "")
    private String phone;

    @SerializedName("country_code")
    @DatabaseField(defaultValue = "")
    private String countryCode;

    @SerializedName("status")
    @DatabaseField(defaultValue = "")
    private String status;

    @DatabaseField()
    private int isFriend;

    @SerializedName("device_os")
    @DatabaseField(defaultValue = "")
    private String deviceOs;

    @SerializedName("device_os_version")
    @DatabaseField(defaultValue = "")
    private String deviceOsVersion;

    @SerializedName("device_id")
    @DatabaseField(defaultValue = "")
    private String deviceId;

    @DatabaseField
    private boolean isEnterDigit;

    private boolean selected;

    public UserModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isLogined() {
        return isLogined;
    }

    public void setLogined(boolean isLogined) {
        this.isLogined = isLogined;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int isFriend() {
        return isFriend;
    }

    public void setFriend(int isFriend) {
        this.isFriend = isFriend;
    }

    public String getDeviceOs() {
        return deviceOs;
    }

    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public boolean isEnterDigit() {
        return isEnterDigit;
    }

    public void setEnterDigit(boolean isEnterDigit) {
        this.isEnterDigit = isEnterDigit;
    }

    public static boolean isExist(UserModel userModel) {
        Connection connection = Connection.getInstance(AppApplication.get());
        try {
//            QueryBuilder<UserModel, Integer> builder = connection.getUserDao().queryBuilder();
//            Where<UserModel, Integer> where = builder.where();
//            where.eq("countryCode", userModel.getCountryCode()).and().eq("phone", userModel.getPhone());
//            builder.setWhere(where);
//            List<UserModel> results = builder.query();
            List<UserModel> results = connection.getUserDao().queryForEq("userId", userModel.getUserId());
            if (results != null && results.size() > 0) {
                Log.d("result: " + results.toString());
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean add(UserModel userModel) {
        Connection connection = Connection.getInstance(AppApplication.get());
        boolean result = false;
        try {
            result = connection.getUserDao().create(userModel) == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            result = false;
        }
        Log.d("AddUser: " + result);
        return result;
    }

    public static boolean update(UserModel userModel) {
        Connection connection = Connection.getInstance(AppApplication.get());
        boolean result = false;
        try {
            result = connection.getUserDao().update(userModel) == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            result = false;
        }
        Log.d("AddUser: " + result);
        return result;
    }

    public static UserModel getUser() {
        Connection connection = Connection.getInstance(AppApplication.get());
        try {
            List<UserModel> userModels = connection.getUserDao().queryForEq("isFriend", 0);
            if (userModels != null && userModels.size() > 0) {
                return userModels.get(0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<UserModel> getFriends() {
        Connection connection = Connection.getInstance(AppApplication.get());
        try {
            List<UserModel> userModels = connection.getUserDao().queryForEq("isFriend", 1);
            return userModels;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean save() {
        Connection connection = Connection.getInstance(AppApplication.get());
        boolean result = false;
        try {
            result = connection.getUserDao().update(this) == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            result = false;
        }

        return result;
    }

    @Override
    public void setChecked(boolean checked) {
        selected = checked;
    }

    @Override
    public boolean isChecked() {
        return selected;
    }

    @Override
    public void toggle() {
        selected = !selected;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isLogined=" + isLogined +
                ", userId='" + userId + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", status='" + status + '\'' +
                ", isFriend=" + isFriend +
                ", selected=" + selected +
                '}';
    }
}
