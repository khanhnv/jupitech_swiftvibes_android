package com.jupitech.swiftvibes.socials.twitter;/*
package com.devmaster.myhelper.social.twitter;

import java.util.ArrayList;
import java.util.List;

import twitter4j.TwitterException;
import twitter4j.User;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class TwitterApp extends Activity implements OnClickListener {

	private Button buttonLogin;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		buttonLogin = (Button) findViewById(R.id.twitterLogin);
		buttonLogin.setOnClickListener(this);
		TwitterUtil.getInstance(this).onCreate(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		TwitterUtil.getInstance(this).onResume();
		if (TwitterUtil.getInstance(this).isConnected()) {
			buttonLogin.setText(R.string.label_disconnect);
			
			//implement twitter util after isConnected
			//search max 1000 users has name bruce
			List<User> users = TwitterUtil.getInstance(this).search("bruce", 0);
			if (users != null) {
				for (User user : users) {
					if (user != null) {
						Log.d(user.getName());
					}
				}
			} else {
				Toast.makeText(this, "not found", Toast.LENGTH_SHORT).show();
			}
		} else {
			buttonLogin.setText(R.string.label_connect);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.twitterLogin:
			if (TwitterUtil.getInstance(this).isConnected()) {
				// disconnect
				TwitterUtil.getInstance(this).disconnectTwitter();
				buttonLogin.setText(R.string.label_connect);
			} else {
				// login
				TwitterUtil.getInstance(this).login();
			}
			break;
		default:
			break;
		}
	}
}
*/
