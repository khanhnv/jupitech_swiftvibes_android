package com.jupitech.swiftvibes.socials.googleplus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.plus.PlusShare;

public abstract class HandleDeepLinkActivity extends Activity {

	private Class<?> startActivity;
	private String deepLinkId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		startActivity = passStartActivity();
		deepLinkId = passDeepLinkId();

		String deepLinkId = PlusShare.getDeepLinkId(this.getIntent());
		Intent target = parseDeepLinkId(deepLinkId);
		if (target != null) {
			startActivity(target);
		}

		finish();
	}

	/**
	 * Set activity is target
	 */
	abstract <T> Class<T> passStartActivity();

	abstract String passDeepLinkId();

	/**
	 * Get the intent for an activity corresponding to the deep-link ID.
	 * 
	 * @param deepLinkId
	 *            The deep-link ID to parse.
	 * @return The intent corresponding to the deep-link ID.
	 */
	private Intent parseDeepLinkId(String deepLinkId) {
		if (startActivity == null) {
			return null;
		}
		Intent route = new Intent();
		if (this.deepLinkId.equals(deepLinkId)) {
			route.setClass(getApplicationContext(), startActivity);
		} else {
			// Fallback to the MainActivity in your app.
			route.setClass(getApplicationContext(), startActivity);
		}
		return route;
	}

}
