package com.jupitech.swiftvibes.activities.fragments.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.adapters.MessageAdapter;
import com.jupitech.swiftvibes.apis.loopj.RestClient;
import com.jupitech.swiftvibes.apis.parse.JSONConvert;
import com.jupitech.swiftvibes.configs.Key;
import com.jupitech.swiftvibes.listeners.OnItemLongClick;
import com.jupitech.swiftvibes.models.PostModel;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.utils.Log;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;

import java.util.List;

public class HomeFragment extends BaseFragment {

    com.handmark.pulltorefresh.library.PullToRefreshListView mListView;

    MessageAdapter mAdapter;

    int mPage = 0;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        if (savedInstanceState != null) {
            mPage = savedInstanceState.getInt("page");
        }
        initModels();
    }

    @Override
    protected void initModels() {
    }

    @Override
    protected void initViews(View view) {

        mListView = (com.handmark.pulltorefresh.library.PullToRefreshListView) view.findViewById(R.id.pull_refresh_list);
        mListView.setMode(PullToRefreshBase.Mode.BOTH);

//        View empty = view.findViewById(R.id.empty_text);
//        mListView.setEmptyView(empty);

        mAdapter = new MessageAdapter(getActivity(), 0, new OnItemLongClick() {
            @Override
            public void onItemLongClick(int position, PostModel post) {
                showDetailWithIndex(position);
            }
        });

        mListView.setAdapter(mAdapter);

        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                mAdapter.clear();
                mPage = 0;
                getMessages();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                mPage++;
                getMessages();
            }
        });

        mAdapter.clear();
        mPage = 0;
        getMessages();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onClick(View v) {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("page", mPage);
    }

    public void getMessages() {
        final String userId = UserModel.getUser().getUserId();
        String limit = (mPage == 0) ? "20" : "10";
        String offset = "0";
        if (mPage > 0) {
            offset = String.valueOf((mPage + 1) * 10 + 1);
        }

        Log.d("Page: " + mPage);
        Log.d("Limit: " + limit);
        Log.d("Offset: " + offset);

        RestClient.getMessages(userId, limit, offset, new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                showProgress();
            }

            @Override
            public void onFailure(String responseBody, Throwable error) {
                super.onFailure(responseBody, error);

                mListView.onRefreshComplete();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
//                Log.d("response: " + responseBody);

                List<PostModel> postModels = JSONConvert.parseMessageResponse(responseBody);
                if (postModels != null && postModels.size() > 0) {

                    postModels = processData(postModels, userId);
                    mAdapter.addAll(postModels);
                    mAdapter.notifyDataSetChanged();
                }

                mListView.onRefreshComplete();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                hideProgress();
            }
        });
    }

    public List<PostModel> processData(List<PostModel> input, String userId) {
        for (PostModel post : input) {
            boolean me = post.getFromId().equals(userId);
            post.setMe(me);

            if (post.getByMe() != null && post.getByMe().equalsIgnoreCase("1")) {

                if (post.getStatus().equalsIgnoreCase("1")) {
                    post.setNotOpen(true);
                } else if (post.getStatus().equalsIgnoreCase("0")) {
                    post.setNotOpen(true);
                } else if (post.getStatus().equalsIgnoreCase("2")) {
                    post.setNotOpen(true);
                }

            } else if (post.getByMe() != null && post.getByMe().equalsIgnoreCase("0")) {

                if (post.getStatus().equalsIgnoreCase("1")) {
                    post.setHeard(true);
                    post.setNotOpen(false);

                } else if (post.getStatus().equalsIgnoreCase("2")) {
                    post.setHeard(false);
                    if (me) {
                        post.setNotOpen(true);
                    } else {
                        post.setNotOpen(false);
                    }
                }
            } else {

                if (!me) {
                    if (post.getStatus().equalsIgnoreCase("0")) {
                        post.setHeard(false);
                        post.setNotOpen(false);
                    } else {
                        post.setNotOpen(true);
                    }
                } else {
                    post.setNotOpen(true);
                }
            }
        }


        return input;
    }

    public void showDetailWithIndex(int position) {
        PostModel post = mAdapter.getItem(position);
        mApplication.setPost(post);
        Log.d("Post: " + post.toString());

        if (!post.isNotOpen()) {

            if (post.getByMe() == null || (post.getByMe() != null && !post.getByMe().equalsIgnoreCase("1"))) {

                if (post.isHeard()) {

                    // show heard view
//                    LaunchIntent.start(getActivity(), HeardActivity.class);
//                    getActivity().overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);

                    switchFragment(Key.FRAGMENT_HEARD, HeardFragment.newInstance(post), true);
                } else {

                    // show voice view
//                    LaunchIntent.start(getActivity(), RecordActivity.class);
//                    getActivity().overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);

                    switchFragment(Key.FRAGMENT_RECORD, RecordFragment.newInstance(post), true);
                }
            }
        }
    }



}
