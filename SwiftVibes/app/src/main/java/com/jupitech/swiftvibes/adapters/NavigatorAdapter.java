package com.jupitech.swiftvibes.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.jupitech.swiftvibes.models.NavigatorModel;
import com.jupitech.swiftvibes.widgets.CustomTextView;
import com.jupitech.swiftvibes.widgets.NavigatorRowItem;

import java.util.List;

public class NavigatorAdapter extends ArrayAdapter<NavigatorModel> {

	public NavigatorAdapter(Context context, int resource,
                            List<NavigatorModel> objects) {
		super(context, resource, objects);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final NavigatorModel item = getItem(position);
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = new NavigatorRowItem(getContext());

			holder = new ViewHolder();

			holder.mIcon = ((NavigatorRowItem) convertView).mIcon;
			holder.mNameText = ((NavigatorRowItem) convertView).mNameText;

			convertView.setTag(holder);

		} else {

			holder = (ViewHolder) convertView.getTag();
		}

		holder.mIcon.setImageResource(item.getId());
		holder.mNameText.setText(item.getName());

		return convertView;
	}

	static class ViewHolder {
		public ImageView mIcon;
		public CustomTextView mNameText;
	}

}
