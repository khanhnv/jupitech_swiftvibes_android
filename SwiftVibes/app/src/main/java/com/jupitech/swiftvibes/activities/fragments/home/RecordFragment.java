package com.jupitech.swiftvibes.activities.fragments.home;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.apis.loopj.RestClient;
import com.jupitech.swiftvibes.configs.ApiConfig;
import com.jupitech.swiftvibes.configs.Key;
import com.jupitech.swiftvibes.models.PostModel;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.utils.ToastUtil;
import com.jupitech.swiftvibes.utils.text.StringUtil;
import com.loopj.android.http.TextHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.apache.http.Header;

import java.io.File;
import java.io.IOException;

public class RecordFragment extends BaseFragment {

    private static final int MAX_TIME = 5;//5 second
    private static final String ARG_POST = "post";
    PostModel mPostModel;

    private ImageView mImageView;
    private TextView mTimeText;

    private int time = MAX_TIME;

    public static RecordFragment newInstance(PostModel post) {
        RecordFragment fragment = new RecordFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_POST, post);
        fragment.setArguments(args);
        return fragment;
    }

    public RecordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPostModel = (PostModel) getArguments().getSerializable(ARG_POST);
        }

        initModels();
    }


    @Override
    protected void initModels() {
        mPostModel = mApplication.getPost();
    }

    @Override
    protected void initViews(View view) {
        view.findViewById(R.id.cancel_image).setOnClickListener(this);

        mImageView = (ImageView) view.findViewById(R.id.image_view);

        mTimeText = (TextView) view.findViewById(R.id.time_text);

        String imageUrl = String.format(ApiConfig.GET_IMAGE, mPostModel.getCode());
        ImageLoader.getInstance().displayImage(imageUrl, mImageView);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_record, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        startCountDownTime();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopCountDownTime();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_image:
                stopCountDownTime();
                break;
        }
    }

    private String mFileName = null;
    private MediaRecorder mRecorder = null;
    private MediaPlayer mPlayer = null;

    private boolean mStartRecording = true;
    private boolean mStartPlaying = true;

    private Handler mHandler = new Handler();
    private long mStartRecordTime = 0;
    private long mCurrentRecordTime = 0;
    private CountDownTimer mCountDownTimer;

    private void startRecord() {
        mStartRecording = true;
        mFileName = String.format(Key.FILE_AUDIO_PATH,
                StringUtil.randomString());
        startRecording(mFileName);
    }

    private void finishRecord() {
        mStartRecording = false;
        stopRecording();
    }

    private void startRecording(String fileName) {
        Log.d("Start recording");
        try {
            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mRecorder.setOutputFile(fileName);
            mRecorder.prepare();
            mRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopRecording() {
        Log.d("Stop record");
        try {
            if (mRecorder != null) {
                mRecorder.stop();
                mRecorder.release();
                mRecorder = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cancelRecording() {
        stopRecording();
        mStartRecording = false;
        File file = new File(mFileName);
        try {
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mFileName = "";
    }

    private void startCountDownTime() {
        startRecord();
        mCountDownTimer = new CountDownTimer(MAX_TIME * 1000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                time--;
                mTimeText.setText(String.valueOf(time));
            }

            @Override
            public void onFinish() {
                finishAll();
            }
        };
        mCountDownTimer.start();
    }

    private void stopCountDownTime() {
        if (mCountDownTimer != null)
            mCountDownTimer.cancel();
        finishAll();
    }

    private void finishAll() {
        mTimeText.setText("0");
        finishRecord();
        postComment();
    }


    public void postComment() {
        String messageId = mPostModel.getMessageId();
        String userId = UserModel.getUser().getUserId();
        String receiveId = mPostModel.getReceivedId();
        File file = new File(mFileName);
        RestClient.addComment(messageId, userId, file, receiveId, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                Log.d("response: " + responseBody);
                ToastUtil.show("Your record sent success");
                finishFragment();
            }

            @Override
            public void onFailure(String responseBody, Throwable error) {
                super.onFailure(responseBody, error);
                finishFragment();
            }
        });
    }


}
