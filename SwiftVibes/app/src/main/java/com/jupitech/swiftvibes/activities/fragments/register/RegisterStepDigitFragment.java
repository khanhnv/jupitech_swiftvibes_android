package com.jupitech.swiftvibes.activities.fragments.register;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.apis.loopj.RestClient;
import com.jupitech.swiftvibes.apis.parse.JSONConvert;
import com.jupitech.swiftvibes.models.ResponseModel;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.utils.ToastUtil;
import com.jupitech.swiftvibes.utils.hardware.KeyboardUtil;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;

public class RegisterStepDigitFragment extends BaseFragment {

    private static final String ARG_USER_ID = "user_id";
    private static final String ARG_PHONE = "phone";
    private static final String ARG_COUNTRY_CODE = "country_code";

    private String mUserId;
    private String mPhone;
    private String mCountryCode;

    TextView mPhoneText;
    EditText mDigit1Edt;
    EditText mDigit2Edt;
    EditText mDigit3Edt;
    EditText mDigit4Edt;

    String mDigit1;
    String mDigit2;
    String mDigit3;
    String mDigit4;

    public static RegisterStepDigitFragment newInstance(String userId, String phone, String countryCode) {
        RegisterStepDigitFragment fragment = new RegisterStepDigitFragment();

        Bundle args = new Bundle();
        args.putString(ARG_USER_ID, userId);
        args.putString(ARG_PHONE, phone);
        args.putString(ARG_COUNTRY_CODE, countryCode);

        fragment.setArguments(args);
        return fragment;
    }

    public RegisterStepDigitFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserId = getArguments().getString(ARG_USER_ID);
            mPhone = getArguments().getString(ARG_PHONE);
            mCountryCode = getArguments().getString(ARG_COUNTRY_CODE);
        }
    }

    @Override
    protected void initModels() {

    }

    @Override
    protected void initViews(View view) {
        mPhoneText = (TextView) view.findViewById(R.id.phone_text);

        mDigit1Edt = (EditText) view.findViewById(R.id.digit1_text);
        mDigit2Edt = (EditText) view.findViewById(R.id.digit2_text);
        mDigit3Edt = (EditText) view.findViewById(R.id.digit3_text);
        mDigit4Edt = (EditText) view.findViewById(R.id.digit4_text);

        view.findViewById(R.id.done_text).setOnClickListener(this);

        mPhoneText.setText(mCountryCode + " " + mPhone);

        mDigit1Edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mDigit1Edt.getText().toString().trim().length() == 1){
                    mDigit2Edt.requestFocus();
                    mDigit2Edt.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mDigit2Edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mDigit2Edt.getText().toString().trim().length() == 1){
                    mDigit3Edt.requestFocus();
                    mDigit3Edt.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mDigit3Edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mDigit3Edt.getText().toString().trim().length() == 1){
                    mDigit4Edt.requestFocus();
                    mDigit4Edt.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register_step_digit, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done_text:
                verificationCode();
                break;
        }
    }

    public boolean validateFields() {
        mDigit1Edt.setError(null);
        mDigit2Edt.setError(null);
        mDigit3Edt.setError(null);
        mDigit4Edt.setError(null);

        mDigit1 = mDigit1Edt.getText().toString().trim();
        mDigit2 = mDigit2Edt.getText().toString().trim();
        mDigit3 = mDigit3Edt.getText().toString().trim();
        mDigit4 = mDigit4Edt.getText().toString().trim();

        if (mDigit1.length() == 0) {
            ToastUtil.show("Enter first digit number");
            return false;
        }

        if (mDigit2.length() == 0) {
            ToastUtil.show("Enter second digit number");
            return false;
        }

        if (mDigit1.length() == 0) {
            ToastUtil.show("Enter third digit number");
            return false;
        }

        if (mDigit1.length() == 0) {
            ToastUtil.show("Enter fourth digit number");
            return false;
        }

        return true;
    }

    public void verificationCode() {
        if (!validateFields()) return;
        KeyboardUtil.hideSoftKeyboard(getActivity());

        String userId = mUserId;
        Log.d("UserId: " + userId);

        StringBuilder builder = new StringBuilder();
        builder.append(mDigit1).append(mDigit2).append(mDigit3).append(mDigit4);
        String verification = builder.toString();
        Log.d("Verficiation: " + verification);

        RestClient.verification(userId, verification, new TextHttpResponseHandler() {
            @Override
            public void onFailure(String responseBody, Throwable error) {
                super.onFailure(responseBody, error);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                super.onSuccess(statusCode, headers, responseBody);

                ResponseModel response = JSONConvert.parseResponse(responseBody);
                if(response.getStatus() == 1){
                    UserModel userModel = UserModel.getUser();
                    if(userModel != null) {
                        userModel.setEnterDigit(true);
                        userModel.save();
                    }
                    switchFragment(new RegisterStepNameFragment());
                }else{
                    showAlertDialog("Sorry, the verification code you entered is invalid. Please try again.");
                }
            }
        });
    }
}
