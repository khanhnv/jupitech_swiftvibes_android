package com.jupitech.swiftvibes.activities.fragments.base;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.jupitech.swiftvibes.AppApplication;
import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.base.BaseActionBarActivity;
import com.jupitech.swiftvibes.activities.base.BaseFragmentActivity;
import com.jupitech.swiftvibes.adapters.SpinnerAdapter;
import com.jupitech.swiftvibes.dialogs.AlertDialogFragment;
import com.jupitech.swiftvibes.listeners.AlertListener;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.utils.preference.PreferenceUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public abstract class BaseFragment extends Fragment implements View.OnClickListener {

    protected BaseActionBarActivity mBaseActionBarActivity;

    protected BaseFragmentActivity mBaseFragmentActivity;

    protected PreferenceUtil mPreferenceManager;

    protected UserModel mUserModel;

    protected AppApplication mApplication;

    protected AlertDialogFragment mAlertDialog;

    protected FragmentManager mFragmentManager;

    protected ImageLoader mImageLoader = ImageLoader.getInstance();

    protected Handler mHandler = new Handler();

    public DisplayImageOptions mOptions;

    public ProgressDialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity() instanceof BaseActionBarActivity) {
            mBaseActionBarActivity = (BaseActionBarActivity) getActivity();
        } else {
            mBaseFragmentActivity = (BaseFragmentActivity) getActivity();
        }

        mPreferenceManager = PreferenceUtil.getInstance(AppApplication.get());

        mUserModel = mPreferenceManager.getUserInfo();

        mApplication = (AppApplication) getActivity().getApplication();

        mFragmentManager = getFragmentManager();

        mOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_stub)
                .showImageForEmptyUri(R.drawable.noimage)
                .showImageOnFail(R.drawable.noimage).cacheInMemory(true)
                .cacheOnDisk(true).bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * Switch fragment
     *
     * @param fragment
     */
    protected void switchFragment(Fragment fragment) {
        switchFragment(fragment, false);
    }

    /**
     * Switch fragment
     *
     * @param fragment
     */
    protected void switchFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(R.id.container, fragment);
        if (addToBackStack)
            transaction.addToBackStack(null);
        transaction.commit();
    }

    protected void switchFragment(String tag, Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(R.id.container, fragment, tag);
        if (addToBackStack)
            transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * @param title
     */
    protected void setTitle(String title) {
        // MainActivity mainActivity = (MainActivity) getActivity();
        // mainActivity.setTitleText(title);
    }

    /**
     * Init all model when onCreate activity here
     */
    protected abstract void initModels();

    /**
     * Init all views when onCreate activity here
     */
    protected abstract void initViews(View view);

    /**
     * Remove previous show dialog fragment by tag
     *
     * @param tag tag of dialog fragment
     */
    protected void removePreviousDialog(String tag) {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction. We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        Fragment prev = mFragmentManager.findFragmentByTag(tag);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.commit();
    }

    /**
     * show alert message
     *
     * @param message
     */
    public void showAlertDialog(String message) {
        // clear all state previous
        removePreviousDialog("alert_dialog");
        mAlertDialog = null;
        // create dialog
        mAlertDialog = AlertDialogFragment.newInstance(
                getString(R.string.app_name), message,
                getString(android.R.string.ok), getString(android.R.string.ok));
        mAlertDialog.showOnlyOneButton(true);
        // show dialog
        mAlertDialog.show(mFragmentManager, "alert_dialog");
    }

    /**
     * show alert
     *
     * @param title
     * @param message
     * @param leftText
     * @param rightText
     * @param listener
     */
    public void showAlertDialog(String title, String
            message, String leftText, String rightText, AlertListener listener) {
        // clear all state previous
        removePreviousDialog("alert_dialog");
        mAlertDialog = null;
        // create dialog
        mAlertDialog = AlertDialogFragment.newInstance(title, message,
                leftText, rightText, listener);
        // show dialog
        mAlertDialog.show(mFragmentManager, "alert_dialog");
    }

    /**
     * Finish current fragment
     */
    public void finishFragment() {
        try {
            mFragmentManager.popBackStack();
        } catch (Exception e) {
            Log.e(e.getMessage());
        }
    }

    /**
     * get position in array
     *
     * @param value
     * @param array
     * @return
     */
    public int getPositionInArray(String value, String[] array) {
        if (value == null)
            return 0;
        int position = 0;
        final int length = array.length;
        for (int i = 0; i < length; i++) {
            if (value.equals(array[i])) {
                position = i;
                break;
            }
        }
        return position;
    }

    /**
     * Create spinner adapter for each spinner
     *
     * @param spinner
     * @param listItem
     * @param itemSelectedListener
     */
    public void createSpinnerAdapter(Spinner spinner, String[] listItem,
                                     AdapterView.OnItemSelectedListener itemSelectedListener) {
        SpinnerAdapter mySpinnerAdapter = new SpinnerAdapter(getActivity(),
                R.layout.item_spinner, listItem);
        mySpinnerAdapter
                .setDropDownViewResource(R.layout.item_spinner_dropdown);
        spinner.setAdapter(mySpinnerAdapter);
        spinner.setOnItemSelectedListener(itemSelectedListener);
    }

    /**
     * Set selected position for spinner
     *
     * @param spinner
     * @param position
     */
    public void setPositionSelectedSpinner(Spinner spinner, int position) {
        spinner.setSelection(position, true);
    }

    public void showProgress() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog = new ProgressDialog(getActivity());
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.setMessage("Loading...");
                mProgressDialog.show();
            }
        });
    }

    public void hideProgress() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog.hide();
                mProgressDialog = null;
            }
        });

    }
}
