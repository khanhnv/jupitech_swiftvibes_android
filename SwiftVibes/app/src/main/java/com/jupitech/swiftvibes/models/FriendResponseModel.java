package com.jupitech.swiftvibes.models;

import java.util.ArrayList;

/**
 * Created by brucenguyen on 10/15/14.
 */
public class FriendResponseModel {

    private int status;
    private ArrayList<UserModel> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<UserModel> getData() {
        return data;
    }

    public void setData(ArrayList<UserModel> data) {
        this.data = data;
    }


    @Override
    public String toString() {
        return "FriendResponseModel{" +
                "status=" + status +
                ", data=" + data +
                '}';
    }
}
