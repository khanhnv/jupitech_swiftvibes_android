package com.jupitech.swiftvibes.providers.calllog;

import android.app.Activity;
import android.os.Bundle;
import android.provider.CallLog.Calls;

import com.jupitech.swiftvibes.utils.Log;

import java.util.ArrayList;
import java.util.Calendar;


public class TestCallLogActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		CallLogFunction function = new CallLogFunction(getBaseContext());
		MyCallLog callLog = new MyCallLog(0, Calendar.getInstance()
				.getTimeInMillis(), "123456", "Test", Calls.INCOMING_TYPE, 0);
		function.insertToCallLog(callLog);
		function.deleteCallLog("123456");
		function.deleteLastCallLog();
		ArrayList<MyCallLog> lst = function.getCallLog();
		if (!lst.isEmpty()) {
			for (MyCallLog log : lst) {
				Log.d(log.toString());
			}
		}
	}
}
