package com.jupitech.swiftvibes.socials.googleplus;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusClient.OnPeopleLoadedListener;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import com.jupitech.swiftvibes.utils.Log;

import java.util.ArrayList;
import java.util.List;

public class GoogleplusUtil implements GoogleplusState {
	public static final int REQUEST_CODE_SIGN_IN = 1074;
	public static final int REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES = 1075;
	public static final int REQUEST_SHARE = 1076;
	public static final int REQUEST_SELECT_MEDIA = 1077;
	public static final String LABEL_VIEW_ITEM = "VIEW_ITEM";

	private Activity activity;
	private PlusClient plusClient;
	private ConnectionResult connectionResult;
	private PlusClient.ConnectionCallbacks connectCallback;
	private PlusClient.ConnectionCallbacks ownerConnectCallback;
	private PlusClient.OnConnectionFailedListener connectFailedCallback;
	private PlusClient.OnConnectionFailedListener ownerConnectFailedCallback;
	private PlusClient.OnAccessRevokedListener accessRevokedListener;
	private PlusClient.OnAccessRevokedListener ownerAccessRevokedListener;
	private GoogleplusCallback callBack;

	// share
	private boolean sharing = false;
	private String message;
	private String deepLink;
	private String deepLinkId;

	// load people
	private boolean loadingPeople = false;

	public GoogleplusUtil(Activity activity) {
		this.activity = activity;
	}

	public PlusClient.ConnectionCallbacks getOwnerConnectCallback() {
		return ownerConnectCallback;
	}

	public void setOwnerConnectCallback(
			PlusClient.ConnectionCallbacks ownerConnectCallback) {
		this.ownerConnectCallback = ownerConnectCallback;
	}

	public PlusClient.OnConnectionFailedListener getOwnerConnectFailedCallback() {
		return ownerConnectFailedCallback;
	}

	public void setOwnerConnectFailedCallback(
			PlusClient.OnConnectionFailedListener ownerConnectFailedCallback) {
		this.ownerConnectFailedCallback = ownerConnectFailedCallback;
	}

	public PlusClient.OnAccessRevokedListener getOwnerAccessRevokedListener() {
		return ownerAccessRevokedListener;
	}

	public void setOwnerAccessRevokedListener(
			PlusClient.OnAccessRevokedListener ownerAccessRevokedListener) {
		this.ownerAccessRevokedListener = ownerAccessRevokedListener;
	}

	@Override
	public void onCreate() {

		connectCallback = new PlusClient.ConnectionCallbacks() {

			@Override
			public void onConnected(Bundle bundle) {
				if (ownerConnectCallback != null) {
					ownerConnectCallback.onConnected(bundle);
				}
				if (sharing) {
					sharing = false;
					postStatusWithLink(message, deepLink, deepLinkId, callBack);
				}
				if (loadingPeople) {
					loadingPeople = false;
					getPeopleInCircle(callBack);
				}
			}

			@Override
			public void onDisconnected() {
				if (ownerConnectCallback != null) {
					ownerConnectCallback.onDisconnected();
				}
				plusClient.connect();
			}
		};

		connectFailedCallback = new PlusClient.OnConnectionFailedListener() {

			@Override
			public void onConnectionFailed(ConnectionResult result) {
				connectionResult = result;
				if (ownerConnectFailedCallback != null) {
					ownerConnectFailedCallback.onConnectionFailed(result);
				}
			}

		};

		accessRevokedListener = new PlusClient.OnAccessRevokedListener() {

			@Override
			public void onAccessRevoked(ConnectionResult result) {
				if (ownerAccessRevokedListener != null) {
					ownerAccessRevokedListener.onAccessRevoked(result);
				}
				plusClient.connect();
			}
		};

		plusClient = new PlusClient.Builder(activity, connectCallback,
				connectFailedCallback).setActions(MomentUtil.ACTIONS).build();
	}

	@Override
	public void onStart() {
		plusClient.connect();
	}

	@Override
	public void onStop() {
		plusClient.disconnect();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_SIGN_IN
				|| requestCode == REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES) {
			if (resultCode == Activity.RESULT_OK && !plusClient.isConnected()
					&& !plusClient.isConnecting()) {
				// This time, connect should succeed.
				plusClient.connect();
			}
		} else if (requestCode == REQUEST_SHARE) {
			if (callBack != null) {
				callBack.onShareResult((resultCode == Activity.RESULT_OK));
				callBack = null;
			}
		} else if (requestCode == REQUEST_SELECT_MEDIA) {
			if (resultCode == Activity.RESULT_OK) {
				Uri selectedMedia = data.getData();
				postMedia(message, selectedMedia.toString(), callBack);
			}

		}
	}

	public void login() {

		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(activity);

		if (resultCode != ConnectionResult.SUCCESS) {
			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					activity, 0);
			dialog.show();
		}

		if (connectionResult == null) {
			plusClient.connect();
		}
		try {
			connectionResult.startResolutionForResult(activity,
					REQUEST_CODE_SIGN_IN);
		} catch (IntentSender.SendIntentException e) {
			// Fetch a new result to start.
			plusClient.connect();
		}
	}

	/**
	 * Clear account is connected to app
	 */
	public void logout() {
		if (plusClient.isConnected()) {
			plusClient.clearDefaultAccount();
			plusClient.disconnect();
			plusClient.connect();
		}
	}

	/**
	 * Remove access token and disconnect to app
	 */
	public void forceLogout() {
		if (plusClient.isConnected()) {
			plusClient.revokeAccessAndDisconnect(accessRevokedListener);
		}
	}

	public boolean isLoggedIn() {
		if (plusClient != null && plusClient.isConnected()) {
			return true;
		}
		return false;
	}

	public Person getUserInfo() {
		return plusClient.getCurrentPerson();
	}

	public void postStatus(String message, GoogleplusCallback callBack) {
		this.callBack = callBack;
		Intent shareIntent = new PlusShare.Builder(activity)
				.setType("text/plain").setText(message).getIntent();
		activity.startActivityForResult(shareIntent, REQUEST_SHARE);
	}

	/**
	 * Post status to google plus with link
	 * 
	 * @param message
	 *            content of status
	 * @param deepLink
	 *            url for desktop use
	 * @param deepLinkId
	 *            deep-link id for mobile use
	 */
	public void postStatusWithLink(String message, String deepLink,
			String deepLinkId, GoogleplusCallback callBack) {
		if (plusClient == null) {
			Log.e("please initialize Google plus client");
			return;
		}
		if (TextUtils.isEmpty(deepLink)) {
			Log.e("deep link is empty");
			return;
		}
		if (TextUtils.isEmpty(deepLinkId)) {
			Log.e("deep link id is empty");
			return;
		}

		if (!plusClient.isConnected()) {
			// login before post status
			// post status implemented at onConnected
			sharing = true;
			if (!plusClient.isConnecting()) {
				this.message = message;
				this.deepLink = deepLink;
				this.deepLinkId = deepLinkId;
				this.callBack = callBack;
				login();
			}
			return;
		}
		String action = "/?view=true";

		Uri callToActionUrl = Uri.EMPTY;
		Uri contentUrl = Uri.EMPTY;
		callToActionUrl = Uri.parse(deepLink + action);
		contentUrl = Uri.parse(deepLink);

		String callToActionDeepLinkId = "";
		callToActionDeepLinkId = deepLinkId + action;

		// Create an interactive post builder.
		PlusShare.Builder builder = new PlusShare.Builder(activity, plusClient);

		// Set call-to-action metadata.
		builder.addCallToAction(LABEL_VIEW_ITEM, callToActionUrl,
				callToActionDeepLinkId);

		// Set the target url (for desktop use).
		builder.setContentUrl(contentUrl);

		// Set the target deep-link ID (for mobile use).
		builder.setContentDeepLinkId(deepLinkId, null, null, null);

		// Set the pre-filled message.
		builder.setText(message);

		Intent requestPostIntent = builder.getIntent();

		activity.startActivityForResult(requestPostIntent, REQUEST_SHARE);
	}

	/**
	 * post image or video to google plus
	 * 
	 * @param message
	 *            content of status
	 * @param pathFile
	 *            path of file image or video
	 * @param callBack
	 *            Called to receiver result share success or failed
	 */
	public void postMedia(String message, String pathFile,
			GoogleplusCallback callBack) {

		this.callBack = callBack;
		Uri uriFile = Uri.parse(pathFile);
		ContentResolver cr = activity.getContentResolver();
		String mime = cr.getType(uriFile);

		PlusShare.Builder share = new PlusShare.Builder(activity);
		share.setText(message);
		share.addStream(uriFile);
		share.setType(mime);
		activity.startActivityForResult(share.getIntent(), REQUEST_SHARE);
	}

	/**
	 * Open expoler to select image and post to google plus
	 * 
	 * @param message
	 * @param callBack
	 */
	public void postMediaFromExpoler(String message, GoogleplusCallback callBack) {
		this.message = message;
		this.callBack = callBack;
		Intent photoPicker = new Intent(Intent.ACTION_PICK);
		photoPicker.setType("video/*, image/*");
		activity.startActivityForResult(photoPicker, REQUEST_SELECT_MEDIA);
	}

	/**
	 * Get all peoples in circle
	 * 
	 * @param loadCallback
	 */
	public void getPeopleInCircle(final GoogleplusCallback loadCallback) {
		if (plusClient == null) {
			Log.e("please initialize Google plus client");
			return;
		}
		if (!plusClient.isConnected()) {
			// login before load people
			// load people implemented at onConnected
			loadingPeople = true;
			if (!plusClient.isConnecting()) {
				this.callBack = loadCallback;
				login();
			}
			return;
		}

		final List<Person> persons = new ArrayList<Person>();
		plusClient.loadVisiblePeople(new OnPeopleLoadedListener() {

			@Override
			public void onPeopleLoaded(ConnectionResult status,
					PersonBuffer personBuffer, String nextPageToken) {
				switch (status.getErrorCode()) {
				case ConnectionResult.SUCCESS:
					try {
						for (Person person : personBuffer) {
							persons.add(person);
						}
					} finally {
						personBuffer.close();
					}
					break;
				default:
					break;
				}
				if (loadCallback != null) {
					loadCallback.onLoadPersonInCircle(persons);
				}
			}
		}, null);
	}

}