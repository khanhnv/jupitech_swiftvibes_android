package com.jupitech.swiftvibes.listeners;

import android.app.Dialog;

public interface AlertListener {

    void onLeftClick(Dialog dialog);

    void onRightClick(Dialog dialog);
}
