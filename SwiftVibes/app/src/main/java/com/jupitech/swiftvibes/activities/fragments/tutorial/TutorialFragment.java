package com.jupitech.swiftvibes.activities.fragments.tutorial;

import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.adapters.TutorialAdapter;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.jupitech.swiftvibes.activities.fragments.tutorial.TutorialFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.jupitech.swiftvibes.activities.fragments.tutorial.TutorialFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class TutorialFragment extends BaseFragment {

    ViewPager mViewPager;
    CirclePageIndicator mCirclePageIndicator;
    TutorialAdapter mAdapter;

    public TutorialFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    protected void initModels() {

    }

    @Override
    protected void initViews(View view) {
        mViewPager = (ViewPager)view.findViewById(R.id.pager);

        mAdapter = new TutorialAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);


        mCirclePageIndicator = (CirclePageIndicator)view.findViewById(R.id.indicator);
        mCirclePageIndicator.setViewPager(mViewPager);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tutorial, container, false);
        initViews(view);
        return view;
    }


    @Override
    public void onClick(View v) {
    }


}
