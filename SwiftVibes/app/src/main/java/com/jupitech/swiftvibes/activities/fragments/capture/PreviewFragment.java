package com.jupitech.swiftvibes.activities.fragments.capture;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jupitech.swiftvibes.AppApplication;
import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.SentActivity;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.configs.Key;
import com.jupitech.swiftvibes.utils.ToastUtil;
import com.jupitech.swiftvibes.utils.bitmap.BitmapUtil;
import com.jupitech.swiftvibes.utils.intent.LaunchIntent;
import com.jupitech.swiftvibes.utils.text.StringUtil;

import java.io.File;
import java.io.IOException;

public class PreviewFragment extends BaseFragment {

    public static final String PARAM_IMAGE = "image";
    ImageView mImageView;
    String mPath;

    public static PreviewFragment newInstance(String path) {
        PreviewFragment fragment = new PreviewFragment();

        Bundle extras = new Bundle();
        extras.putString(PARAM_IMAGE, path);

        fragment.setArguments(extras);

        return fragment;
    }

    public PreviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPath = getArguments().getString(PARAM_IMAGE);
        }
        initModels();
    }

    @Override
    protected void initModels() {

    }

    @Override
    protected void initViews(View view) {
        mImageView = (ImageView) view.findViewById(R.id.image_view);

        view.findViewById(R.id.save_image).setOnClickListener(this);
        view.findViewById(R.id.sent_image).setOnClickListener(this);
        view.findViewById(R.id.cancel_image).setOnClickListener(this);

        Bitmap bitmap = BitmapUtil.decodeBitmapFromFile(getActivity(), 2, mPath);
        if (bitmap != null) {
            mImageView.setImageBitmap(bitmap);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_preview, container, false);
        initViews(view);
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_image:
                writeBitmap();
                break;
            case R.id.sent_image:
//                switchFragment(SendFragment.newInstance(mPath));
                mApplication.setImagePath(mPath);
                LaunchIntent.start(getActivity(), SentActivity.class);
                break;
            case R.id.cancel_image:
                finishFragment();
                switchFragment(new CaptureFragment());
                break;
        }
    }

    private void writeBitmap() {
        addImageToGallery(mPath, AppApplication.get());

        Bitmap bitmap = BitmapUtil.decodeBitmapFromFile(getActivity(), 1, mPath);
        if (bitmap != null) {

            Boolean isSDPresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
            if(isSDPresent) {
                String path = String.format(Key.IMAGE_PATH, StringUtil.convertNowToString(StringUtil.DATE_FORMAT_8));
                try {
                    BitmapUtil.writeBitmap(bitmap, path, 100);
                    ToastUtil.show("Save image to " + path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                File galleryPath = Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES);
                File imagePath = new File(galleryPath, StringUtil.convertNowToString(StringUtil.DATE_FORMAT_8) + ".jpg");
                try {
                    BitmapUtil.writeBitmap(bitmap, imagePath.getAbsolutePath(), 100);
                    ToastUtil.show("Save image to " + imagePath.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void addImageToGallery(final String filePath, final Context context) {

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);
        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }
}
