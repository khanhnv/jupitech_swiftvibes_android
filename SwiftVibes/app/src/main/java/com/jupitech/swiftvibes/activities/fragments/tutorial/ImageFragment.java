package com.jupitech.swiftvibes.activities.fragments.tutorial;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;


public class ImageFragment extends BaseFragment {

    ImageView mImageView;
    int mImageId;

    public ImageFragment() {
        // Required empty public constructor
    }

    public static ImageFragment newInstance(int imageId){
        ImageFragment fragment = new ImageFragment();
        Bundle extras = new Bundle();
        extras.putInt("image_id", imageId);
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mImageId = getArguments().getInt("image_id");
        }
    }

    @Override
    protected void initModels() {

    }

    @Override
    protected void initViews(View view) {
        mImageView = (ImageView)view.findViewById(R.id.image_view);
        mImageView.setImageResource(mImageId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_image, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onClick(View v) {
    }
}
