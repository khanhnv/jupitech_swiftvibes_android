package com.jupitech.swiftvibes.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

/**
 * BaseRelativeLayout
 *
 * @author bruce
 */
public class BaseRelativeLayout extends RelativeLayout {
    /**
     * Constructor
     *
     * @param context
     */
    public BaseRelativeLayout(Context context) {
        super(context);
    }

    /**
     * Constructor
     *
     * @param context
     * @param attrs
     */
    public BaseRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Initial inflater layout
     *
     * @param reID
     * @param context
     */
    protected void initLayout(Context context, int reID) {
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(reID, this, true);
    }
}