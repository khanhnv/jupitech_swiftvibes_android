package com.jupitech.swiftvibes.activities;

import android.os.Bundle;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.base.BaseActionBarActivity;
import com.jupitech.swiftvibes.activities.fragments.register.RegisterStepNameFragment;
import com.jupitech.swiftvibes.activities.fragments.register.RegisterStepPhoneFragment;
import com.jupitech.swiftvibes.models.UserModel;

public class RegisterActivity extends BaseActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        UserModel userModel = UserModel.getUser();
        if(userModel != null && userModel.isEnterDigit()){
            switchContent(R.id.container, new RegisterStepNameFragment());
        }else {
            switchContent(R.id.container, new RegisterStepPhoneFragment());
        }
    }

    @Override
    protected void initModels() {
    }

    @Override
    protected void initViews() {
    }


    @Override
    public void onBackPressed() {
        return;
    }
}
