package com.jupitech.swiftvibes.socials.twitter;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.text.TextUtils;

import com.jupitech.swiftvibes.configs.TwitterConfig;
import com.jupitech.swiftvibes.utils.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import twitter4j.AccountSettings;
import twitter4j.Friendship;
import twitter4j.IDs;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Relationship;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.media.ImageUpload;
import twitter4j.media.ImageUploadFactory;
import twitter4j.media.MediaProvider;


public class TwitterUtil implements TwitterState {

	public final static MediaProvider DEFAULT_HOST = MediaProvider.TWITTER;

	public static TwitterUtil instance = null;

	public Twitter mTwitter;
	protected Configuration mConfig;
	protected ImageUploadFactory mImageUploadFactory;
	protected ImageUpload mImageUpload;
	protected RequestToken mRequestToken;
	protected Activity mActivity;
	protected SharedPreferences mSharedPreferences;

	protected String mOauthAccessToken = "";
	protected String mOauthAccessTokenSecret = "";

	/**
	 * Method is called onCreate state of activity
	 */
	@Override
	public void onCreate(Activity activity) {
		// only update new activity for singleton when activity onCreate
		updateActivity(activity);
		Uri uri = mActivity.getIntent().getData();
		if (uri != null
				&& uri.toString().startsWith(TwitterConfig.CALLBACK_URL)) {
			/*
			 * After user login from web browser and back to main activity Save
			 * access token to preference
			 */
			String verifier = uri
					.getQueryParameter(TwitterConfig.IEXTRA_OAUTH_VERIFIER);
			try {
				AccessToken accessToken = mTwitter.getOAuthAccessToken(
						mRequestToken, verifier);
				Log.d("accessToken=" + accessToken.getToken() + ", "
                        + accessToken.getTokenSecret());
				Editor e = mSharedPreferences.edit();
				e.putString(TwitterConfig.PREF_KEY_TOKEN,
						accessToken.getToken());
				e.putString(TwitterConfig.PREF_KEY_SECRET,
						accessToken.getTokenSecret());
				e.commit();
			} catch (Exception e) {
				Log.e( e.toString());
			}
		} else {
			Log.d( "uri null");
		}
	}

	/**
	 * Method is called onResume state of activity
	 */
	@Override
	public void onResume() {
		/*
		 * Init config twitter from access token
		 */
		mOauthAccessToken = mSharedPreferences.getString(
				TwitterConfig.PREF_KEY_TOKEN, "");
		mOauthAccessTokenSecret = mSharedPreferences.getString(
				TwitterConfig.PREF_KEY_SECRET, "");
		Log.d( "accessToken from preference = " + mOauthAccessToken + ", "
				+ mOauthAccessTokenSecret);
		ConfigurationBuilder confbuilder = new ConfigurationBuilder();
		mConfig = confbuilder.setOAuthConsumerKey(TwitterConfig.CONSUMER_KEY)
				.setOAuthConsumerSecret(TwitterConfig.CONSUMER_SECRET)
				.setOAuthAccessToken(mOauthAccessToken)
				.setOAuthAccessTokenSecret(mOauthAccessTokenSecret).build();
		mTwitter = new TwitterFactory(mConfig).getInstance();
	}

	public TwitterUtil(Activity activity) {
		mActivity = activity;
		mSharedPreferences = mActivity.getSharedPreferences(
				TwitterConfig.PREFERENCE_NAME, Activity.MODE_PRIVATE);
		ConfigurationBuilder configBuilder = new ConfigurationBuilder();
		configBuilder.setOAuthConsumerKey(TwitterConfig.CONSUMER_KEY);
		configBuilder.setOAuthConsumerSecret(TwitterConfig.CONSUMER_SECRET);
		mConfig = configBuilder.build();
		mTwitter = new TwitterFactory(mConfig).getInstance();
	}

	public static TwitterUtil getInstance(Activity activity) {
		if (instance == null) {
			instance = new TwitterUtil(activity);
		}
		return instance;
	}

	/**
	 * Update new activity for singleton
	 * 
	 * @param activity
	 */
	private void updateActivity(Activity activity) {
		this.mActivity = activity;
		this.mSharedPreferences = activity.getSharedPreferences(
				TwitterConfig.PREFERENCE_NAME, Activity.MODE_PRIVATE);
	}

	/**
	 * Next to web browser to login activity
	 * 
	 * @return true if login success
	 */
	public boolean login() {
		ConfigurationBuilder configBuilder = new ConfigurationBuilder();
		configBuilder.setOAuthConsumerKey(TwitterConfig.CONSUMER_KEY);
		configBuilder.setOAuthConsumerSecret(TwitterConfig.CONSUMER_SECRET);
		mConfig = configBuilder.build();
		mTwitter = new TwitterFactory(mConfig).getInstance();
		try {
			mRequestToken = mTwitter
					.getOAuthRequestToken(TwitterConfig.CALLBACK_URL);
			mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri
					.parse(mRequestToken.getAuthenticationURL())));
			return true;
		} catch (TwitterException te) {
			Log.e( "Can't get request token");
			Log.e( te.toString());
		}
		return false;
	}

	public void disconnectTwitter() {
		// Clear token, secret from peferences
		Editor editor = mSharedPreferences.edit();
		editor.remove(TwitterConfig.PREF_KEY_TOKEN);
		editor.remove(TwitterConfig.PREF_KEY_SECRET);
		editor.commit();
	}

	/**
	 * Check twitter is connected
	 * 
	 * @return
	 */
	public boolean isConnected() {
		return mSharedPreferences.getString(TwitterConfig.PREF_KEY_TOKEN, null) != null;
	}

	/******************************************* Account Util *******************************/

	/**
	 * Get info of user Use this method to test if supplied user credentials are
	 * valid.
	 * 
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public User getInfoUser() {
		User user = null;
		try {
			user = mTwitter.verifyCredentials();
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return user;
	}

	/**
	 * Get AccountSetting
	 * 
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public AccountSettings getAccountSetting() {
		AccountSettings settings = null;
		try {
			settings = mTwitter.getAccountSettings();
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return settings;
	}

	/**
	 * 
	 * @param name
	 *            Optional. Maximum of 20 characters.
	 * @param url
	 *            Optional. Maximum of 100 characters. Will be prepended with
	 *            "http://" if not present.
	 * @param location
	 *            Optional. Maximum of 30 characters. The contents are not
	 *            normalized or geocoded in any way.
	 * @param description
	 *            Optional. Maximum of 160 characters.
	 * 
	 */
	public boolean updateProfile(String name, String url, String location,
			String description) {
		try {
			mTwitter.updateProfile(name, url, location, description);
			return true;
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return false;
	}

	/**
	 * Updates profile image.
	 * 
	 * @param filePath
	 *            Must be a valid GIF, JPG, or PNG image of less than 800
	 *            kilobytes in size
	 * @throws twitter4j.TwitterException
	 */
	public boolean updateProfileImage(String filePath) {
		if (TextUtils.isEmpty(filePath)) {
			Log.e( "file path is empty");
			return false;
		}
		File file = new File(filePath);
		if (!file.exists()) {
			Log.e( "file isn't exist");
			return false;
		}
		try {
			mTwitter.updateProfileImage(new File(filePath));
			return true;
		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return false;
	}

	/**
	 * Update profile background image with tiled
	 * 
	 * @param filePath
	 * @param tiled
	 *            If set true, background image is displayed titled
	 * @return
	 */
	public boolean updateProfileBackgroundImage(String filePath, boolean tiled) {

		if (TextUtils.isEmpty(filePath)) {
			Log.e( "file path is empty");
			return false;
		}
		File file = new File(filePath);
		if (!file.exists()) {
			Log.e( "file isn't exist");
			return false;
		}

		try {
			mTwitter.updateProfileBackgroundImage(new File(filePath), tiled);
			return true;
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return false;
	}

	public boolean updateProfileColor(String profileBackgroundColor,
			String profileTextColor, String profileLinkColor,
			String profileSidebarFillColor, String profileSidebarBorderColor) {

		try {
			mTwitter.updateProfileColors(profileBackgroundColor,
					profileTextColor, profileLinkColor,
					profileSidebarFillColor, profileSidebarBorderColor);
			return true;
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return false;
	}

	/************************************ User Util **************************************/

	/**
	 * Lockup user has name in array screenNames
	 * 
	 * @param screenNames
	 *            size < 100
	 * 
	 * @return 100 first users
	 */
	public List<User> lookup(String[] screenNames) {
		try {
			ResponseList<User> users = mTwitter.lookupUsers(screenNames);
			return users;
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return null;
	}

	/**
	 * Lockup user has name in array ids
	 * 
	 * @param ids
	 *            size < 100
	 * @return 100 first users
	 * @throws twitter4j.TwitterException
	 */
	public List<User> lookup(long[] ids) {
		try {
			ResponseList<User> users = mTwitter.lookupUsers(ids);
			return users;
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return null;
	}

	/**
	 * Search with screen name
	 * 
	 * @param screenName
	 * @param page
	 *            page is positive inteher
	 * @return max users return is 1000 null if not found
	 * @throws twitter4j.TwitterException
	 */
	public List<User> search(String screenName, int page) {
		if (page < 1) {
			Log.e( "page is positive integer");
			return null;
		}
		try {
			ResponseList<User> users = mTwitter.searchUsers(screenName, page);
			return users;
		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return null;
	}

	/**
	 * Show info with screen name
	 * 
	 * @param screenName
	 * @return first user has screen name
	 * @throws twitter4j.TwitterException
	 */
	public User showUserFromName(String screenName) {
		User user = null;
		try {
			user = mTwitter.showUser(screenName);
			if (user != null && user.getStatus() == null) {
				Log.d( "this user is protected");
			}
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return user;
	}

	/**
	 * Show info user from id
	 * 
	 * @param id
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public User showUserFromId(Long id) {
		User user = null;
		try {
			user = mTwitter.showUser(id);
			if (user != null && user.getStatus() == null) {
				Log.d( "this user is protected");
			}
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return user;
	}

	/****************************** Status Util **********************************/

	/**
	 * Update status
	 * 
	 * @param message
	 *            content status
	 * @return Status is updated
	 * @throws twitter4j.TwitterException
	 */
	public Status updateStatus(String message) {
		try {
			Status status = mTwitter.updateStatus(message);
			return status;
		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return null;
	}

	/**
	 * Delete status
	 * 
	 * @param idStatus
	 *            id of status to delete
	 * @return status is deleted
	 * @throws twitter4j.TwitterException
	 */
	public Status destroyStatus(Long idStatus) {
		Status status = null;
		try {
			status = mTwitter.destroyStatus(idStatus);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return status;
	}

	/**
	 * Show status
	 * 
	 * @param idStatus
	 *            id of status to show
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public Status showStatus(Long idStatus) {
		Status status = null;
		try {
			status = mTwitter.showStatus(idStatus);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return status;
	}

	/**
	 * Reetweet a status
	 * 
	 * @param idStatus
	 *            id of status to reetweet
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public Status reetweetStatus(Long idStatus) {
		Status status = null;
		try {
			status = mTwitter.retweetStatus(idStatus);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return status;
	}

	/**
	 * Shows up to 100 of the first retweets of a given tweet.
	 * 
	 * @param idStatus
	 *            id of status to get reetweet
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<Status> getRetweets(Long idStatus) {
		List<Status> status = new ArrayList<Status>();
		try {
			status = mTwitter.getRetweets(idStatus);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return status;
	}

	/******************************** Timeline Util *********************************/

	/**
	 * Get 20 most recent status on timeline
	 * 
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<Status> getTweetFromTimeLine() {

		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getHomeTimeline();
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;

	}

	/**
	 * Get all status in sepcificed page
	 * 
	 * @param page
	 *            page is positive integer
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<Status> getTweetFromTimeLine(int page) {
		if (page < 1) {
			Log.e( "page is positive integer");
			return null;
		}
		Paging paging = new Paging(page);
		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getHomeTimeline(paging);
		} catch (TwitterException e) {
			Log.d( e.toString());
		}
		return statuses;
	}

	/**
	 * Get 20 most recent tweet of user authentication
	 */
	public List<Status> getTweetOfUser() {
		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getUserTimeline();
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;
	}

	/**
	 * Get tweet of user authentication in specificed page
	 * 
	 * @param page
	 *            page is positive integer
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<Status> getTweetOfUser(int page) {
		List<Status> statuses = new ArrayList<Status>();
		if (page < 1) {
			Log.e( "page is positive integer");
			return null;
		}
		Paging paging = new Paging(page);
		try {
			statuses = mTwitter.getUserTimeline(paging);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;
	}

	/**
	 * Get tweet of user specificed via screen name
	 * 
	 * @param screenName
	 * @return tweets at first page
	 */
	public List<Status> getTweetOfUser(String screenName) {
		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getUserTimeline(screenName);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;
	}

	/**
	 * Get tweet of user specificed via id
	 * 
	 * @param id
	 * @return tweets at fist page
	 */
	public List<Status> getTweetOfUser(long id) {
		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getUserTimeline(id);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;
	}

	/**
	 * Get tweet in page of user specificed via screen name
	 * 
	 * @param screenName
	 * @param page
	 * @return
	 */
	public List<Status> getTweetOfUser(String screenName, int page) {
		if (page < 1) {
			Log.e( "page is positive integer");
			return null;
		}
		Paging paging = new Paging(page);
		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getUserTimeline(screenName, paging);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;
	}

	/**
	 * Get tweet in page of user specificed via id
	 * 
	 * @param id
	 * @param page
	 * @return
	 */
	public List<Status> getTweetOfUser(long id, int page) {
		if (page < 1) {
			Log.e( "page is positive integer");
			return null;
		}
		Paging paging = new Paging(page);
		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getUserTimeline(id, paging);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;
	}

	/**
	 * Get tweet contaning a user screen names
	 * 
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<Status> getMentionTweetOfUser() throws TwitterException {
		List<Status> statuses = mTwitter.getMentionsTimeline();
		return statuses;
	}

	/**
	 * Get tweet contaning a user screen names at page
	 * 
	 * @param page
	 *            page is positive integer
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<Status> getMentionTweetOfUser(int page) throws TwitterException {
		if (page < 1) {
			Log.e( "page is positive integer");
			return null;
		}
		Paging paging = new Paging(page);
		List<Status> statuses = mTwitter.getMentionsTimeline(paging);
		return statuses;
	}

	/******************************* Follow Util ************************************/

	/**
	 * Get ids of follower of user authentication each request
	 * 
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<Long> getIdFollower() {
		long cursor = -1;
		IDs ids;
		List<Long> resultIds = new ArrayList<Long>();
		try {
			do {
				ids = mTwitter.getFollowersIDs(cursor);
				for (long id : ids.getIDs()) {
					resultIds.add(id);
				}
			} while ((cursor = ids.getNextCursor()) != 0);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return resultIds;
	}

	/**
	 * Get ids of follower of specificied user via screen name
	 * 
	 * @param screenName
	 *            screen name of user
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<Long> getIdFollower(String screenName) {
		long cursor = -1;
		IDs ids;
		List<Long> resultIds = new ArrayList<Long>();
		try {
			do {
				ids = mTwitter.getFollowersIDs(screenName, cursor);
				for (long id : ids.getIDs()) {
					resultIds.add(id);
				}
			} while ((cursor = ids.getNextCursor()) != 0);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return resultIds;
	}

	/**
	 * Get ids of follower of specificied user via user id
	 * 
	 * @param userId
	 * @return
	 */
	public List<Long> getIdFollower(Long userId) {
		long cursor = -1;
		IDs ids;
		List<Long> resultIds = new ArrayList<Long>();
		try {
			do {
				ids = mTwitter.getFollowersIDs(userId, cursor);
				for (long id : ids.getIDs()) {
					resultIds.add(id);
				}
			} while ((cursor = ids.getNextCursor()) != 0);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return resultIds;
	}

	/**
	 * Get first 100 follower of user authentication
	 * 
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<User> getFollower() {
		List<User> users = new ArrayList<User>();
		List<Long> ids = getIdFollower();
		long[] arrayId = null;

		if (ids != null) {
			int size = ids.size();

			// get first 100 elements of array id
			if (size >= 100) {
				arrayId = new long[100];
			} else {
				arrayId = new long[size];
			}
			int len = arrayId.length;
			for (int i = 0; i < len; i++) {
				arrayId[i] = ids.get(i);
			}

			users = lookup(arrayId);
		}
		return users;
	}

	/**
	 * Get first 100 follower of specificied user
	 * 
	 * @param screenName
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<User> getFollower(String screenName) {
		List<User> users = new ArrayList<User>();
		List<Long> ids = getIdFollower(screenName);
		long[] arrayId = null;

		if (ids != null) {
			int size = ids.size();
			// get first 100 elements of array id
			if (size >= 100) {
				arrayId = new long[100];
			} else {
				arrayId = new long[size];
			}
			int len = arrayId.length;
			for (int i = 0; i < len; i++) {
				arrayId[i] = ids.get(i);
			}

			users = lookup(arrayId);
		}
		return users;
	}

	/**
	 * Get ids of friends of user authentication
	 * 
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<Long> getIdFriends() {
		long cursor = -1;
		IDs ids;
		List<Long> resultIds = new ArrayList<Long>();
		try {
			do {
				ids = mTwitter.getFriendsIDs(cursor);
				for (long id : ids.getIDs()) {
					resultIds.add(id);
				}
			} while ((cursor = ids.getNextCursor()) != 0);
		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return resultIds;
	}

	/**
	 * Get all id of friends of specificed user
	 * 
	 * @param screenName
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<Long> getIdFriends(String screenName) {
		long cursor = -1;
		IDs ids;
		List<Long> resultIds = new ArrayList<Long>();
		try {
			do {
				ids = mTwitter.getFriendsIDs(screenName, cursor);
				for (long id : ids.getIDs()) {
					resultIds.add(id);
				}
			} while ((cursor = ids.getNextCursor()) != 0);
		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return resultIds;
	}

	public List<Long> getIdFriends(Long userId) {
		long cursor = -1;
		IDs ids;
		List<Long> resultIds = new ArrayList<Long>();
		try {
			do {
				ids = mTwitter.getFriendsIDs(userId, cursor);
				for (long id : ids.getIDs()) {
					resultIds.add(id);
				}
			} while ((cursor = ids.getNextCursor()) != 0);
		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return resultIds;
	}

	/**
	 * Get 100 first friends of user authentication
	 * 
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<User> getFriends() throws TwitterException {
		List<User> users = new ArrayList<User>();
		List<Long> ids = getIdFriends();
		long[] arrayId = null;
		if (ids != null) {
			int size = ids.size();

			// get first 100 elements of array id
			if (size >= 100) {
				arrayId = new long[100];
			} else {
				arrayId = new long[size];
			}
			int len = arrayId.length;
			for (int i = 0; i < len; i++) {
				arrayId[i] = ids.get(i);
			}

			users = lookup(arrayId);
		}
		return users;
	}

	/**
	 * Get 100 first friends of specificied user via screen name
	 * 
	 * @param screenName
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<User> getFriends(String screenName) throws TwitterException {
		List<User> users = new ArrayList<User>();
		List<Long> ids = getIdFriends(screenName);
		long[] arrayId = null;

		if (ids != null) {
			int size = ids.size();
			// get first 100 elements of array id
			if (size >= 100) {
				arrayId = new long[100];
			} else {
				arrayId = new long[size];
			}
			int len = arrayId.length;
			for (int i = 0; i < len; i++) {
				arrayId[i] = ids.get(i);
			}
			users = lookup(arrayId);
		}
		return users;
	}

	/**
	 * Get 100 first friends of specificied user via user id
	 *
	 * @return
	 * @throws twitter4j.TwitterException
	 */
	public List<User> getFriends(Long userId) throws TwitterException {
		List<User> users = new ArrayList<User>();
		List<Long> ids = getIdFriends(userId);
		long[] arrayId = null;

		if (ids != null) {
			int size = ids.size();
			// get first 100 elements of array id
			if (size >= 100) {
				arrayId = new long[100];
			} else {
				arrayId = new long[size];
			}
			int len = arrayId.length;
			for (int i = 0; i < len; i++) {
				arrayId[i] = ids.get(i);
			}
			users = lookup(arrayId);
		}
		return users;
	}

	/***************************** Search Util ******************************/
	public List<Status> searchTweets(String queryStr) {
		Query query = new Query(queryStr);
		QueryResult result;
		List<Status> tweets = new ArrayList<Status>();
		try {
			do {
				result = mTwitter.search(query);
				tweets.addAll(result.getTweets());
			} while ((query = result.nextQuery()) != null);

		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return tweets;
	}

	/****************************** Favorite Util ****************************/

	/**
	 * Favorite one tweet
	 * 
	 * @param tweetId
	 *            id of tweet to favorite
	 * @return null if favorite error
	 */
	public Status createFavoriteTweet(Long tweetId) {
		Status status = null;
		try {
			status = mTwitter.createFavorite(tweetId);
		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return status;
	}

	/**
	 * delete one tweet
	 * 
	 * @param tweetId
	 *            id of tweet to delete
	 * @return null if delete favorite error
	 */
	public Status deleteFavoriteTweet(Long tweetId) {
		Status status = null;
		try {
			status = mTwitter.destroyFavorite(tweetId);
		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return status;
	}

	/**
	 * Get most 20 recent favorite tweets of authencation user
	 * 
	 * @return
	 */
	public List<Status> getFavorites() {
		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getFavorites();
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;
	}

	/**
	 * Get favorite tweets of authencation user at specificed page
	 * 
	 * @param page
	 *            positive integer
	 * @return
	 */
	public List<Status> getFavorites(int page) {
		if (page < 1) {
			Log.e( "page is positive integer");
			return null;
		}
		Paging paging = new Paging(page);
		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getFavorites(paging);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;
	}

	/**
	 * Get most 20 recent favorite tweets of specificed user
	 * 
	 * @param userId
	 *            id of user
	 * @return
	 */
	public List<Status> getFavorites(long userId) {
		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getFavorites(userId);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;
	}

	/**
	 * Get favorites tweets of user in specificed page
	 * 
	 * @param userId
	 * @param page
	 * @return
	 */
	public List<Status> getFavorites(long userId, int page) {
		if (page < 1) {
			Log.e( "page is positive integer");
			return null;
		}
		Paging paging = new Paging(page);

		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getFavorites(userId, paging);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;
	}

	/**
	 * Get favorite tweets of user has screenName
	 * 
	 * @param screenName
	 * @return
	 */
	public List<Status> getFavorites(String screenName) {
		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getFavorites(screenName);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;
	}

	/**
	 * Get favorites tweets of user at specificed page
	 */
	public List<Status> getFavorites(String screenName, int page) {
		if (page < 1) {
			Log.e( "page is positive integer");
			return null;
		}
		Paging paging = new Paging(page);

		List<Status> statuses = new ArrayList<Status>();
		try {
			statuses = mTwitter.getFavorites(screenName, paging);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return statuses;
	}

	/************************** Image Util ************************************/

	/**
	 * upload image and add comment to host
	 * 
	 * @param host
	 *            host to upload image, from enum MediaProvider
	 * @param pathFile
	 *            path of file image
	 * @param message
	 *            message for image
	 * 
	 * @return link of image uploaded
	 */
	public String uploadImage(MediaProvider host, String pathFile,
			String message) {
		if (TextUtils.isEmpty(pathFile)) {
			Log.e( "path file is empty");
			return null;
		}
		File file = new File(pathFile);
		if (!file.exists()) {
			Log.e( "file isn't exist");
			return null;
		}
		mConfig = new ConfigurationBuilder()
				.setOAuthConsumerKey(TwitterConfig.CONSUMER_KEY)
				.setOAuthConsumerSecret(TwitterConfig.CONSUMER_SECRET)
				.setOAuthAccessToken(mOauthAccessToken)
				.setOAuthAccessTokenSecret(mOauthAccessTokenSecret).build();
		mImageUploadFactory = new ImageUploadFactory(mConfig);
		mImageUpload = mImageUploadFactory.getInstance(host);
		String url = null;
		try {
			url = mImageUpload.upload(file, message);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return url;
	}

	public String uploadImage(MediaProvider host, String pathFile) {
		return uploadImage(host, pathFile, "");
	}

	public String uploadImage(String pathFile) {
		return uploadImage(DEFAULT_HOST, pathFile, "");
	}

	/******************************** Friendship Util *************************************/

	/**
	 * Follow a user with screen name
	 * 
	 * @param screenName
	 *            screen name of target user
	 * @param follow
	 *            If set true, notify to target user
	 * @return Info of user target, null if error
	 */
	public User createFriend(String screenName, boolean follow) {
		User user = null;
		try {
			user = mTwitter.createFriendship(screenName, follow);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return user;
	}

	/**
	 * Follow a user with screen name and notify to target user
	 * 
	 * @param screenName
	 * @return
	 */
	public User createFriend(String screenName) {
		User user = null;
		try {
			user = mTwitter.createFriendship(screenName);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return user;
	}

	/**
	 * Follow a user with user id
	 * 
	 * @param userId
	 *            id of user target
	 * @param follow
	 *            If set true, notify to target user
	 * @return
	 */
	public User createFriend(long userId, boolean follow) {
		User user = null;
		try {
			user = mTwitter.createFriendship(userId, follow);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return user;
	}

	/**
	 * Follow a user with user id and notify to target user
	 * 
	 * @param userId
	 * @return
	 */
	public User createFriend(long userId) {
		User user = null;
		try {
			user = mTwitter.createFriendship(userId);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return user;
	}

	/**
	 * Delete a friend with screen name
	 * 
	 * @param screenName
	 * @return Info of friend deleted, null if error
	 */
	public User destroyFriend(String screenName) {
		User user = null;
		try {
			user = mTwitter.destroyFriendship(screenName);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return user;
	}

	/**
	 * Delete a friend with user id
	 * 
	 * @param userId
	 * @return
	 */
	public User destroyFriend(long userId) {
		User user = null;
		try {
			user = mTwitter.destroyFriendship(userId);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return user;
	}

	/**
	 * Get list user wait follow user authencation
	 * 
	 * @return array id of list user
	 */
	public List<Long> getIncomingWaitFollow() {
		List<Long> resultIds = new ArrayList<Long>();
		long cursor = -1;
		IDs ids;
		try {
			do {
				ids = mTwitter.getIncomingFriendships(cursor);
				for (long id : ids.getIDs()) {
					resultIds.add(id);
				}
			} while ((cursor = ids.getNextCursor()) != 0);
		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return resultIds;
	}

	/**
	 * Get list user who user authencation wait to follow
	 * 
	 * @return
	 */
	public List<Long> getOutgoingFollow() {
		List<Long> resultIds = new ArrayList<Long>();
		long cursor = -1;
		IDs ids;
		try {
			do {
				ids = mTwitter.getOutgoingFriendships(cursor);
				for (long id : ids.getIDs()) {
					resultIds.add(id);
				}
			} while ((cursor = ids.getNextCursor()) != 0);
		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return resultIds;
	}

	/**
	 * Get relationship of authenticating user to specificied users
	 * 
	 * @param screenNames
	 *            screen name of specificied users
	 * @return
	 */
	public List<Friendship> lookupFriendShips(String[] screenNames) {
		List<Friendship> results = new ArrayList<Friendship>();
		try {
			ResponseList<Friendship> friendShips = mTwitter
					.lookupFriendships(screenNames);
			if (friendShips != null) {
				for (Friendship friendShip : friendShips) {
					results.add(friendShip);
				}
			}
		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return results;
	}

	/**
	 * Get relationship of authenticating user to specificied users
	 * 
	 * @param ids
	 *            id of specificied users
	 * @return
	 */
	public List<Friendship> lookupFriendShips(long[] ids) {
		List<Friendship> results = new ArrayList<Friendship>();
		try {
			ResponseList<Friendship> friendShips = mTwitter
					.lookupFriendships(ids);
			if (friendShips != null) {
				for (Friendship friendShip : friendShips) {
					results.add(friendShip);
				}
			}
		} catch (TwitterException te) {
			Log.e( te.toString());
		}
		return results;
	}

	/**
	 * Show relationShip beetween two users
	 * 
	 * @param sourceScreenName
	 *            screen name of first user
	 * @param targetScreenName
	 *            screen name of second user
	 * @return null if error
	 */
	public Relationship showFriendShip(String sourceScreenName,
			String targetScreenName) {
		Relationship result = null;
		try {
			result = mTwitter
					.showFriendship(sourceScreenName, targetScreenName);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return result;
	}

	/**
	 * Show relationShip beetween two users
	 * 
	 * @param sourceId
	 *            id of first user
	 * @param targetId
	 *            id of target user
	 * @return
	 */
	public Relationship showFriendShip(long sourceId, long targetId) {
		Relationship result = null;
		try {
			result = mTwitter.showFriendship(sourceId, targetId);
		} catch (TwitterException e) {
			Log.e( e.toString());
		}
		return result;
	}

}
