package com.jupitech.swiftvibes.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class SquareLayout extends LinearLayout {

    public SquareLayout(Context context) {
        super(context);
    }

    @SuppressLint("NewApi")
    public SquareLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public SquareLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int size;
        if (widthMode == MeasureSpec.EXACTLY && widthSize > 0) {
            size = widthSize;
        } else if (heightMode == MeasureSpec.EXACTLY && heightSize > 0) {
            size = heightSize;
        } else {
            int widthWithoutPadding = widthSize - getPaddingLeft()
                    - getPaddingRight();
            int heightWithouPadding = heightSize - getPaddingBottom()
                    - getPaddingTop();
            size = widthWithoutPadding < heightWithouPadding ? widthWithoutPadding
                    : heightWithouPadding;
        }

        int finalMeasureSpec = MeasureSpec.makeMeasureSpec(size,
                MeasureSpec.EXACTLY);
        super.onMeasure(finalMeasureSpec, finalMeasureSpec);
        setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(), size
                + getPaddingBottom() + getPaddingTop());
    }

}
