package com.jupitech.swiftvibes.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.base.BaseActionBarActivity;
import com.jupitech.swiftvibes.adapters.TutorialAdapter;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.utils.intent.LaunchIntent;
import com.viewpagerindicator.CirclePageIndicator;

public class TutorialActivity extends BaseActionBarActivity {

    ViewPager mViewPager;
    CirclePageIndicator mCirclePageIndicator;
    TutorialAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserModel userModel = UserModel.getUser();
        if (userModel != null && userModel.isLogined()) {
            LaunchIntent.start(this, NavigationActivity.class);
            finish();
            return;
        }else if(userModel != null && userModel.isEnterDigit()){
            LaunchIntent.start(this, RegisterActivity.class);
            finish();
            return;
        }
        setContentView(R.layout.activity_tutorial);
        registerGCM();
        initModels();
        initViews();
    }

    @Override
    protected void initModels() {
    }

    @Override
    protected void initViews() {
        mViewPager = (ViewPager) findViewById(R.id.pager);

        mAdapter = new TutorialAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);


        mCirclePageIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mCirclePageIndicator.setViewPager(mViewPager);

        findViewById(R.id.skip_text).setOnClickListener(this);

        mTotal = mAdapter.getCount();

        startAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();

        startAnimation();
    }

    @Override
    protected void onPause() {
        super.onPause();

        stopAnimation();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.skip_text:
                LaunchIntent.start(this, RegisterActivity.class);
                finish();
                break;
        }
    }


    public void startAnimation(){
        mHandler.postDelayed(mRunnable, 2000);
    }

    public void stopAnimation(){
        mHandler.removeCallbacks(mRunnable);
    }

    int mPosition = 0;
    int mTotal = 0;

    public Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mViewPager.setCurrentItem(++mPosition % mTotal, true);
            mHandler.postDelayed(this, 2000);
        }
    };
}
