package com.jupitech.swiftvibes.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.fragments.tutorial.ImageFragment;

/**
 * Created by brucenguyen on 10/2/14.
 */
public class TutorialAdapter extends FragmentPagerAdapter {

    int[] IMAGE_ID = new int[]{R.drawable.slide1, R.drawable.slide2, R.drawable.slide3};

    public TutorialAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        return ImageFragment.newInstance(IMAGE_ID[position % IMAGE_ID.length]);
    }

    @Override
    public int getCount() {
        return IMAGE_ID.length;
    }
}
