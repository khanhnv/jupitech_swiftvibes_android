package com.jupitech.swiftvibes.widgets;

import android.content.Context;
import android.widget.ImageView;

import com.jupitech.swiftvibes.R;


public class PlaceRowItem extends BaseLinearLayout {

    public ImageView mImageView;
    public CustomTextView mNameText;
    public CustomTextView mDetailText;

    public PlaceRowItem(Context context) {
        super(context);

        initLayout(context, R.layout.item_place);

        mImageView = (ImageView) findViewById(R.id.image_view);

        mNameText = (CustomTextView) findViewById(R.id.name_text);

        mDetailText = (CustomTextView) findViewById(R.id.detail_text);
    }

}
