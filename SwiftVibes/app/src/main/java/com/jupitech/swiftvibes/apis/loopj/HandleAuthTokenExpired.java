package com.jupitech.swiftvibes.apis.loopj;

import android.content.Intent;

import com.facebook.LoginActivity;
import com.jupitech.swiftvibes.AppApplication;

import org.apache.http.Header;

public class HandleAuthTokenExpired implements IStrageryHandleError {

	@Override
	public void handleError(int statusCode, Header[] headers,
			String responseString, Throwable throwable) {
		if (statusCode == 403) {
			// token is expired, back to login activity
			Intent intent = new Intent(AppApplication.get(),
					LoginActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TOP);
			AppApplication.get().startActivity(intent);
		}
	}

}
