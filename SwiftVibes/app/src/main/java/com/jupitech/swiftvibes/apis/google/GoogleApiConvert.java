package com.jupitech.swiftvibes.apis.google;

import com.google.android.gms.maps.model.LatLng;
import com.jupitech.swiftvibes.apis.parse.JSONUtil;
import com.jupitech.swiftvibes.models.LatLngModel;
import com.jupitech.swiftvibes.models.PlaceModel;
import com.jupitech.swiftvibes.utils.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class GoogleApiConvert {

	private static GoogleApiConvert mInstance;

	private GoogleApiConvert() {
	}

	public static GoogleApiConvert getInstance() {
		if (mInstance == null) {
			mInstance = new GoogleApiConvert();
		}
		return mInstance;
	}

	/**
	 * get result by nearby place
	 * 
	 * @param response
	 * @return
	 */
	public List<PlaceModel> searchNearbyPlace(String response) {
		List<PlaceModel> placeModels = null;

		JSONObject jsonObject;
		JSONArray jsonResults;
		JSONObject jsonResult;
		JSONObject jsonGeometry;
		JSONObject jsonLocation;
		PlaceModel model;
		int length;

		try {
			jsonObject = new JSONObject(response);
			jsonResults = jsonObject.getJSONArray("results");
			length = jsonResults.length();
			placeModels = new ArrayList<PlaceModel>();

			for (int i = 0; i < length; i++) {

				try {
					jsonResult = jsonResults.getJSONObject(i);

					model = new PlaceModel();

					model.setId(JSONUtil.getString(jsonResult, "id"));
					model.setIcon(JSONUtil.getString(jsonResult, "icon"));
					model.setName(JSONUtil.getString(jsonResult, "name"));
					model.setVicinity(JSONUtil
							.getString(jsonResult, "vicinity"));

					jsonGeometry = jsonResult.getJSONObject("geometry");
					jsonLocation = jsonGeometry.getJSONObject("location");

					model.setLat(JSONUtil.getFloat(jsonLocation, "lat"));
					model.setLng(JSONUtil.getFloat(jsonLocation, "lng"));

					placeModels.add(model);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return placeModels;
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	public String getAddress(String response) {
		String address = "";

		JSONObject jsonObject = null;

		try {
			jsonObject = new JSONObject(response);

			String status = jsonObject.getString("status");
			if (status.equals("OK")) {

				JSONArray resultsJson = jsonObject.getJSONArray("results");
				JSONObject itemJson = resultsJson.getJSONObject(0);
				address = JSONUtil.getString(itemJson, "formatted_address");
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return address;
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	public LatLngModel getLocationAdress(String response) {
		LatLngModel latLng = null;
		if (response != null) {
			JSONObject object;
			try {
				object = new JSONObject(response);
				Log.d("result = " + object.toString());
				if (object != null) {
					String status = object.getString("status");
					if (status.equals("OK")) {
						latLng = new LatLngModel();

						JSONArray resultsJson = object.getJSONArray("results");

						JSONObject jAddressComponents = resultsJson
								.getJSONObject(0);

						// for lng and lat
						JSONObject geomatric = jAddressComponents
								.getJSONObject("geometry");
						if (geomatric != null) {
							JSONObject location = geomatric
									.getJSONObject("location");
							if (location != null) {
								String lat = location.getString("lat");
								String lng = location.getString("lng");
								double latitude = Double.parseDouble(lat);
								double longitude = Double.parseDouble(lng);
								latLng.setLatLng(new LatLng(latitude, longitude));
							}
						}
						return latLng;
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
				return null;
			}
		}

		return null;
	}
}
