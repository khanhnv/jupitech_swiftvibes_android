package com.jupitech.swiftvibes.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.jupitech.swiftvibes.AppApplication;
import com.jupitech.swiftvibes.BusProvider;
import com.jupitech.swiftvibes.apis.loopj.RestClient;
import com.jupitech.swiftvibes.apis.parse.JSONConvert;
import com.jupitech.swiftvibes.models.ContactJson;
import com.jupitech.swiftvibes.models.ContactModel;
import com.jupitech.swiftvibes.models.NotificationEvent;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.providers.contact.ContactUtil;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;

import java.util.ArrayList;
import java.util.List;

/**
 * An {@link android.app.IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class AppIntentService extends IntentService {
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FETCH_FRIEND = "com.jupitech.swibvibes.services.action.FETCH_FRIEND";

    private static final String EXTRA_PARAM1 = "com.roxwin.jetblack.services.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "com.roxwin.jetblack.services.extra.PARAM2";

    private UserModel mUser;

    private Handler mHandler = new Handler();

    public static void startActionFetchFriend(Context context) {
        Intent intent = new Intent(context, AppIntentService.class);
        intent.setAction(ACTION_FETCH_FRIEND);
        context.startService(intent);
    }


    public AppIntentService() {
        super("AppIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FETCH_FRIEND.equals(action)) {
                handleActionFetchFriend();
            } else {

            }
        }
    }

    private void handleActionFetchFriend() {
        mUser = UserModel.getUser();

        final String userId = mUser.getUserId();
        final String friendJson = getContactJson();

//        Log.d("userId: " + userId);
//        Log.d("friendJson: " + friendJson);

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                RestClient.getFriendsNews(userId, friendJson, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(String responseBody, Throwable error) {
                        super.onFailure(responseBody, error);

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                BusProvider.getInstance().post(new NotificationEvent(NotificationEvent.ACTION_UPDATE_FRIEND));
                            }
                        });
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                        super.onSuccess(statusCode, headers, responseBody);
//                        Log.d("ResponseBody: " + responseBody);
                        List<UserModel> userModels = JSONConvert.parseFriendResponse(responseBody);
                        if (userModels != null && userModels.size() > 0) {
                            for (UserModel user : userModels) {
                                if (!UserModel.isExist(user)) {
                                    user.setFriend(1);
                                    UserModel.add(user);
                                }
                            }
                        }
//                        Log.d("Friend: " + UserModel.getFriends().toString());
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                BusProvider.getInstance().post(new NotificationEvent(NotificationEvent.ACTION_UPDATE_FRIEND));
                            }
                        });
                    }
                });
            }
        });
    }

    private String getContactJson() {
        ArrayList<ContactModel> contactModels = ContactUtil.getListContact(AppApplication.get());
        if (contactModels == null || contactModels.size() == 0)
            return "";

        ArrayList<ContactJson> contactJsons = new ArrayList<ContactJson>();
        ContactJson contactJson = null;

        String phone = null, countryCode = null;
        for (ContactModel contact : contactModels) {

            phone = contact.getPhoneNumber();
            if (phone != null && phone.length() > 0) {

                phone = phone.replaceAll("\\s+", "");

//                Log.d("PHONE: " + phone);

                contactJson = new ContactJson();

                if (phone.contains("+")) {
                    try {
                        Phonenumber.PhoneNumber phonenumber = PhoneNumberUtil.getInstance().parse(phone, "");
                        countryCode = String.valueOf(phonenumber.getCountryCode());
                        if (countryCode.length() > 0) {
                            phone = phone.substring(countryCode.length()+1, phone.length());
                        } else {
                            countryCode = mUser.getCountryCode();
                        }
                    } catch (NumberParseException e) {
                        e.printStackTrace();
                        countryCode = mUser.getCountryCode();
                    }
                } else {
                    countryCode = mUser.getCountryCode();
                }

//                Log.d("countryCode: " + countryCode);
                if(!countryCode.contains("+"))
                    countryCode = "+" + countryCode;
                contactJson.setCountry_code(countryCode);

                if (phone.indexOf("0") == 0) {
                    phone = phone.substring(1, phone.length());
                }
//                Log.d("Phone: " + phone);
                contactJson.setPhone(phone);

                contactJsons.add(contactJson);
            }
        }

        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(contactJsons);
        return json;
    }

}
