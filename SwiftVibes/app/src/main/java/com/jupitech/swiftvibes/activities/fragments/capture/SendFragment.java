package com.jupitech.swiftvibes.activities.fragments.capture;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.CaptureActivity;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.adapters.SentFriendsAdapter;
import com.jupitech.swiftvibes.apis.loopj.RestClient;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.utils.bitmap.BitmapUtil;
import com.jupitech.swiftvibes.utils.hardware.KeyboardUtil;
import com.jupitech.swiftvibes.utils.intent.LaunchIntent;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SendFragment extends BaseFragment {

    public static final String PARAM_IMAGE = "image";

    ImageView mImageView;
    EditText mCommentText;
    ListView mListView;
    TextView mSentText;

    SentFriendsAdapter mAdapter;

    List<UserModel> mFriendModels = new ArrayList<UserModel>();
    String mPath;
    String mMessage;

    public static SendFragment newInstance(String path) {
        SendFragment fragment = new SendFragment();

        Bundle extras = new Bundle();
        extras.putString(PARAM_IMAGE, path);

        fragment.setArguments(extras);

        return fragment;
    }

    public SendFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPath = getArguments().getString(PARAM_IMAGE);
        }
        Log.d("ImagePath: " + mPath);
        initModels();
    }

    @Override
    protected void initModels() {
        mFriendModels = UserModel.getFriends();
        mPath = mApplication.getImagePath();
    }

    @Override
    protected void initViews(View view) {
        mSentText = (TextView)view.findViewById(R.id.send_text);
        mImageView = (ImageView) view.findViewById(R.id.image_view);
        mCommentText = (EditText) view.findViewById(R.id.comment_text);
        mListView = (ListView) view.findViewById(R.id.list_view);
        view.findViewById(R.id.send_text).setOnClickListener(this);

        mAdapter = new SentFriendsAdapter(getActivity(), 0, mFriendModels);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mFriendModels.get(position).toggle();
                mAdapter.notifyDataSetChanged();
                showSentView();
            }
        });

        Bitmap bitmap = BitmapUtil.decodeBitmapFromFile(getActivity(), 2, mPath);
        if (bitmap != null) {
            mImageView.setImageBitmap(bitmap);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_send, container, false);
        initViews(view);
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_text:
                sentMessage();
                break;
        }
    }

    private void showSentView(){
        boolean isChecked = false;
        for(UserModel user : mFriendModels){
           if(user.isChecked()){
               isChecked = true;
               break;
           }
        }

        if(isChecked){
            mSentText.setVisibility(View.VISIBLE);
        }else {
            mSentText.setVisibility(View.GONE);
        }
    }

    public void sentMessage() {
        if (!validate()) return;

        KeyboardUtil.hideSoftKeyboard(getActivity());

        callSentMessageApi();
    }

    public String getFriendId() {
        String friendId = "";

        UserModel user = null;
        if (mFriendModels.size() > 0) {
            for (int i = 0; i < mFriendModels.size() - 1; i++) {
                user = mFriendModels.get(i);
                if (user.isChecked()) {
                    friendId += user.getUserId();
                    friendId += ",";
                }
            }

            user = mFriendModels.get(mFriendModels.size() - 1);
            if (user.isChecked()) {
                friendId += user.getUserId();
            }
        }
        return friendId;
    }

    public boolean validate() {
        mCommentText.setError(null);
        mMessage = mCommentText.getText().toString().trim();
        if (mMessage.length() == 0) {
            mCommentText.setError("Enter comment here");
            return false;
        }
        return true;
    }

    public void callSentMessageApi() {

        String fromId = UserModel.getUser().getUserId();
        String message = mMessage;
        String friendList = getFriendId();
        final File image = new File(mPath);

        Log.d("fromId: " + fromId);
        Log.d("Message: " + message);
        Log.d("friendList: " + friendList);
        Log.d("ImagePath: " + image.getAbsolutePath());

        RestClient.sentMessage(fromId, message, image, friendList, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                Log.d(responseBody);
//                ResponseModel responseModel = JSONConvert.parseResponse(responseBody);
//                if(responseModel.getStatus() == 1){
//                    switchFragment(new CaptureFragment());
                LaunchIntent.start(getActivity(), CaptureActivity.class);
                getActivity().finish();
//                }

                image.delete();
            }

            @Override
            public void onFailure(String responseBody, Throwable error) {
                super.onFailure(responseBody, error);
                LaunchIntent.start(getActivity(), CaptureActivity.class);
                getActivity().finish();

                image.delete();
            }
        });
    }
}
