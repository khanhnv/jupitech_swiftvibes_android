package com.jupitech.swiftvibes.models;

import android.os.Bundle;

/**
 * Created by brucenguyen on 10/16/14.
 */
public class NotificationEvent {

    public static final String ACTION_UPDATE_FRIEND = "update_friend";

    public NotificationEvent(String action) {
        this.action = action;
    }

    public NotificationEvent(String action, Bundle data) {
        this.action = action;
        this.data = data;
    }

    private String action;
    private Bundle data;


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Bundle getData() {
        return data;
    }

    public void setData(Bundle data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Nofitication{" +
                "action='" + action + '\'' +
                ", data=" + data +
                '}';
    }
}
