package com.jupitech.swiftvibes.activities.fragments.contact;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.models.ContactModel;

public class ContactDetailFragment extends BaseFragment {

    private static final String ARG_CONTACT = "contact";

    private ContactModel mContact;

    private TextView mNameText;
    private TextView mMobileText;
    private TextView mInviteText;



    public static ContactDetailFragment newInstance(ContactModel contact) {
        ContactDetailFragment fragment = new ContactDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_CONTACT, contact);
        fragment.setArguments(args);
        return fragment;
    }
    public ContactDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mContact = (ContactModel) getArguments().getSerializable(ARG_CONTACT);
        }
    }

    @Override
    protected void initModels() {

    }

    @Override
    protected void initViews(View view) {
        mNameText = (TextView)view.findViewById(R.id.name_text);
        mMobileText = (TextView)view.findViewById(R.id.phone_text);
        mInviteText = (TextView)view.findViewById(R.id.invite_text);
        mInviteText.setOnClickListener(this);

        mNameText.setText(mContact.getContactName());
        mMobileText.setText(mContact.getPhoneNumber());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_contact_detail, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.invite_text:
                invite();
                break;
        }
    }

    private void invite(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, mContact.getPhoneNumber());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Send To"));
    }

}
