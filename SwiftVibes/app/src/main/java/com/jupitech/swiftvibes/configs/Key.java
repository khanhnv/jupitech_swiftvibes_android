package com.jupitech.swiftvibes.configs;

import com.jupitech.swiftvibes.utils.sdcard.FileManager;

import java.io.File;


public class Key {

	public static final String SIGN_UP_RESULT = "sign_up_result";
	public static final String SIGN_UP_EMAIL = "sign_up_email";

	public static final String APPSTORE_TYPE_APPLE = "apple";
	public static final String APPSTORE_TYPE_GOOGLE_PLAY = "google_play";

	/*
	 * folder root
	 */
	public static final String FOLDER_ROOT_NAME = "SwifVibes";
	public static final String FOLDER_ROOT_PATH = FileManager.EXTERNAL_PATH
			+ File.separator + FOLDER_ROOT_NAME;

	/*
	 * folder data
	 */
	public static final String FOLDER_DATA_NAME = "data";

	public static final String FOLDER_DATA_PATH = FOLDER_ROOT_PATH
			+ File.separator + FOLDER_DATA_NAME;

	/*
	 * folder image
	 */
	public static final String FOLDER_IMAGE_NAME = "images";

	// folder image path
	public static final String FOLDER_IMAGE_PATH = FOLDER_ROOT_PATH
			+ File.separator + FOLDER_IMAGE_NAME;

    public static final String IMAGE_PATH = FOLDER_IMAGE_PATH + File.separator + "%s.jpg";

    /*
	 * folder audio
	 */
    public static final String FOLDER_AUDIO_NAME = "audios";
    public static final String FOLDER_AUDIO_PATH = FOLDER_ROOT_PATH
            + File.separator + FOLDER_AUDIO_NAME;


    public static final String FILE_AUDIO_PATH = FOLDER_AUDIO_PATH
            + File.separator + "%s.mp4";

    public static final String FILE_TEMP_NAME = "lola.jpg";

    public static final String FILE_TEMP_PATH = FOLDER_IMAGE_PATH
            + File.separator + "lola.jpg";

    public static int NOTIFICATION_ID = 2181989;

    public static final String FRAGMENT_HEARD = "fragment_heard";
    public static final String FRAGMENT_RECORD = "fragment_record";

}
