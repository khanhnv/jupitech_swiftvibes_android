package com.jupitech.swiftvibes.activities.fragments.capture;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.FriendActivity;
import com.jupitech.swiftvibes.activities.NavigationActivity;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.configs.Key;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.utils.ToastUtil;
import com.jupitech.swiftvibes.utils.bitmap.BitmapUtil;
import com.jupitech.swiftvibes.utils.hardware.CameraUtils;
import com.jupitech.swiftvibes.utils.intent.LaunchIntent;
import com.jupitech.swiftvibes.utils.text.StringUtil;
import com.jupitech.swiftvibes.widgets.CameraPreview;
import com.readystatesoftware.viewbadger.BadgeView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import eu.janmuller.android.simplecropimage.CropImage;

public class CaptureFragment extends BaseFragment {

    public static final String TEMP_PHOTO_FILE = "test.jpg";
    public static final int REQUEST_CODE_CROP_IMAGE = 0x3;
    private static final int PICTURE_SIZE_MAX_WIDTH = 640;
    private static final int PREVIEW_SIZE_MAX_WIDTH = 640;
    private static final int REQUEST_LOAD_IMAGE = 1;

    private FrameLayout mFrameCamera;
    private boolean mFrontCamera = false;
    private int mCameraId;

    // flag camera
    public static boolean mChooseImage;
    public static boolean mTakeImage;

    OrientationEventListener mOrientationEventListener;
    BadgeView mBadgeView;


    // image for send
    private Bitmap mBitmap;
    private ImageView mSwitchImage;

    // for camera
    private Camera mCamera;
    private CameraPreview mPreview;

    private Camera.PictureCallback mPictureCallBack = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            FileOutputStream fos = null;
            try {
                File pictureFile = getTempFile();

                fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();

                Bitmap source = BitmapFactory.decodeByteArray(data, 0,
                        data.length);

                mBitmap = rotateImage(source, pictureFile.getAbsolutePath());

                pictureFile.delete();
                pictureFile.createNewFile();

                // save display file
                BitmapUtil.writeBitmap(mBitmap, pictureFile.getAbsolutePath(),
                        100);

                // flag take image
                mTakeImage = true;

                // hide camera layout
                mCamera.stopPreview();
                mFrameCamera.setVisibility(View.GONE);
                // show image again

//                startCropImage(pictureFile.getAbsolutePath());
                switchFragment(PreviewFragment.newInstance(pictureFile.getAbsolutePath()));

            } catch (FileNotFoundException e) {
                Log.d("File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d("Error accessing file: " + e.getMessage());
            }
        }
    };


    public CaptureFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        initModels();
    }

    @Override
    protected void initModels() {
    }

    @Override
    protected void initViews(View view) {
        view.findViewById(R.id.gallery_image).setOnClickListener(this);
        view.findViewById(R.id.rotate_image).setOnClickListener(this);
        view.findViewById(R.id.home_image).setOnClickListener(this);
        view.findViewById(R.id.get_image).setOnClickListener(this);
        view.findViewById(R.id.friend_image).setOnClickListener(this);

        mFrameCamera = (FrameLayout) view.findViewById(R.id.container);
        mFrameCamera.setVisibility(View.VISIBLE);

        mSwitchImage = (ImageView) view
                .findViewById(R.id.rotate_image);
        mSwitchImage.setOnClickListener(this);
        if (CameraUtils.isFrontCamera()) {
            mSwitchImage.setVisibility(View.VISIBLE);
        } else {
            mSwitchImage.setVisibility(View.GONE);
        }

        mFrontCamera = false;

        setupCamera(Camera.CameraInfo.CAMERA_FACING_BACK);

        Log.d("UnReadMessage: " + mPreferenceManager.getUnReadMessage());

        ImageView homeImage = (ImageView) view.findViewById(R.id.home_image);
        mBadgeView = new BadgeView(getActivity(), homeImage);
        mBadgeView.setText(String.valueOf(mPreferenceManager.getUnReadMessage()));
        mBadgeView.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
        mBadgeView.setTextColor(Color.WHITE);
        mBadgeView.setBadgeBackgroundColor(Color.RED);

        mBadgeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mPreferenceManager.getUnReadMessage().equals("0")) {
                    mBadgeView.toggle();
                }
            }
        });
        if (!mPreferenceManager.getUnReadMessage().equals("0")) {
            mBadgeView.show();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_capture, container, false);
        initViews(view);
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gallery_image:
                onClickChoosePhoto();
                break;
            case R.id.rotate_image:
                toggleCamera();
                break;
            case R.id.home_image:
                if (!mPreferenceManager.getUnReadMessage().equals("0")) {
                    mBadgeView.toggle();
                }
                LaunchIntent.start(getActivity(), NavigationActivity.class);
                getActivity().overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
                break;
            case R.id.get_image:
                onClickCamera();
                break;
            case R.id.friend_image:
                LaunchIntent.start(getActivity(), FriendActivity.class);
                getActivity().overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
                break;
        }
    }


    private Camera.Size determineBestPreviewSize(Camera.Parameters parameters) {
        List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
        return determineBestSize(sizes, PREVIEW_SIZE_MAX_WIDTH);
    }

    private Camera.Size determineBestPictureSize(Camera.Parameters parameters) {
        List<Camera.Size> sizes = parameters.getSupportedPictureSizes();
        return determineBestSize(sizes, PICTURE_SIZE_MAX_WIDTH);
    }

    protected Camera.Size determineBestSize(List<Camera.Size> sizes, int widthThreshold) {
        Camera.Size bestSize = null;

        for (Camera.Size currentSize : sizes) {
            boolean isDesiredRatio = (currentSize.width / 4) == (currentSize.height / 3);
            boolean isBetterSize = (bestSize == null || currentSize.width > bestSize.width);
            boolean isInBounds = currentSize.width <= widthThreshold;

            if (isDesiredRatio && isInBounds && isBetterSize) {
                bestSize = currentSize;
            }
        }

        if (bestSize == null) {
            return sizes.get(0);
        }

        return bestSize;
    }

    private void toggleCamera() {
        try {
            if (mFrontCamera) {
                mFrontCamera = false;
                openBackCamera();
            } else {
                mFrontCamera = true;
                openFrontCamera();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openFrontCamera() {
        try {
            mCamera.stopPreview();
            mCamera.release();
            mPreview = null;
            mFrameCamera.removeAllViews();
            setupCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openBackCamera() {
        try {
            mCamera.stopPreview();
            mCamera.release();
            mPreview = null;
            mFrameCamera.removeAllViews();
            setupCamera(Camera.CameraInfo.CAMERA_FACING_BACK);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupCamera(int cameraId) {
        mCameraId = cameraId;
        if (CameraUtils.checkCameraHardware(getActivity())) {
            mFrameCamera.setVisibility(View.VISIBLE);
            mCamera = CameraUtils.getCameraInstance(getActivity(), cameraId);

            Camera.Parameters parameters = mCamera.getParameters();
//            Camera.Size bestPreviewSize = determineBestPreviewSize(parameters);
            Camera.Size bestPictureSize = determineBestPictureSize(parameters);
//            parameters.setPreviewSize(bestPreviewSize.width,
//                    bestPreviewSize.height);
            parameters.setPictureSize(bestPictureSize.width,
                    bestPictureSize.height);

            mCamera.setParameters(parameters);

            mPreview = new CameraPreview(getActivity(), mCamera);
            mFrameCamera.addView(mPreview);

            mOrientationEventListener = new OrientationEventListener(
                    getActivity(), SensorManager.SENSOR_DELAY_UI) {

                @Override
                public void onOrientationChanged(int orientation) {
                    if (orientation == ORIENTATION_UNKNOWN)
                        return;
                    if (mCamera != null) {
                        Camera.CameraInfo info = new Camera.CameraInfo();
                        Camera.getCameraInfo(mCameraId, info);
                        orientation = (orientation + 45) / 90 * 90;
                        int rotation = 0;
                        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                            rotation = (info.orientation - orientation + 360) % 360;
                        } else { // back-facing camera
                            rotation = (info.orientation + orientation) % 360;
                        }
                        if(mCamera != null) {
                            try {
                                Camera.Parameters parameters = mCamera.getParameters();
                                parameters.setRotation(rotation);
                                mCamera.setParameters(parameters);
                            }catch(RuntimeException e){
                                e.printStackTrace();
                            }
                        }
                    }
                }
            };
        }
    }

    private void onClickChoosePhoto() {
        // in onCreate or any event where your want the user to
        // select a file
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), REQUEST_LOAD_IMAGE);
    }

    private void onClickCamera() {
        try {
            mCamera.takePicture(null, null, mPictureCallBack);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File getTempFile() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            File file = new File(String.format(Key.IMAGE_PATH, StringUtil.randomString()));
            try {
                file.createNewFile();
            } catch (IOException e) {
            }
            return file;
        } else {
            File file = new File(getActivity().getFilesDir().getAbsolutePath(),
                    TEMP_PHOTO_FILE);
            try {
                file.createNewFile();
            } catch (IOException e) {
            }
            return file;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            if (mFrontCamera) {
                openFrontCamera();
            } else {
                openBackCamera();
            }
            return;
        }

        switch (requestCode) {
            case REQUEST_LOAD_IMAGE:
                if (data != null) {
                    Uri selectedImageUri = data.getData();
                    String selectedImagePath = getPath(selectedImageUri);

                    Bitmap source = BitmapUtil.decodeBitmapFromFile(getActivity(), 2, selectedImagePath);
                    mBitmap = rotateImage(source, selectedImagePath);

                    if (mBitmap == null) {
                        Log.d("Decode fail");
                        return;
                    }

                    File tempFile = getTempFile();

                    try {
                        BitmapUtil.writeBitmap(mBitmap,
                                tempFile.getAbsolutePath(), 100);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // hide camera layout
                    if (mCamera != null) {
                        mCamera.stopPreview();
                    }
                    mFrameCamera.setVisibility(View.GONE);

                    // flag choose image
                    mChooseImage = true;

//                    startCropImage(tempFile.getAbsolutePath());
                    switchFragment(PreviewFragment.newInstance(tempFile.getAbsolutePath()));
                }
                break;
            case REQUEST_CODE_CROP_IMAGE:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                switchFragment(PreviewFragment.newInstance(path));
                break;
        }
    }

    /**
     * helper to retrieve the path of an image URI
     */
    public String getPath(Uri uri) {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.i("on resume");

        if (mCamera == null) {
            Log.d("setup camera");
            if (mFrontCamera) {
                setupCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
            } else {
                setupCamera(Camera.CameraInfo.CAMERA_FACING_BACK);
            }
            if (mCamera != null) {
                mCamera.startPreview();
            } else {
                Log.d("Cannot connect to camera");
                ToastUtil.show(getActivity(), "Cannot connect to camera");
            }
        }

        mOrientationEventListener.enable();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("onPause");
        try {
            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.unlock();
                mCamera.setPreviewCallback(null);
                mCamera.release();
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        // mPreview.releaseCamera();
        mFrameCamera.removeView(mPreview);
        mCamera = null;

        mOrientationEventListener.disable();
        mOrientationEventListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void saveImageLayout() {
        FileOutputStream stream = null;
        // get image from layout
//        try {
//            layoutImage.setDrawingCacheEnabled(true);
//            Bitmap bmImageUpload = layoutImage.getDrawingCache();
//            if (viewPager.getCurrentItem() == 6) {
//                Bitmap crop = processCrop(bmImageUpload);
//                // save bitmap to sd card
//                String sdCardPath = Environment.getExternalStorageDirectory()
//                        .getAbsolutePath();
//                imageFile = new File(sdCardPath, TEMP_PHOTO_FILE);
//                if (imageFile.exists())
//                    imageFile.delete();
//                stream = new FileOutputStream(imageFile);
//                crop.compress(CompressFormat.JPEG, 70, stream);
//                stream.flush();
//                stream.close();
//                mApplication.setTemporaryUrl(imageFile.getAbsolutePath());
//            } else {
//                // save bitmap to sd card
//                String sdCardPath = Environment.getExternalStorageDirectory()
//                        .getAbsolutePath();
//                imageFile = new File(sdCardPath, TEMP_PHOTO_FILE);
//                if (imageFile.exists())
//                    imageFile.delete();
//                stream = new FileOutputStream(imageFile);
//                bmImageUpload.compress(CompressFormat.JPEG, 70, stream);
//                stream.flush();
//                stream.close();
//                mApplication.setTemporaryUrl(imageFile.getAbsolutePath());
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }


    private Bitmap rotateImage(Bitmap bitmap, String fileName) {
        Matrix matrix = new Matrix();

        // rotate image if not right
        ExifInterface ei;
        try {
            ei = new ExifInterface(fileName);
            int orientation = ei.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(270);
                    break;
                case ExifInterface.ORIENTATION_TRANSPOSE:
                    matrix.setRotate(90);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_TRANSVERSE:
                    matrix.setRotate(-90);
                    matrix.postScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                    matrix.setScale(-1, 1);
                    break;
                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    matrix.setRotate(180);
                    matrix.postScale(-1, 1);
                    break;
                default:
                    return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Bitmap rotate = Bitmap.createBitmap(bitmap, 0, 0,
                    bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return rotate;
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    private void startCropImage(String path) {

        Intent intent = new Intent(getActivity(), CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, path);
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 2);
        intent.putExtra(CropImage.ASPECT_Y, 3);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }
}
