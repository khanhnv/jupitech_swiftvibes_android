package com.jupitech.swiftvibes;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;

import com.jupitech.swiftvibes.models.PostModel;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.jupitech.swiftvibes.configs.DeveloperConfig;
import com.jupitech.swiftvibes.configs.Key;
import com.jupitech.swiftvibes.utils.bitmap.BitmapResourceLoader;
import com.jupitech.swiftvibes.utils.bitmap.BitmapUtil;
import com.jupitech.swiftvibes.utils.sdcard.FileManager;

public class AppApplication extends Application {

    private static AppApplication instance;
    private boolean appPurchasing;
    private boolean appRunning;
    private boolean startNewScreen;
    private boolean backPress;
    private boolean initAppBilling = false;
    private boolean appBilling = false;
    private PostModel post;
    private String imagePath;

    public static AppApplication get() {
        return instance;
    }

    public boolean isAppPurchasing() {
        return appPurchasing;
    }

    public void setAppPurchasing(boolean appPurchasing) {
        this.appPurchasing = appPurchasing;
    }

    public boolean isAppRunning() {
        return appRunning;
    }

    public void setAppRunning(boolean appRunning) {
        this.appRunning = appRunning;
    }

    public boolean isStartNewScreen() {
        return startNewScreen;
    }

    public void setStartNewScreen(boolean startNewScreen) {
        this.startNewScreen = startNewScreen;
    }

    public boolean isBackPress() {
        return backPress;
    }

    public void setBackPress(boolean backPress) {
        this.backPress = backPress;
    }

    public boolean isInitAppBilling() {
        return initAppBilling;
    }

    public void setInitAppBilling(boolean initAppBilling) {
        this.initAppBilling = initAppBilling;
    }

    public boolean isAppBilling() {
        return appBilling;
    }

    public void setAppBilling(boolean appBilling) {
        this.appBilling = appBilling;
    }

    public PostModel getPost() {
        return post;
    }

    public void setPost(PostModel post) {
        this.post = post;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void onCreate() {
        super.onCreate();

        instance = this;

        if (DeveloperConfig.DEVELOPER_MODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                StrictMode
                        .setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                                .detectDiskReads().detectDiskWrites()
                                .detectNetwork().penaltyLog().build());
                StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                        .detectLeakedSqlLiteObjects().penaltyLog()
                        .penaltyDeath().build());
            }
        }
        initImageLoader(getApplicationContext());
        initApplicationFolder();
        initUtils();
    }

    /**
     * init image loader
     *
     * @param context
     */
    public void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you
        // may tune some of them,
        // or you can create default configuration by
        // ImageLoaderConfiguration.createDefault(this);
        // method.
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_stub)// resource or drawable
                .showImageForEmptyUri(R.drawable.ic_empty) // resource or
                        // drawable
                .showImageOnFail(R.drawable.ic_error) // resource or drawable
                .cacheInMemory(true) // default
                .cacheOnDisk(true) // default
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                context).threadPriority(Thread.NORM_PRIORITY - 1)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .defaultDisplayImageOptions(options).build();

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }

    /**
     * create all folder of application
     */
    public void initApplicationFolder() {
        // root folder
        FileManager.createNewFolder(Key.FOLDER_ROOT_NAME);
        // data folder
        FileManager.createNewFolder(Key.FOLDER_ROOT_PATH, Key.FOLDER_DATA_NAME);
        // image folder
        FileManager
                .createNewFolder(Key.FOLDER_ROOT_PATH, Key.FOLDER_IMAGE_NAME);
        // audio folder
        FileManager.createNewFolder(Key.FOLDER_ROOT_PATH, Key.FOLDER_AUDIO_NAME);
    }


    public void initUtils() {
        BitmapUtil.initializeStaticContext(this);
        BitmapResourceLoader.initializeStaticContext(this);
        BitmapResourceLoader.initializeLruCache();
    }

}
