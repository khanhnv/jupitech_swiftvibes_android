package com.jupitech.swiftvibes.providers.contact;

import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;

import com.google.common.base.Preconditions;
import com.jupitech.swiftvibes.models.ContactModel;
import com.jupitech.swiftvibes.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * 
 * @author khanhnv
 * 
 */
public class ContactUtil {

	/**
	 * Remove hyphen in phone number in Contact version 2.0
	 * 
	 * @param phoneNumber
	 *            The phone number of contact
	 * @return Return phone number without hyphen
	 */
	public static String removeHyphenInPhoneNumber(String phoneNumber) {
		Preconditions.checkNotNull(phoneNumber);

		String phoneNumberWithoutHyphen = "";
		String arr[] = phoneNumber.trim().split("-");

		if (arr.length > 0) {
			for (int i = 0; i < arr.length; i++) {
				phoneNumberWithoutHyphen += arr[i].toString().trim();
			}
		}
		return phoneNumberWithoutHyphen.equals("") ? phoneNumber
				: phoneNumberWithoutHyphen;
	}

	public static String getColumnValue(Cursor cursor, String columnName,
			String where) {
		String value = "";
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				do {
					String columnValue = cursor.getString(cursor
							.getColumnIndex(columnName));
					if (columnValue.equalsIgnoreCase(where)) {
						value = columnValue;
						break;
					}
				} while (cursor.moveToNext());
			}
		}
		return value;
	}

	public static String getPersonNameContact(Context context,
			String phoneNumber) {
		Preconditions.checkNotNull(phoneNumber);

		boolean stop = false;
		String personNameContact = "";

		ContentResolver contentResolver = context.getContentResolver();

		Cursor cContact = contentResolver.query(
				Contacts.CONTENT_URI, null, null, null, null);

		if (cContact != null) {
			if (cContact.moveToFirst()) {
				do {
					String id = cContact.getString(cContact
							.getColumnIndex(Contacts._ID));
					String name = cContact
							.getString(cContact
									.getColumnIndex(Contacts.DISPLAY_NAME));

					if (Integer
							.parseInt(cContact.getString(cContact
									.getColumnIndex(Contacts.HAS_PHONE_NUMBER))) > 0) {

						Cursor cPhone = contentResolver
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = ?", new String[] { id },
										null);

						if (cPhone != null) {
							if (cPhone.moveToFirst()) {
								do {
									String number = cPhone
											.getString(cPhone
													.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

									if (removeHyphenInPhoneNumber(number)
											.equalsIgnoreCase(phoneNumber)) {
										personNameContact = name;
										stop = true;
										break;
									}
								} while (cPhone.moveToNext());
							}
						}

						cPhone.close();
					}
					if (stop) {
						break;
					}
				} while (cContact.moveToNext());
			}
			cContact.close();
		}

		return personNameContact.equals("") ? null : personNameContact;
	}

	public static void addContact(Context context, String displayName,
			String phoneNumber, Bitmap photo) {

		ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
		ops.add(ContentProviderOperation
				.newInsert(ContactsContract.RawContacts.CONTENT_URI)
				.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
				.withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
				.build());

		ops.add(ContentProviderOperation
				.newInsert(ContactsContract.Data.CONTENT_URI)
				.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
				.withValue(
						ContactsContract.Data.MIMETYPE,
						ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
				.withValue(
						ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
						displayName).build());

		ops.add(ContentProviderOperation
				.newInsert(ContactsContract.Data.CONTENT_URI)
				.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
				.withValue(
						ContactsContract.Data.MIMETYPE,
						ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
				.withValue(ContactsContract.CommonDataKinds.Phone.NUMBER,
						phoneNumber)
				.withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
						ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
				.build());

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		if (photo != null) {
			photo.compress(Bitmap.CompressFormat.PNG, 100, arrayOutputStream);
		}

		ops.add(ContentProviderOperation
				.newInsert(ContactsContract.Data.CONTENT_URI)
				.withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
				.withValue(
						ContactsContract.Data.MIMETYPE,
						ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)
				.withValue(ContactsContract.CommonDataKinds.Photo.PHOTO,
						arrayOutputStream.toByteArray()).build());
		try {
			ContentResolver contentResolver = context.getContentResolver();
			contentResolver.applyBatch(ContactsContract.AUTHORITY, ops);
		} catch (Exception e) {
		}
	}

	public static boolean checkExistPhoneNumber(Context context,
			String phoneNumber) {
		boolean result = false, isStop = false;

		ContentResolver contentResolver = context.getContentResolver();
		Cursor cContact = contentResolver.query(
				Contacts.CONTENT_URI, null, null, null, null);

		if (cContact != null) {
			if (cContact.moveToFirst()) {
				do {
					String id = cContact.getString(cContact
							.getColumnIndex(Contacts._ID));

					if (Integer
							.parseInt(cContact.getString(cContact
									.getColumnIndex(Contacts.HAS_PHONE_NUMBER))) > 0) {
						Cursor cPhone = contentResolver
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = ?", new String[] { id },
										null);

						while (cPhone.moveToNext()) {
							String number = cPhone
									.getString(cPhone
											.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
							if (removeHyphenInPhoneNumber(number).equals(
									phoneNumber)) {
								result = true;
								isStop = true;
								break;
							}
						}
						cPhone.close();

						if (isStop) {
							break;
						}
					}

				} while (cContact.moveToNext());

				cContact.close();
			}
		}

		return result;
	}

	/**
	 * This interface defines constants for the Cursor and CursorLoader, based
	 * on constants defined in the
	 * {@link android.provider.ContactsContract.Contacts} class.
	 */
	public interface ContactsQuery {

		// An identifier for the loader
		final static int QUERY_ID = 1;

		// A content URI for the Contacts table
		final static Uri CONTENT_URI = Contacts.CONTENT_URI;

		// A content URI for the Phone table
		final static Uri PHONE_CONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

		final static Uri EMAIL_CONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;

		// The search/filter query Uri
		final static Uri FILTER_URI = Contacts.CONTENT_FILTER_URI;

		// The selection clause for the CursorLoader query. The search criteria
		// defined here
		// restrict results to contacts that have a display name and are linked
		// to visible groups.
		// Notice that the search on the string provided by the user is
		// implemented by appending
		// the search string to CONTENT_FILTER_URI.
		@SuppressLint("InlinedApi")
		final static String SELECTION = (Utils.hasHoneycomb() ? Contacts.DISPLAY_NAME_PRIMARY
				: Contacts.DISPLAY_NAME)
				+ "<>''" + " AND " + Contacts.IN_VISIBLE_GROUP + "=1";

		// The desired sort order for the returned Cursor. In Android 3.0 and
		// later, the primary
		// sort key allows for localization. In earlier versions. use the
		// display name as the sort
		// key.
		@SuppressLint("InlineApi")
		final static String SORT_ORDER = Utils.hasHoneycomb() ? Contacts.SORT_KEY_PRIMARY
				: Contacts.DISPLAY_NAME;

		final static String[] PROJECTION = {

				// The contact's row id
				Contacts._ID,

				// A pointer to the contact that is guaranteed to be more
				// permanent than _ID. Given
				// a contact's current _ID value and LOOKUP_KEY, the Contacts
				// Provider can generate
				// a "permanent" contact URI.
				Contacts.LOOKUP_KEY,

				// In platform version 3.0 and later, the Contacts table
				// contains
				// DISPLAY_NAME_PRIMARY, which either contains the contact's
				// displayable name or
				// some other useful identifier such as an email address. This
				// column isn't
				// available in earlier versions of Android, so you must use
				// Contacts.DISPLAY_NAME
				// instead.
				Utils.hasHoneycomb() ? Contacts.DISPLAY_NAME_PRIMARY
						: Contacts.DISPLAY_NAME,

				// In Android 3.0 and later, the thumbnail image is pointed to
				// by
				// PHOTO_THUMBNAIL_URI. In earlier versions, there is no direct
				// pointer; instead,
				// you generate the pointer from the contact's ID value and
				// constants defined in
				// android.provider.ContactsContract.Contacts.
				Utils.hasHoneycomb() ? Contacts.PHOTO_THUMBNAIL_URI
						: Contacts._ID,

				// The sort order column for the returned Cursor, used by the
				// AlphabetIndexer
				SORT_ORDER,

				// Check has phone number
				Contacts.HAS_PHONE_NUMBER, };

		// The query column numbers which map to each value in the projection
		final static int ID = 0;
		final static int LOOKUP_KEY = 1;
		final static int DISPLAY_NAME = 2;
		final static int PHOTO_THUMBNAIL_DATA = 3;
		final static int SORT_KEY = 4;
		final static int HAS_PHONE_NUMBER = 5;
	}

	public static ArrayList<ContactModel> getListContact(Context context) {
		ArrayList<ContactModel> contactModels = null;
		ContactModel model = null;
		String id, name, number, photo, email;
		boolean hasPhoneNumber = false;

		ContentResolver contentResolver = context.getContentResolver();

		Cursor cursor = contentResolver.query(ContactsQuery.CONTENT_URI,
				ContactsQuery.PROJECTION, ContactsQuery.SELECTION, null,
				ContactsQuery.SORT_ORDER);

		if (cursor == null)
			return null;

		if (!cursor.moveToFirst())
			return null;

		contactModels = new ArrayList<ContactModel>();
		do {
			model = new ContactModel();
			id = cursor.getString(ContactsQuery.ID);
			model.setContactId(id);

			name = cursor.getString(ContactsQuery.DISPLAY_NAME);
			model.setContactName(name);

			hasPhoneNumber = cursor.getInt(ContactsQuery.HAS_PHONE_NUMBER) > 0;
			if (hasPhoneNumber) {
				Cursor cursorPhoneNumber = contentResolver.query(
						ContactsQuery.PHONE_CONTENT_URI, null,
						ContactsContract.CommonDataKinds.Phone.CONTACT_ID
								+ " = ?", new String[] { id }, null);
				if (cursorPhoneNumber != null) {
					if (cursorPhoneNumber.moveToFirst()) {
						number = cursorPhoneNumber
								.getString(cursorPhoneNumber
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
						model.setPhoneNumber(number);
					}
					cursorPhoneNumber.close();
				}
			}

			Cursor cursorEmail = contentResolver.query(
					ContactsQuery.EMAIL_CONTENT_URI, null,
					ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
					new String[] { id }, null);
			if (cursorEmail != null) {
				if (cursorEmail.moveToNext()) {
					email = cursorEmail
							.getString(cursorEmail
									.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
					model.setEmail(email);
				}
				cursorEmail.close();
			}



			contactModels.add(model);

		} while (cursor.moveToNext());

		cursor.close();

		return contactModels;
	}
}
