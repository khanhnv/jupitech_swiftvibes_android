package com.jupitech.swiftvibes.listeners;


import com.jupitech.swiftvibes.models.PlaceModel;

public interface PlaceListener {

	/**
	 * 
	 * @param postion
	 * @param model
	 */
	void onItemSelected(final int postion, final PlaceModel model);
}
