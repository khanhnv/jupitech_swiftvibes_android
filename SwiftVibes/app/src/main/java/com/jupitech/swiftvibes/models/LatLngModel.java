package com.jupitech.swiftvibes.models;

import java.io.Serializable;

import com.google.android.gms.maps.model.LatLng;

public class LatLngModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4685611248017763302L;

	private int id;

	private LatLng latLng;

	private String iconURL;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LatLng getLatLng() {
		return latLng;
	}

	public void setLatLng(LatLng latLng) {
		this.latLng = latLng;
	}

	public String getIconURL() {
		return iconURL;
	}

	public void setIconURL(String iconURL) {
		this.iconURL = iconURL;
	}

	@Override
	public String toString() {
		return "JLatLng [id=" + id + ", latLng=" + latLng + ", iconURL="
				+ iconURL + "]";
	}

}
