package com.jupitech.swiftvibes.apis.loopj;

import com.jupitech.swiftvibes.configs.ApiConfig;
import com.jupitech.swiftvibes.utils.Log;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;
import java.io.FileNotFoundException;

public class RestClient {

    public static void addUser(String countryCode, /*String name,*/ String phone, /*String invitationCode,*/
                               String deviceOs, String deviceOsVersion, String deviceId, TextHttpResponseHandler responseHandler) {

        RequestParams params = new RequestParams();
        params.add("country_code", countryCode);
        params.add("phone", phone);
        params.add("device_os", deviceOs);
        params.add("device_os_version", deviceOsVersion);
        params.add("device_id", deviceId);
        Log.d("AddParams: " + params.toString());

        LoopjRestClient.post(ApiConfig.ADD_USER, params, responseHandler);
    }

    public static void verification(String userId, String verificationCode, TextHttpResponseHandler responseHandler) {

        RequestParams params = new RequestParams();
        params.add("user_id", userId);
        params.add("verification_code", verificationCode);

        LoopjRestClient.post(ApiConfig.VERIFICATION_CODE, params, responseHandler);
    }

    public static void setName(String userId, String name, TextHttpResponseHandler responseHandler) {

        RequestParams params = new RequestParams();
        params.add("user_id", userId);
        params.add("name", name);

        LoopjRestClient.post(ApiConfig.VERIFICATION_CODE, params, responseHandler);
    }

    public static void getFriendsNews(String userId, String friendJson, TextHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams();
        params.add("user_id", userId);
        params.add("friend_json", friendJson);

        LoopjRestClient.post(ApiConfig.GET_FRIEND_NEWS, params, responseHandler);
    }

    public static void sentMessage(String fromId, String message, File image, String friendList, TextHttpResponseHandler responseHandler) {

        RequestParams params = new RequestParams();
        params.add("from_id", fromId);
        params.add("message", message);
        params.add("friend_list", friendList);
        try {
            params.put("url", image);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        LoopjRestClient.post(ApiConfig.SENT_MESSAGE, params, responseHandler);
    }

    public static void getMessages(String userId, String limit, String offset, TextHttpResponseHandler responseHandler){
        RequestParams params = new RequestParams();
        params.add("user_id", userId);
        params.add("limit", limit);
        params.add("offset", offset);

        LoopjRestClient.post(ApiConfig.GET_ALL_MESSAGE, params, responseHandler);
    }

    public static void getComment(String receiveId, TextHttpResponseHandler responseHandler){
        RequestParams params = new RequestParams();
        params.add("received_id", receiveId);

        LoopjRestClient.post(ApiConfig.GET_COMMENT, params, responseHandler);
    }

    public static void addComment(String messageId, String userId, File file, String receiveId, TextHttpResponseHandler responseHandler){

        RequestParams params = new RequestParams();
        params.add("message_id", messageId);
        params.add("user_id", userId);
        params.add("received_id", receiveId);
        try {
            params.put("url", file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        LoopjRestClient.post(ApiConfig.ADD_COMMENT, params, responseHandler);
    }
}
