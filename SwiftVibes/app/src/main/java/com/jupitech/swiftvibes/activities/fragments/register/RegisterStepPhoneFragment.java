package com.jupitech.swiftvibes.activities.fragments.register;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.apis.loopj.RestClient;
import com.jupitech.swiftvibes.apis.parse.JSONConvert;
import com.jupitech.swiftvibes.models.RegisterResponseModel;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.utils.ToastUtil;
import com.jupitech.swiftvibes.utils.hardware.KeyboardUtil;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;

import java.util.HashMap;

public class RegisterStepPhoneFragment extends BaseFragment {

    Spinner mCountrySpinner;
    TextView mDigitText;
    EditText mPhoneEdt;

    String[] mCountries = new String[]{};
    HashMap<String, String> mData = null;
    String mPhoneNumber;
    String mCountryCode;


    public static RegisterStepPhoneFragment newInstance(String param1, String param2) {
        RegisterStepPhoneFragment fragment = new RegisterStepPhoneFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public RegisterStepPhoneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        initModels();
    }

    @Override
    protected void initModels() {
        mData = init();

        mCountries = new String[mData.size()];
        int count = 0;
        for (String country : mData.keySet()) {
            mCountries[count] = country;
            count++;
        }

        for (int i = 0; i < mCountries.length - 1; i++) {
            for (int j = i + 1; j < mCountries.length; j++) {
                if(mCountries[i].compareTo(mCountries[j]) > 0){
                    String temp = mCountries[i];
                    mCountries[i] = mCountries[j];
                    mCountries[j] = temp;
                }
            }
        }
    }

    @Override
    protected void initViews(View view) {
        mCountrySpinner = (Spinner) view.findViewById(R.id.country_spinner);
        mDigitText = (TextView) view.findViewById(R.id.digit_text);
        mPhoneEdt = (EditText) view.findViewById(R.id.phone_edt);

        mPhoneEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String phone = mPhoneEdt.getText().toString().trim();
                if(phone.indexOf("0") == 0){
                    phone = phone.replace("0", "");
                    mPhoneEdt.setText(phone);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        view.findViewById(R.id.done_text).setOnClickListener(this);

        createSpinnerAdapter(mCountrySpinner, mCountries, new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mDigitText.setText(mData.get(mCountries[position]));
                mCountryCode = mData.get(mCountries[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register_step_phone, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done_text:
                register();
                break;
        }
    }

    public boolean validateFields() {
        mPhoneEdt.setError(null);
        mPhoneNumber = mPhoneEdt.getText().toString().trim();

        Log.d("Phone: " + mPhoneNumber);
        if(mPhoneNumber.indexOf("0") == 0){
            mPhoneNumber = mPhoneNumber.substring(1, mPhoneNumber.length());
            Log.d("Phone: " + mPhoneNumber);
        }

        if (mPhoneEdt.length() == 0) {
            mPhoneEdt.setError("Please enter your phonenumber");
            return false;
        }

        if (mPhoneNumber.length() < 8) {
            mPhoneEdt.setError("Phonenumber invalid");
            return false;
        }

        return true;
    }

    public void register() {
        if (!validateFields()) return;

        String message = "%s %s <br/> <br/> Is your phone number above correct?";
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("NUMBER CONFIRMATION:")
                .setMessage(Html.fromHtml(String.format(message, mCountryCode, mPhoneNumber)))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        callRegisterApi();
                    }
                })
                .setNegativeButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        mPhoneEdt.requestFocus();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void callRegisterApi() {
        KeyboardUtil.hideSoftKeyboard(getActivity());
        String countryCode = mCountryCode;
        String phone = mPhoneNumber;
        String deviceOs = "Android";
        String deviceOsVersion = Build.VERSION.RELEASE;
        String deviceId = mPreferenceManager.getRegisterId();

        RestClient.addUser(countryCode, phone, deviceOs, deviceOsVersion, deviceId, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                RegisterResponseModel result = JSONConvert.parseAddUserResponse(responseBody);
                if (result.getUserId().length() > 0) {
                    UserModel userModel = new UserModel();
                    userModel.setPhone(result.getPhone());
                    userModel.setCountryCode(result.getCountryCode());
                    userModel.setUserId(result.getUserId());
                    userModel.setFriend(0);
                    userModel.setLogined(false);
                    userModel.setEnterDigit(false);
                    UserModel.add(userModel);
                    switchFragment(RegisterStepDigitFragment.newInstance(result.getUserId(), result.getPhone(), result.getCountryCode()));
                } else {
                    ToastUtil.show(result.getMessage());
                }
            }

            @Override
            public void onFailure(String responseBody, Throwable error) {
                super.onFailure(responseBody, error);
                ToastUtil.show(getString(R.string.error));
            }
        });
    }

    public HashMap<String, String> init() {
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("Canada", "+1");
        data.put("China", "+86");
        data.put("France", "+33");
        data.put("Germany", "+49");
        data.put("India", "+91");
        data.put("Japan", "+81");
        data.put("Pakistan", "+92");
        data.put("United Kingdom", "+44");
        data.put("United States", "+1");
        data.put("Abkhazia", "+7 840");
        data.put("Abkhazia", "+7 940");
        data.put("Afghanistan", "+93");
        data.put("Albania", "+355");
        data.put("Algeria", "+213");
        data.put("American Samoa", "+1 684");
        data.put("Andorra", "+376");
        data.put("Angola", "+244");
        data.put("Anguilla", "+1 264");
        data.put("Antigua and Barbuda", "+1 268");
        data.put("Argentina", "+54");
        data.put("Armenia", "+374");
        data.put("Aruba", "+297");
        data.put("Ascension", "+247");
        data.put("Australia", "+61");
        data.put("Australian External Territories", "+672");
        data.put("Austria", "+43");
        data.put("Azerbaijan", "+994");
        data.put("Bahamas", "+1 242");
        data.put("Bahrain", "+973");
        data.put("Bangladesh", "+880");
        data.put("Barbados", "+1 246");
        data.put("Barbuda", "+1 268");
        data.put("Belarus", "+375");
        data.put("Belgium", "+32");
        data.put("Belize", "+501");
        data.put("Benin", "+229");
        data.put("Bermuda", "+1 441");
        data.put("Bhutan", "+975");
        data.put("Bolivia", "+591");
        data.put("Bosnia and Herzegovina", "+387");
        data.put("Botswana", "+267");
        data.put("Brazil", "+55");
        data.put("British Indian Ocean Territory", "+246");
        data.put("British Virgin Islands", "+1 284");
        data.put("Brunei", "+673");
        data.put("Bulgaria", "+359");
        data.put("Burkina Faso", "+226");
        data.put("Burundi", "+257");
        data.put("Cambodia", "+855");
        data.put("Cameroon", "+237");
        data.put("Canada", "+1");
        data.put("Cape Verde", "+238");
        data.put("Cayman Islands", "+ 345");
        data.put("Central African Republic", "+236");
        data.put("Chad", "+235");
        data.put("Chile", "+56");
        data.put("China", "+86");
        data.put("Christmas Island", "+61");
        data.put("Cocos-Keeling Islands", "+61");
        data.put("Colombia", "+57");
        data.put("Comoros", "+269");
        data.put("Congo", "+242");
        data.put("Congo, Dem. Rep. of (Zaire)", "+243");
        data.put("Cook Islands", "+682");
        data.put("Costa Rica", "+506");
        data.put("Ivory Coast", "+225");
        data.put("Croatia", "+385");
        data.put("Cuba", "+53");
        data.put("Curacao", "+599");
        data.put("Cyprus", "+537");
        data.put("Czech Republic", "+420");
        data.put("Denmark", "+45");
        data.put("Diego Garcia", "+246");
        data.put("Djibouti", "+253");
        data.put("Dominica", "+1 767");
        data.put("Dominican Republic", "+1 809");
        data.put("Dominican Republic", "+1 829");
        data.put("Dominican Republic", "+1 849");
        data.put("East Timor", "+670");
        data.put("Easter Island", "+56");
        data.put("Ecuador", "+593");
        data.put("Egypt", "+20");
        data.put("El Salvador", "+503");
        data.put("Equatorial Guinea", "+240");
        data.put("Eritrea", "+291");
        data.put("Estonia", "+372");
        data.put("Ethiopia", "+251");
        data.put("Falkland Islands", "+500");
        data.put("Faroe Islands", "+298");
        data.put("Fiji", "+679");
        data.put("Finland", "+358");
        data.put("France", "+33");
        data.put("French Antilles", "+596");
        data.put("French Guiana", "+594");
        data.put("French Polynesia", "+689");
        data.put("Gabon", "+241");
        data.put("Gambia", "+220");
        data.put("Georgia", "+995");
        data.put("Germany", "+49");
        data.put("Ghana", "+233");
        data.put("Gibraltar", "+350");
        data.put("Greece", "+30");
        data.put("Greenland", "+299");
        data.put("Grenada", "+1 473");
        data.put("Guadeloupe", "+590");
        data.put("Guam", "+1 671");
        data.put("Guatemala", "+502");
        data.put("Guinea", "+224");
        data.put("Guinea-Bissau", "+245");
        data.put("Guyana", "+595");
        data.put("Haiti", "+509");
        data.put("Honduras", "+504");
        data.put("Hong Kong SAR China", "+852");
        data.put("Hungary", "+36");
        data.put("Iceland", "+354");
        data.put("India", "+91");
        data.put("Indonesia", "+62");
        data.put("Iran", "+98");
        data.put("Iraq", "+964");
        data.put("Ireland", "+353");
        data.put("Israel", "+972");
        data.put("Italy", "+39");
        data.put("Jamaica", "+1 876");
        data.put("Japan", "+81");
        data.put("Jordan", "+962");
        data.put("Kazakhstan", "+7 7");
        data.put("Kenya", "+254");
        data.put("Kiribati", "+686");
        data.put("North Korea", "+850");
        data.put("South Korea", "+82");
        data.put("Kuwait", "+965");
        data.put("Kyrgyzstan", "+996");
        data.put("Laos", "+856");
        data.put("Latvia", "+371");
        data.put("Lebanon", "+961");
        data.put("Lesotho", "+266");
        data.put("Liberia", "+231");
        data.put("Libya", "+218");
        data.put("Liechtenstein", "+423");
        data.put("Lithuania", "+370");
        data.put("Luxembourg", "+352");
        data.put("Macau SAR China", "+853");
        data.put("Macedonia", "+389");
        data.put("Madagascar", "+261");
        data.put("Malawi", "+265");
        data.put("Malaysia", "+60");
        data.put("Maldives", "+960");
        data.put("Mali", "+223");
        data.put("Malta", "+356");
        data.put("Marshall Islands", "+692");
        data.put("Martinique", "+596");
        data.put("Mauritania", "+222");
        data.put("Mauritius", "+230");
        data.put("Mayotte", "+262");
        data.put("Mexico", "+52");
        data.put("Micronesia", "+691");
        data.put("Midway Island", "+1 808");
        data.put("Micronesia", "+691");
        data.put("Moldova", "+373");
        data.put("Monaco", "+377");
        data.put("Mongolia", "+976");
        data.put("Montenegro", "+382");
        data.put("Montserrat", "+1664");
        data.put("Morocco", "+212");
        data.put("Myanmar", "+95");
        data.put("Namibia", "+264");
        data.put("Nauru", "+674");
        data.put("Nepal", "+977");
        data.put("Netherlands", "+31");
        data.put("Netherlands Antilles", "+599");
        data.put("Nevis", "+1 869");
        data.put("New Caledonia", "+687");
        data.put("New Zealand", "+64");
        data.put("Nicaragua", "+505");
        data.put("Niger", "+227");
        data.put("Nigeria", "+234");
        data.put("Niue", "+683");
        data.put("Norfolk Island", "+672");
        data.put("Northern Mariana Islands", "+1 670");
        data.put("Norway", "+47");
        data.put("Oman", "+968");
        data.put("Pakistan", "+92");
        data.put("Palau", "+680");
        data.put("Palestinian Territory", "+970");
        data.put("Panama", "+507");
        data.put("Papua New Guinea", "+675");
        data.put("Paraguay", "+595");
        data.put("Peru", "+51");
        data.put("Philippines", "+63");
        data.put("Poland", "+48");
        data.put("Portugal", "+351");
        data.put("Puerto Rico", "+1 787");
        data.put("Puerto Rico", "+1 939");
        data.put("Qatar", "+974");
        data.put("Reunion", "+262");
        data.put("Romania", "+40");
        data.put("Russia", "+7");
        data.put("Rwanda", "+250");
        data.put("Samoa", "+685");
        data.put("San Marino", "+378");
        data.put("Saudi Arabia", "+966");
        data.put("Senegal", "+221");
        data.put("Serbia", "+381");
        data.put("Seychelles", "+248");
        data.put("Sierra Leone", "+232");
        data.put("Singapore", "+65");
        data.put("Slovakia", "+421");
        data.put("Slovenia", "+386");
        data.put("Solomon Islands", "+677");
        data.put("South Africa", "+27");
        data.put("South Georgia and the South Sandwich Islands", "+500");
        data.put("Spain", "+34");
        data.put("Sri Lanka", "+94");
        data.put("Sudan", "+249");
        data.put("Suriname", "+597");
        data.put("Swaziland", "+268");
        data.put("Sweden", "+46");
        data.put("Switzerland", "+41");
        data.put("Syria", "+963");
        data.put("Taiwan", "+886");
        data.put("Tajikistan", "+992");
        data.put("Tanzania", "+255");
        data.put("Thailand", "+66");
        data.put("Timor Leste", "+670");
        data.put("Togo", "+228");
        data.put("Tokelau", "+690");
        data.put("Tonga", "+676");
        data.put("Trinidad and Tobago", "+1 868");
        data.put("Tunisia", "+216");
        data.put("Turkey", "+90");
        data.put("Turkmenistan", "+993");
        data.put("Turks and Caicos Islands", "+1 649");
        data.put("Tuvalu", "+688");
        data.put("Uganda", "+256");
        data.put("Ukraine", "+380");
        data.put("United Arab Emirates", "+971");
        data.put("United Kingdom", "+44");
        data.put("United States", "+1");
        data.put("Uruguay", "+598");
        data.put("U.S. Virgin Islands", "+1 340");
        data.put("Uzbekistan", "+998");
        data.put("Vanuatu", "+678");
        data.put("Venezuela", "+58");
        data.put("Vietnam", "+84");
        data.put("Wake Island", "+1 808");
        data.put("Wallis and Futuna", "+681");
        data.put("Yemen", "+967");
        data.put("Zambia", "+260");
        data.put("Zanzibar", "+255");
        data.put("Zimbabwe", "+263");
        return data;
    }
}
