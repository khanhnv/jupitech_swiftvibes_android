package com.jupitech.swiftvibes.apis.loopj;

import org.apache.http.Header;

public interface IStrageryHandleError {
	public void handleError(int statusCode, Header[] headers,
                            String responseString, Throwable throwable);
}
