package com.jupitech.swiftvibes.activities.fragments.capture;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;

public class ProgressImageFragment extends BaseFragment {

    public static final String PARAM_IMAGE = "image";
    ImageView mImageView;
    String mPath;

    public static ProgressImageFragment newInstance(String path){
        ProgressImageFragment fragment = new ProgressImageFragment();

        Bundle extras = new Bundle();
        extras.putString(PARAM_IMAGE, path);

        fragment.setArguments(extras);

        return fragment;
    }

    public ProgressImageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPath = getArguments().getString(PARAM_IMAGE);
        }
        initModels();
    }

    @Override
    protected void initModels() {

    }

    @Override
    protected void initViews(View view) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_progress_image, container, false);
        initViews(view);
        return view;
    }



    @Override
    public void onClick(View v) {

    }

}
