package com.jupitech.swiftvibes.utils.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.utils.Log;

public class UniversalImageUtil {

    private static UniversalImageUtil instance = null;
    private Context context;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    private ImageLoadingListener loadingListener;
    private ImageLoadingProgressListener progressListener;

    public UniversalImageUtil(Context context) {
        this.context = context;
        initImageLoader();
    }

    public static UniversalImageUtil getInstance(Context context) {
        if (instance == null) {
            instance = new UniversalImageUtil(context);
        }
        return instance;
    }

    /**
     * Init image loader in application class
     */
    public void initImageLoaderInMyApp() {
        // This configuration tuning is custom. You can tune every option, you
        // may tune some of them,
        // or you can create default configuration by
        // ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                context).threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO).build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }

    /**
     * Init universal image loader
     */
    private void initImageLoader() {
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.ic_stub)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error).cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();

        loadingListener = new ImageLoadingListener() {

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view,
                                          Bitmap loadedImage) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view,
                                        FailReason failReason) {

            }

            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }
        };

        progressListener = new ImageLoadingProgressListener() {

            @Override
            public void onProgressUpdate(String imageUri, View view,
                                         int current, int total) {
                // TODO
                Log.d("load " + imageUri + " " + current + "/" + total);
            }
        };
    }

    public void displayImage(String imgUri, ImageView imgView,
                             ImageLoadingListener imgListener) {
        imageLoader.displayImage(imgUri, imgView, options, imgListener);
    }

    public void displayImage(String imgUri, ImageView imgView) {
        imageLoader.displayImage(imgUri, imgView, options, loadingListener,
                progressListener);
    }

    public Bitmap loadBitmapFromUrl(String imageUri) {
        return imageLoader.loadImageSync(imageUri);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    public DisplayImageOptions getImageOptions() {
        return options;
    }

}
