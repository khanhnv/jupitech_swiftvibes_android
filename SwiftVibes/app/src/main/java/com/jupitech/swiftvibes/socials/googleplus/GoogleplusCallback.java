package com.jupitech.swiftvibes.socials.googleplus;

import com.google.android.gms.plus.model.people.Person;

import java.util.List;

public interface GoogleplusCallback {
	
	void onShareResult(boolean isSuccess);

	void onLoadPersonInCircle(List<Person> persons);
}
