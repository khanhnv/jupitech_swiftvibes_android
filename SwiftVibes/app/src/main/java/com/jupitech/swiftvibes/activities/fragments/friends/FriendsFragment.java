package com.jupitech.swiftvibes.activities.fragments.friends;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.jupitech.swiftvibes.AppApplication;
import com.jupitech.swiftvibes.BusProvider;
import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.adapters.FriendsAdapter;
import com.jupitech.swiftvibes.models.NotificationEvent;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.services.AppIntentService;
import com.jupitech.swiftvibes.utils.Log;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class FriendsFragment extends BaseFragment {

    ListView mListView;
    FriendsAdapter mAdapter;

    List<UserModel> mFriendModels = new ArrayList<UserModel>();

    public FriendsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        initModels();
    }

    @Override
    protected void initModels() {
        mFriendModels = UserModel.getFriends();
//        Log.d("Friend: " + mFriendModels.toString());
    }

    @Override
    protected void initViews(View view) {
        mListView = (ListView) view.findViewById(R.id.listView);

        view.findViewById(R.id.add_text).setOnClickListener(this);

        mAdapter = new FriendsAdapter(getActivity(), 0, mFriendModels);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_friends, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        BusProvider.getInstance().register(this);

        showProgress();
        AppIntentService.startActionFetchFriend(AppApplication.get());
    }

    @Override
    public void onPause() {
        super.onPause();

        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void onNotificationEvent(NotificationEvent event) {
        if(event.getAction().equals(NotificationEvent.ACTION_UPDATE_FRIEND)) {
            Log.d("Update friends");
            hideProgress();
            mFriendModels = UserModel.getFriends();
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_text:
                openContacts();
                break;
        }
    }

    public void openContacts() {
        Intent intent = new Intent(Intent.ACTION_VIEW, ContactsContract.Contacts.CONTENT_URI);
        startActivity(intent);
    }


}
