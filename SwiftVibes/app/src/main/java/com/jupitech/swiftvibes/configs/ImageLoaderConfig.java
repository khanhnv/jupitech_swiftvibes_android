package com.jupitech.swiftvibes.configs;

public class ImageLoaderConfig {

	public static final String FROM_SD_CARD = "file:///";
	public static final String FROM_CONTENT_PROVIDER = "content://";
	public static final String FROM_ASSETS = "assets://";
	public static final String FROM_DRAWABLE = "drawable://";
}
