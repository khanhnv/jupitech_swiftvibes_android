package com.jupitech.swiftvibes.utils.media;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

import java.io.IOException;

/**
 * Created by brucenguyen on 8/15/14.
 */
public class DemoPlayer {

	private static DemoPlayer instance;
	private MediaPlayer mediaPlayer;

	private boolean isPrepare = false;

	public DemoPlayer() {
	}

	public static DemoPlayer getInstance() {
		if (instance == null) {
			instance = new DemoPlayer();
		}
		return instance;
	}

	private void prepare(String path) {
		try {
			mediaPlayer = new MediaPlayer();
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mediaPlayer.setDataSource(path);
			mediaPlayer.prepare();
			mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mediaPlayer) {

				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean isPlaying() {
		return mediaPlayer.isPlaying();
	}

	public void play(String path) {
		if (!isPrepare()) {
			isPrepare = true;
			prepare(path);
		}
		if (!isPlaying()) {
			mediaPlayer.start();
		} else {
			mediaPlayer.pause();
		}
	}

	public void release() {
		isPrepare = false;
		try {
			if (mediaPlayer != null) {
				if (mediaPlayer.isPlaying()) {
					mediaPlayer.stop();
				}
				mediaPlayer.release();
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
	}

	public int getCurrentPosition() {
		return mediaPlayer.getCurrentPosition();
	}

	public int getDuration() {
		return mediaPlayer.getDuration();
	}

	public boolean isPrepare() {
		return isPrepare;
	}

	public MediaPlayer getMediaPlayer() {
		return mediaPlayer;
	}

	public void setMediaPlayer(MediaPlayer mediaPlayer) {
		this.mediaPlayer = mediaPlayer;
	}

}
