package com.jupitech.swiftvibes.configs;

public class DatabaseConfig {

	// name of the database file for your application -- change to something
	// appropriate for your app
	public static final String DATABASE_NAME = "SwifVibes.db";
	// any time you make changes to your database objects, you may have to
	// increase the database version
	public static final int DATABASE_VERSION = 1;
}
