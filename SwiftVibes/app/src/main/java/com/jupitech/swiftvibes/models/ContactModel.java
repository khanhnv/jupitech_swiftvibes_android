package com.jupitech.swiftvibes.models;

import android.widget.Checkable;

import com.jupitech.swiftvibes.listeners.Item;

import java.io.Serializable;


public class ContactModel implements Serializable, Checkable, Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5776383704128127589L;

	public String contactId;
	public String groupId;
	public String contactName;
	public String phoneNumber;
	public String countryCode;
	public String countryId;
	public String birthday;
	public String title;
	public String titleId;
	public String email;
	public String photo;
	public boolean selected;
	public boolean displayAsImport;
	public String groupName;
	public boolean mFollow;

	@Override
	public boolean isChecked() {
		return selected;
	}

	@Override
	public void setChecked(boolean checked) {
		selected = checked;
	}

	@Override
	public void toggle() {
		selected = !selected;
	}

	@Override
	public boolean isSection() {
		return false;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleId() {
		return titleId;
	}

	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public boolean isDisplayAsImport() {
		return displayAsImport;
	}

	public void setDisplayAsImport(boolean displayAsImport) {
		this.displayAsImport = displayAsImport;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public boolean ismFollow() {
		return mFollow;
	}

	public void setmFollow(boolean mFollow) {
		this.mFollow = mFollow;
	}

}
