package com.jupitech.swiftvibes.models;

/**
 * Created by brucenguyen on 10/14/14.
 */
public class ContactJson {


    private String country_code;
    private String phone;


    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "ContactJson{" +
                "country_code='" + country_code + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
