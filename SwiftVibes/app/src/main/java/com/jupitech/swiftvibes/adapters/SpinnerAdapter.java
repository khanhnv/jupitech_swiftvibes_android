package com.jupitech.swiftvibes.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;

public class SpinnerAdapter extends ArrayAdapter<String> {

	public SpinnerAdapter(Context context, int textViewResourceId,
                          String[] objects) {
		super(context, textViewResourceId, objects);
	}

}
