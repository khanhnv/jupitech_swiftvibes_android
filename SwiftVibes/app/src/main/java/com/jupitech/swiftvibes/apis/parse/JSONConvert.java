package com.jupitech.swiftvibes.apis.parse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.jupitech.swiftvibes.models.CommentModel;
import com.jupitech.swiftvibes.models.FriendResponseModel;
import com.jupitech.swiftvibes.models.PostModel;
import com.jupitech.swiftvibes.models.RegisterResponseModel;
import com.jupitech.swiftvibes.models.ResponseModel;
import com.jupitech.swiftvibes.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

public class JSONConvert {

    public static Gson mGson = new Gson();


    public static RegisterResponseModel parseAddUserResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            RegisterResponseModel result = new RegisterResponseModel();
            result.setStatus(JSONUtil.getInt(jsonObject, "status"));
            result.setMessage(JSONUtil.getString(jsonObject, "message"));
            JSONObject jsonData = jsonObject.getJSONObject("data");
            result.setVerificationCode(JSONUtil.getString(jsonData, "verification_code"));
            result.setUserId(JSONUtil.getString(jsonData, "user_id"));
            result.setPhone(JSONUtil.getString(jsonData, "phone"));
            result.setCountryCode(JSONUtil.getString(jsonData, "country_code"));
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ResponseModel parseResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            ResponseModel result = new ResponseModel();
            result.setStatus(JSONUtil.getInt(jsonObject, "status"));
            result.setMessage(JSONUtil.getString(jsonObject, "message"));
            return result;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<UserModel> parseFriendResponse(String response) {
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<FriendResponseModel>() {
        }.getType();
        FriendResponseModel model = (FriendResponseModel) gson.fromJson(response, listType);
        return model.getData();
    }


    public static List<PostModel> parseMessageResponse(String response){
        Gson gson = new GsonBuilder().create();

        try {
            JSONObject jsonObject = new JSONObject(response);
            int status = jsonObject.getInt("status");
            if(status == 0)
                return null;
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            String json = jsonArray.toString();

            Type listType = new TypeToken<List<PostModel>>() {
            }.getType();
            return (List<PostModel>) gson.fromJson(json, listType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static CommentModel parseCommentResponse(String response){
        Gson gson = new GsonBuilder().create();

        try {
            JSONObject jsonObject = new JSONObject(response);
            int status = jsonObject.getInt("status");
            if(status == 0)
                return null;
            JSONObject jsonData = jsonObject.getJSONObject("data");
            String json = jsonData.toString();

            Type listType = new TypeToken<CommentModel>() {
            }.getType();
            return (CommentModel) gson.fromJson(json, listType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
