package com.jupitech.swiftvibes.activities.fragments.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.CaptureActivity;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.apis.loopj.RestClient;
import com.jupitech.swiftvibes.apis.parse.JSONConvert;
import com.jupitech.swiftvibes.models.ResponseModel;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.utils.ToastUtil;
import com.jupitech.swiftvibes.utils.hardware.KeyboardUtil;
import com.jupitech.swiftvibes.utils.intent.LaunchIntent;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;

public class RegisterStepNameFragment extends BaseFragment {

    private static final String ARG_USER_ID = "user_id";
    private String mUserId;
    private String mName;

    private EditText mNameEdt;

//    public static RegisterStepNameFragment newInstance(String userId) {
//        RegisterStepNameFragment fragment = new RegisterStepNameFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_USER_ID, userId);
//        fragment.setArguments(args);
//        return fragment;
//    }

    public RegisterStepNameFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserId = getArguments().getString(ARG_USER_ID);
        }
        initModels();
    }

    @Override
    protected void initModels() {
        UserModel userModel = UserModel.getUser();
        mUserId = userModel.getUserId();
    }

    @Override
    protected void initViews(View view) {
        mNameEdt = (EditText) view.findViewById(R.id.name_edt);
        view.findViewById(R.id.done_text).setOnClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register_step_name, container, false);
        initViews(view);
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done_text:
                setUserName();
                break;
        }
    }

    public boolean validateFields() {
        mNameEdt.setError(null);
        mName = mNameEdt.getText().toString().trim();

        if (mName.length() == 0) {
            mNameEdt.setError("Please enter your name");
            return false;
        }

        return true;
    }

    public void setUserName() {
        if (!validateFields()) return;
        KeyboardUtil.hideSoftKeyboard(getActivity());

        final String userId = mUserId;
        String name = mName;

        RestClient.setName(userId, name, new TextHttpResponseHandler() {

            @Override
            public void onFailure(String responseBody, Throwable error) {
                super.onFailure(responseBody, error);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                ResponseModel response = JSONConvert.parseResponse(responseBody);

                if (response.getStatus() == 1) {
                    UserModel userModel = UserModel.getUser();
                    if (userModel != null) {
                        userModel.setName(mName);
                        userModel.setLogined(true);
                        userModel.save();
                    }
                    LaunchIntent.start(getActivity(), CaptureActivity.class, Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getActivity().finish();
                } else {
                    ToastUtil.show("Error, try again!");
                }
            }
        });
    }


}
