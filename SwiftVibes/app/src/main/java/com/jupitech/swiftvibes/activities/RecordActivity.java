package com.jupitech.swiftvibes.activities;

import android.os.Bundle;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.base.BaseActionBarActivity;

public class RecordActivity extends BaseActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
    }

    @Override
    protected void initModels() {

    }

    @Override
    protected void initViews() {

    }
}
