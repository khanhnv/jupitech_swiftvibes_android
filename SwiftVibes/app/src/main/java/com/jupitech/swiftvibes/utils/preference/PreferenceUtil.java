package com.jupitech.swiftvibes.utils.preference;

import android.content.Context;

import com.jupitech.swiftvibes.models.DeviceInfoModel;
import com.jupitech.swiftvibes.models.UserModel;

public class PreferenceUtil {

    static final String IS_FIRST_RUN_APP = "is_first_run_app";
    static final String KEY_FIRST_NAME = "first_name";
    static final String KEY_LAST_NAME = "last_name";
    static final String USER_PURCHASE_IS_LOGIN = "purchaseIsLogin";
    static final String SETTING_LANGUAGE = "setting_language";
    static final String SETTING_LANGUAGE_DEFAULT = "default";
    static final String SETTING_LANGUAGE_ZH = "zh";
    static final String PREF_SETTING_TWITTER_TOKEN_KEY = "PREF_SETTING_TWITTER_TOKEN_KEY";
    static final String PREF_SETTING_TWITTER_TOKEN_SECRET = "PREF_SETTING_TWITTER_TOKEN_SECRET";
    static final String PREF_SETTING_TWITTER_USER_NAME = "PREF_SETTING_TWITTER_USER_NAME";
    static final String REGISTER_ID = "register_id";
    static final String SET_UP_PURCHASED = "set_up_purchased";
    static final String SKU = "sku";
    static final String UNREAD_MESSAGE = "unread_message";

    private static PreferenceUtil instance;
    private PreferenceHelper preference;

    private PreferenceUtil(Context context) {
        this.preference = PreferenceHelper.getInstance(context);
    }

    public static PreferenceUtil getInstance(Context context) {
        if (instance == null) {
            instance = new PreferenceUtil(context);
        }
        return instance;
    }

    /**
     * clear values
     */
    public void clear() {
        preference.clear();
    }

    public boolean isFirstRunApp() {
        return preference.getBoolean(IS_FIRST_RUN_APP, true);
    }

    public void setFirstRunApp(boolean run) {
        preference.putBoolean(IS_FIRST_RUN_APP, run);
    }

    public boolean getPurchaseLogin() {
        return preference.getBoolean(USER_PURCHASE_IS_LOGIN, false);
    }

    public void setPurchaseLogin(boolean login) {
        preference.putBoolean(USER_PURCHASE_IS_LOGIN, login);
    }

    public UserModel getUserInfo() {
        UserModel model = new UserModel();
        model.setFirstName(preference.getString(KEY_FIRST_NAME, ""));
        model.setLastName(preference.getString(KEY_LAST_NAME, ""));
        return model;
    }

    public void setUserInfo(UserModel model) {
        preference.putString(KEY_FIRST_NAME, model.getFirstName());
        preference.putString(KEY_LAST_NAME, model.getLastName());
    }

    public String getLanguage() {
        return preference.getString(SETTING_LANGUAGE, SETTING_LANGUAGE_DEFAULT);
    }

    public void setLanguage(String language) {
        preference.putString(SETTING_LANGUAGE, language);
    }

    /**
     * Get user twitter token key
     *
     * @return
     */
    public String getUserTwitterTokenKey() {
        return preference.getString(PREF_SETTING_TWITTER_TOKEN_KEY, "");
    }

    /**
     * Get user twitter token secret
     *
     * @return
     */
    public String getUserTwitterTokenSecret() {
        return preference.getString(PREF_SETTING_TWITTER_TOKEN_SECRET, "");
    }

    /**
     * get register id
     *
     * @return
     */
    public String getRegisterId() {
        return preference.getString(REGISTER_ID, "");
    }

    /**
     * set register id
     *
     * @param registerId
     */
    public void setRegisterId(String registerId) {
        preference.putString(REGISTER_ID, registerId);
    }

    /**
     * @param model
     */
    public void setDeviceInfo(DeviceInfoModel model) {
        preference.putInt("device_width", model.getWidth());
        preference.putInt("device_height", model.getHeight());
        preference.putInt("device_orientation", model.getOrientation());
        preference.putBoolean("device_is_tablet", model.isTablet());
    }

    /**
     * @return
     */
    public DeviceInfoModel getDeviceInfoModel() {
        DeviceInfoModel model = new DeviceInfoModel();
        model.setWidth(preference.getInt("device_width", 0));
        model.setHeight(preference.getInt("device_height", 0));
        model.setOrientation(preference.getInt("device_orientation", 0));
        model.setTablet(preference.getBoolean("device_is_tablet", false));
        return model;
    }

    public void setupPurchased(boolean setup) {
        preference.putBoolean(SET_UP_PURCHASED, setup);
    }

    public boolean isSetupPurchased() {
        return preference.getBoolean(SET_UP_PURCHASED, false);
    }

    public String getSku() {
        return preference.getString(SKU, "");
    }

    public void setSku(String sku) {
        preference.putString(SKU, sku);
    }

    public void setUnReadMessage(String number){
        preference.putString(UNREAD_MESSAGE, number);
    }

    public String getUnReadMessage(){
        return preference.getString(UNREAD_MESSAGE, "0");
    }

}
