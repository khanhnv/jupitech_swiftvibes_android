package com.jupitech.swiftvibes.activities.base;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;

import com.android.vending.billing.IInAppBillingService;
import com.android.vending.billing.util.IabHelper;
import com.android.vending.billing.util.IabResult;
import com.android.vending.billing.util.Inventory;
import com.android.vending.billing.util.Purchase;
import com.google.android.gcm.GCMRegistrar;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.jupitech.swiftvibes.AppApplication;
import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.MainActivity;
import com.jupitech.swiftvibes.configs.DeveloperConfig;
import com.jupitech.swiftvibes.configs.GooglePlayConfig;
import com.jupitech.swiftvibes.dialogs.AlertDialogFragment;
import com.jupitech.swiftvibes.dialogs.ProgressDialogFragment;
import com.jupitech.swiftvibes.gcm.CommonUtilities;
import com.jupitech.swiftvibes.gcm.GCMUtil;
import com.jupitech.swiftvibes.listeners.AlertListener;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.socials.facebook.FacebookUtil;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.utils.hardware.WakeLocker;
import com.jupitech.swiftvibes.utils.preference.PreferenceUtil;
import com.jupitech.swiftvibes.utils.text.StringUtil;
import com.jupitech.swiftvibes.widgets.CustomTextView;

import java.util.Timer;
import java.util.TimerTask;

public abstract class BaseFragmentActivity extends FragmentActivity implements
        OnClickListener {

    protected final long TIMEOUT = 30000; // 30 seconds

    protected static BaseFragmentActivity mSelf;

    // Utils
    protected PreferenceUtil mPreferenceManager;
    protected Handler mHandler = new Handler();

    // contain user info, used to check login
    protected UserModel mUserModel;

    // contain all of global variable
    protected AppApplication mApplication;

    // fragment manager
    protected FragmentManager mFragmentManager;

    // Image Loader
    protected ImageLoader mImageLoader = ImageLoader.getInstance();
    protected DisplayImageOptions mOptions;

    // Custom Dialog
    protected AlertDialogFragment mAlertDialog;
    protected ProgressDialogFragment mProgressDialog;

    // navigation bar
    protected CustomTextView mTitleText;
    protected CustomTextView mLeftText;
    protected CustomTextView mRightText;
    protected ImageView mLeftImage;
    protected ImageView mRightImage;
    protected View mLoadingLayout;

    // GCM
    protected String mRegisterId;
    protected ProgressDialog mSpinner = null;
    protected Timer mGcmTimeoutTimer = null;

    protected FacebookUtil mFacebookUtil;

    // (arbitrary) request code for the purchase flow
    public static final int RC_REQUEST = 10001;
    // The helper object
    IabHelper mHelper;
    private String mProductPurchaseId;
    private String mPayload;
    private IInAppBillingService mService;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Switch to fullscreen view, getting rid of the status bar as well.
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        // WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // hide the keyboard everytime the activty starts
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mSelf = BaseFragmentActivity.this;

        mPreferenceManager = PreferenceUtil
                .getInstance(getApplicationContext());

        mUserModel = mPreferenceManager.getUserInfo();

        mApplication = (AppApplication) getApplication();

        mFragmentManager = getSupportFragmentManager();

        mOptions = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.noimage)
                .showImageOnFail(R.drawable.noimage).cacheInMemory(true)
                .cacheOnDisk(true).bitmapConfig(Bitmap.Config.RGB_565).build();

        mFacebookUtil = FacebookUtil.newInstance(this);
        mFacebookUtil.onCreate(bundle);

        // Create the helper, passing it our context and the public key to
        // verify signatures with
        Log.d("Creating IAB helper.");
        mHelper = new IabHelper(this, GooglePlayConfig.base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set
        // this to false).
        mHelper.enableDebugLogging(DeveloperConfig.DEVELOPER_MODE);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d("Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d("Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    complain("Problem setting up in-app billing: " + result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null)
                    return;

                // IAB is fully set up. Now, let's get an inventory of stuff we
                // own.
                Log.d("Setup successful. Querying inventory.");
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });

        /** register broadcast receiver */
        LocalBroadcastManager.getInstance(mSelf).registerReceiver(mGCMReceiver,
                new IntentFilter(GCMUtil.ACTION_BROADCAST_REGISTRATION_RESULT));
        LocalBroadcastManager.getInstance(mSelf).registerReceiver(mGCMReceiver,
                new IntentFilter(GCMUtil.ACTION_BROADCAST_MESSAGE));

        bindService(new
                        Intent("com.android.vending.billing.InAppBillingService.BIND"),
                mServiceConn, Context.BIND_AUTO_CREATE
        );
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mFacebookUtil.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mApplication.isBackPress()) {
            mApplication.setBackPress(false);
        }

        mFacebookUtil.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mApplication.setAppRunning(true);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mApplication.setAppRunning(false);
    }

    @Override
    protected void onStop() {
        super.onStop();

        mApplication.setAppRunning(false);

        mFacebookUtil.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mFacebookUtil.onActivityResult(this, requestCode, resultCode, data);

        Log.d("onActivityResult(" + requestCode + "," + resultCode + ","
                + data);
        if (mHelper == null)
            return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d("onActivityResult handled by IABUtil.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(mSelf).unregisterReceiver(
                mGCMReceiver);

        Log.d("Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }

        if (mService != null) {
            unbindService(mServiceConn);
        }
    }

    @Override
    public void onBackPressed() {

        mApplication.setBackPress(true);

        super.onBackPressed();

        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            this.finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    /**
     * Init all model when onCreate activity here
     */
    protected abstract void initModels();

    /**
     * Init all views when onCreate activity here
     */
    protected abstract void initViews();

    /**
     * Remove previous show dialog fragment by tag
     *
     * @param tag tag of dialog fragment
     */
    protected void removePreviousDialog(String tag) {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction. We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        Fragment prev = mFragmentManager.findFragmentByTag(tag);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.commit();
    }

    /**
     * set visible button
     *
     * @param showOrHide
     */
    private void setVisibleView(View view, boolean showOrHide) {
        view.setVisibility(showOrHide ? View.VISIBLE : View.INVISIBLE);
    }

    /**
     * set visible left button
     *
     * @param showOrHide : true if show, else hide is false
     */
    public void setVisibleLeftTextView(boolean showOrHide) {
        setVisibleView(mLeftText, showOrHide);
    }

    /**
     * set visible right button
     *
     * @param showOrHide : true if show, else hide is false
     */
    public void setVisibleRightTextView(boolean showOrHide) {
        setVisibleView(mRightText, showOrHide);
    }

    /**
     * @param showOrHide
     */
    public void setVisibleLeftImage(boolean showOrHide) {
        setVisibleView(mLeftImage, showOrHide);
    }

    /**
     * @param showOrHide
     */
    public void setVisibleRightImage(boolean showOrHide) {
        setVisibleView(mRightImage, showOrHide);
    }

    /**
     * @param showOrHide
     */
    public void setVisibleTitleText(boolean showOrHide) {
        setVisibleView(mTitleText, showOrHide);
    }

    /**
     * @param textView
     * @param text
     * @param drawable
     */
    private void setBackgroundTextView(CustomTextView textView, String text,
                                       int drawable) {
        textView.setBackgroundResource(drawable);
        if (text.length() > 0) {
            textView.setText(text);
        }
    }

    /**
     * @param imageView
     * @param drawable
     */
    private void setBackgroundImage(ImageView imageView, int drawable) {
        imageView.setBackgroundResource(drawable);
    }

    /**
     * @param text
     * @param drawable
     */
    public void setBackgroundLeftButton(String text, int drawable) {
        setBackgroundTextView(mLeftText, text, drawable);
    }

    /**
     * @param text
     * @param drawable
     */
    public void setBackgroundRightButton(String text, int drawable) {
        setBackgroundTextView(mRightText, text, drawable);
    }


    /**
     * @param drawable
     */
    public void setBackgroundLeftImage(int drawable) {
        setBackgroundImage(mLeftImage, drawable);
    }

    /**
     * @param drawable
     */
    public void setBackgroundRightImage(int drawable) {
        setBackgroundImage(mRightImage, drawable);
    }

    /**
     * @param text
     */
    public void setTitleText(String text) {
        if (!StringUtil.isEmpty(text)) {
            mTitleText.setText(text);
        }
    }

    /**
     * clear all fragment in backstack
     */
    public void clearBackStackFragment() {
        int length = mFragmentManager.getBackStackEntryCount();
        for (int i = 0; i < length; i++) {
            // remove each fragment in back statck
            mFragmentManager.popBackStack();
        }
    }

    /**
     * register GCM. This method is called onCreate activity
     */
    public void registerGCM() {

        try {
            // Make sure the device has the proper dependencies.
            GCMRegistrar.checkDevice(this);
            // Make sure the manifest was properly set - comment out this line
            // while developing the app, then uncomment it when it's ready.
            GCMRegistrar.checkManifest(this);
        } catch (Exception e) {

            // if check device fail, we show pop-up for user
            e.printStackTrace();
            showAlertDialog("This device does not support GCM.");
            return;
        }

        final String regId = GCMRegistrar.getRegistrationId(this);
        mPreferenceManager.setRegisterId(regId);
        Log.d("regId = " + regId);
        if (regId.length() == 0) {
            // Automatically registers application on startup.

            mSpinner = new ProgressDialog(mSelf);
            mSpinner.setMessage("Registering push notification service...");
            mSpinner.setCancelable(false);
            mSpinner.show();

            GCMRegistrar.register(this, CommonUtilities.SENDER_ID);

            // show alert register fail GCM when time out (30 seconds)
            mGcmTimeoutTimer = new Timer();
            mGcmTimeoutTimer.schedule(new StopGcmRegistrationTimerTask(),
                    TIMEOUT);

        } else {
            // Device is already registered on GCM, check server.
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                // register in our server successfully, skips registration.
                Log.d(getString(R.string.already_registered));
            } else {
                // do register in our server
                // registerDeviceToServer(mTeamModel.mAccessCode, regId);
                Log.d("register: " + regId);
            }
        }
    }

    /**
     * unregister GCM. This method is called on destroy activity
     */
    public void unregisterGCM() {
        try {
            GCMRegistrar.setRegisteredOnServer(mSelf, false);
            GCMRegistrar.onDestroy(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param context
     * @param intent
     */
    private void processRegistrationResult(Context context, Intent intent) {
        if (mGcmTimeoutTimer != null) {
            mGcmTimeoutTimer.cancel();
            mGcmTimeoutTimer = null;
        }

        if (mSpinner != null && mSpinner.isShowing()) {
            try {
                mSpinner.dismiss();
                mSpinner = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Bundle bundle = intent.getExtras();
        boolean isRegistrationSuccess = bundle
                .getBoolean(GCMUtil.EXTRA_REGISTRATION_SUCCESS);
        String message = bundle.getString(GCMUtil.EXTRA_REGISTRATION_CONTENT);

        if (isRegistrationSuccess) {
            /* get registration id */

            mPreferenceManager.setRegisterId(message);

            // registerDeviceToServer(mTeamModel.mAccessCode, message);
        } else {
            /* register failed after MAX_ATTEMPTS tries. Show errors message */
            if (message.equalsIgnoreCase(GCMUtil.ERR_INVALID_SENDER)) {
                showAlertDialog(
                        context.getString(R.string.MSG_GCM_INVALID_SENDER));
            } else if (message.equalsIgnoreCase(GCMUtil.ERR_ACCOUNT_MISSING)) {
                showAlertDialog(
                        context.getString(R.string.MSG_GCM_ACCOUNT_MISSING));
            } else if (message
                    .equalsIgnoreCase(GCMUtil.ERR_AUTHENTICATION_FAILED)) {
                showAlertDialog(

                        context.getString(R.string.MSG_GCM_AUTHENTICATION_FAILED));
            } else if (message.equalsIgnoreCase(GCMUtil.ERR_INVALID_PARAMETERS)) {
                showAlertDialog(
                        context.getString(R.string.MSG_GCM_INVALID_PARAMETERS));
            } else if (message
                    .equalsIgnoreCase(GCMUtil.ERR_PHONE_REGISTRATION_ERROR)) {
                showAlertDialog(

                        context.getString(R.string.MSG_GCM_PHONE_REGISTRATION_ERROR));
            } else if (message
                    .equalsIgnoreCase(GCMUtil.ERR_SERVICE_NOT_AVAILABLE)) {
                showAlertDialog(

                        context.getString(R.string.MSG_GCM_SERVICE_NOT_AVAILABLE));
            } else {
                showAlertDialog(message);
            }
        }
    }

    /**
     * @param context
     * @param intent
     */
    private void processMessageResult(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle == null)
            return;
        // TODO parse data here
    }

    @SuppressWarnings("deprecation")
    public void generateNotification(Context context, String message) {
        Log.d("show notification");
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        String title = context.getString(R.string.app_name);
        Intent notificationIntent = new Intent(context, MainActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, 0,
                notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);
    }

    /**
     * Switch content tab
     *
     * @param fragment
     */
    public void switchContent(int contentId, Fragment fragment) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(contentId, fragment);
        transaction.commit();
    }

    /**
     * Switch content tab, fragment will add to back stack
     *
     * @param fragment
     * @param tag
     */
    public void switchContent(int contentId, Fragment fragment, String tag) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(contentId, fragment);
        transaction.addToBackStack(tag);
        transaction.commit();
    }


    /**
     * show alert message
     *
     * @param message
     */
    public void showAlertDialog(String message) {
        // clear all state previous
        removePreviousDialog("alert_dialog");
        mAlertDialog = null;
        // create dialog
        mAlertDialog = AlertDialogFragment.newInstance(
                getString(R.string.app_name), message,
                getString(android.R.string.ok), getString(android.R.string.ok));
        mAlertDialog.showOnlyOneButton(true);
        // show dialog
        mAlertDialog.show(mFragmentManager, "alert_dialog");
    }

    /**
     * Show confirm dialog. The dialog contains left + right button
     *
     * @param title
     * @param message
     * @param leftText
     * @param rightText
     * @param listener
     */
    public void showConfirmDialog(String title, String message,
                                  String leftText, String rightText, AlertListener listener) {
        // clear all state previous
        removePreviousDialog("alert_dialog");
        mAlertDialog = null;
        // create dialog
        mAlertDialog = AlertDialogFragment.newInstance(title, message,
                leftText, rightText, listener);
        // show dialog
        mAlertDialog.show(mFragmentManager, "alert_dialog");
    }

    /**
     * Show loading dialog.
     *
     * @param title   title of dialog
     * @param message message of dialog
     */
    public void showLoadingDialog(String title, String message) {
        removePreviousDialog("progress_dialog");
        mProgressDialog = null;
        mProgressDialog = ProgressDialogFragment.newInstance(title, message);
        mProgressDialog.show(mFragmentManager, "progress_dialog");
    }

    /**
     * timer task to hide wait dialog when gcm registration timeout
     */
    class StopGcmRegistrationTimerTask extends TimerTask {
        @Override
        public void run() {
            Looper.prepare();
            if (mSpinner != null && mSpinner.isShowing()) {
                mSpinner.dismiss();
                mSpinner = null;
            }

            mPreferenceManager.setRegisterId("");

            showAlertDialog(
                    getString(R.string.not_able_register_gcm));
            Looper.loop();
            Looper.myLooper().quit();
        }
    }

    /**
     * Broadcast Receiver: listen message from GCM intent service to got
     * registration Id/error, then call API to notify server to update device
     * token
     */
    public BroadcastReceiver mGCMReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            // get action
            String action = intent.getAction();
            Log.d("action: " + action);

            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());

            if (action.equals(GCMUtil.ACTION_BROADCAST_REGISTRATION_RESULT)) {
                Log.d("action broadcast registration result");
                processRegistrationResult(context, intent);

            } else if (action.equals(GCMUtil.ACTION_BROADCAST_MESSAGE)) {
                Log.d("action broadcast message");
                processMessageResult(context, intent);
            }

            // Releasing wake lock
            WakeLocker.release();
        }

    };


    public void complain(String message) {
        Log.e("Error: " + message);
        alert("Error: " + message);
    }

    public void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d("Showing alert dialog: " + message);
        bld.create().show();
    }


    // Listener that's called when we finish querying the items and
    // subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {
            Log.d("Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null)
                return;

            // Is it a failure?
            if (result.isFailure()) {
                complain("Failed to query inventory: " + result);
                return;
            }

            Log.d("Query inventory was successful.");

			/*
             * TODO Check for items we own. Notice that for each purchase, we check
			 * the developer payload to see if it's correct! See
			 * verifyDeveloperPayload().
			 */

            /* TODO consuming purchase */
            Purchase purchase = inventory.getPurchase(GooglePlayConfig.SKU);
            if (purchase != null && verifyDeveloperPayload(purchase)) {
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                return;
            }

            Log.d("Initial inventory query finished; enabling main UI.");
        }
    };

    /**
     * Verifies the developer payload of a purchase.
     */
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

		/*
         * TODO: verify that the developer payload of the purchase is correct.
		 * It will be the same one that you sent when initiating the purchase.
		 *
		 * WARNING: Locally generating a random string when starting a purchase
		 * and verifying it here might seem like a good approach, but this will
		 * fail in the case where the user purchases an item on one device and
		 * then uses your app on a different device, because on the other device
		 * you will not have access to the random string you originally
		 * generated.
		 *
		 * So a good developer payload has these characteristics:
		 *
		 * 1. If two different users purchase an item, the payload is different
		 * between them, so that one user's purchase can't be replayed to
		 * another user.
		 *
		 * 2. The payload must be such that you can verify it even when the app
		 * wasn't the one who initiated the purchase flow (so that items
		 * purchased by the user on one device work on other devices owned by
		 * the user).
		 *
		 * Using your own server to store and verify developer payloads across
		 * app installations is recommended.
		 */

        return true;
    }


    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d("Purchase finished: " + result + ", purchase: "
                    + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null)
                return;

            if (result.isFailure()) {
                dealWithPurchaseFailed(result);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                return;
            }

            Log.d("Purchase successful.");
            if (DeveloperConfig.DEVELOPER_MODE) {
                if (GooglePlayConfig.SKU.equals(purchase.getSku())) {
                    dealWithPurchaseSuccess(result, purchase);
                }
            } else {
                if (mProductPurchaseId.equals(purchase.getSku())) {
                    dealWithPurchaseSuccess(result, purchase);
                }
            }

        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d("Consumption finished. Purchase: " + purchase
                    + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null)
                return;

            // We know this is the "gas" sku because it's the only one we
            // consume,
            // so we don't check which sku was consumed. If you have more than
            // one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in
                // our
                // game world's logic, which in our case means filling the gas
                // tank a bit
                Log.d("Consumption successful. Provisioning.");
                // TODO
            } else {
                complain("Error while consuming: " + result);
            }
            Log.d("End consumption flow.");
        }
    };


    /**
     * Purchased item success
     *
     * @param result IabResult
     * @param info   Purchase
     */
    public void dealWithPurchaseSuccess(IabResult result, Purchase info) {
        Log.d("Item purchased: " + result);
        Log.d("Purchase info: " + info.toString());
        Log.d("Purchase json: " + info.getOriginalJson());

        // complain("Item purchased: " + result);

        mApplication.setAppBilling(false);

        if (DeveloperConfig.DEVELOPER_MODE) {
            // DEBUG
            // We consume the item straight away so we can test multiple
            // purchases
            mHelper.consumeAsync(info, null);
            // END DEBUG
        }

		/* TODO purchase success, we will save to our server */
    }

    /**
     * purchase item failed
     *
     * @param result
     */
    public void dealWithPurchaseFailed(IabResult result) {
        Log.d("Error purchasing: " + result);
        complain("Error purchasing: " + result);

        mApplication.setAppBilling(false);

        int response = result.getResponse();

        if (response == IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED) {
            /* if we bought this item before, we will save to our server */
            Log.d("Item already owned, save to server");
            // TODO save purchase
        } else {
            /* purchase fail, show message */
            removePreviousDialog("purchase_fail");
            mAlertDialog = AlertDialogFragment
                    .newInstance(
                            getString(R.string.purchase_unsuccessful),
                            getString(R.string.the_purchase_did_not_go_through_successfully),
                            getString(R.string.cancel),
                            getString(R.string.try_again));
            /*
             * The other FragmentActivity's action is performing a task which
			 * requires the Fragment to save its state via a call to
			 * onSaveInstanceState in preparation for a new instance being
			 * reconstructed. I've seen this for example when I was firing off
			 * an activity from a fragment which filled the entire screen as
			 * this resulted in the view being detached from the fragment, state
			 * needing to be saved etc.
			 *
			 * You basically cannot call commit between onSaveInstanceState and
			 * the new instance of the fragment being recreated. See commit.
			 *
			 * As for the solution, then either re-think to try and avoid the
			 * commit being called when it is or alternatively call
			 * commitAllowingStateLoss if you think it's OK for the UI to change
			 * unexpectedly on the user.
			 */
            FragmentTransaction transaction = mFragmentManager
                    .beginTransaction();
            transaction.add(mAlertDialog, "purchase_fail");
            transaction.commitAllowingStateLoss();
        }
    }

    public void purchaseItem(String sku) {
        if (DeveloperConfig.DEVELOPER_MODE)
            sku = GooglePlayConfig.PURCHASE_SUCCESS;

        /*
         * TODO: for security, generate your payload here for verification. See
		 * the comments on verifyDeveloperPayload() for more info. Since this is
		 * a SAMPLE, we just use an empty string, but on a production app you
		 * should carefully generate this.
		 */
        Log.d("Launching purchase flow.");
        String payload = "";
        mHelper.launchPurchaseFlow(this, sku, RC_REQUEST,
                mPurchaseFinishedListener, payload);
    }

    public void purchaseSubscription() {
        /*
		 * TODO: for security, generate your payload here for verification. See
		 * the comments on verifyDeveloperPayload() for more info. Since this is
		 * a SAMPLE, we just use an empty string, but on a production app you
		 * should carefully generate this.
		 */
        String payload = "";

        Log.d("Launching purchase flow for subscription.");
        mHelper.launchPurchaseFlow(this, GooglePlayConfig.SKU,
                IabHelper.ITEM_TYPE_SUBS, RC_REQUEST,
                mPurchaseFinishedListener, payload);
    }


    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
        }
    };

    protected void initActionBar(){
        mLeftImage = (ImageView)findViewById(R.id.left_image);
        mRightImage = (ImageView)findViewById(R.id.right_image);
        mLeftText = (CustomTextView)findViewById(R.id.left_text);
        mRightText = (CustomTextView)findViewById(R.id.right_text);
        mTitleText = (CustomTextView)findViewById(R.id.title_text);
    }

    @Override
    public void onClick(View v) {
    }
}
