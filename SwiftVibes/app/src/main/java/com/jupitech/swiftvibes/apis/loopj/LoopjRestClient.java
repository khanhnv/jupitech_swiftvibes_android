package com.jupitech.swiftvibes.apis.loopj;

import android.content.Context;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.jupitech.swiftvibes.AppApplication;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.utils.hardware.NetworkUtil;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Set;

public class LoopjRestClient {

	public static AsyncHttpClient mClient = new AsyncHttpClient();
	static {
		mClient.setTimeout(30000);
		setHandleError(new HandleAuthTokenExpired());
	}

	private static IStrageryHandleError mHandleError;

	public static void setHandleError(IStrageryHandleError handleError) {
		mHandleError = handleError;
	}

	/**
	 * get request with parameters
	 * 
	 * @param url
	 * @param params
	 * @param responseHandler
	 */
	public static void get(String url, RequestParams params,
			TextHttpResponseHandler responseHandler) {
		boolean isConnect = isNetworkConnected();
		if (!isConnect) {
			return;
		}
		mClient.get(url, params, makeWrapperHandler(url, responseHandler));
	}

	/**
	 * get request with parameters and headers
	 * 
	 * @param url
	 * @param params
	 * @param headers
     * @param responseHandler
     */
	public static void get(String url, RequestParams params,
			HashMap<String, String> headers,
			TextHttpResponseHandler responseHandler) {
		boolean isConnect = isNetworkConnected();
		if (!isConnect) {
			return;
		}
		Set<String> keys = headers.keySet();
		for (String key : keys) {
			mClient.addHeader(key, headers.get(key));
		}
		mClient.get(url, params, makeWrapperHandler(url, responseHandler));
	}

	/**
	 * post request
	 * 
	 * @param url
	 * @param responseHandler
	 */
	public static void post(String url, TextHttpResponseHandler responseHandler) {
		boolean isConnect = isNetworkConnected();
		if (!isConnect) {
			return;
		}
		mClient.post(url, makeWrapperHandler(url, responseHandler));
	}

	/**
	 * Post with parameters
	 * 
	 * @param url
	 * @param params
	 * @param responseHandler
	 */
	public static void post(String url, RequestParams params,
			TextHttpResponseHandler responseHandler) {
		boolean isConnect = isNetworkConnected();
		if (!isConnect) {
			return;
		}
		mClient.post(url, params, makeWrapperHandler(url, responseHandler));
	}

	/**
	 * post with parameters and headers
	 * 
	 * @param url
	 * @param params
	 * @param headers
	 * @param responseHandler
	 */
	public static void post(String url, RequestParams params,
			HashMap<String, String> headers,
			TextHttpResponseHandler responseHandler) {
		boolean isConnect = isNetworkConnected();
		if (!isConnect) {
			return;
		}
		Set<String> keys = headers.keySet();
		for (String key : keys) {
			mClient.addHeader(key, headers.get(key));
		}
		mClient.post(url, params, makeWrapperHandler(url, responseHandler));
	}

	/**
	 * Post with body data
	 * 
	 * @param context
	 * @param url
	 * @param data
	 * @param responseHandler
	 */
	public static void post(Context context, String url, String data,
			TextHttpResponseHandler responseHandler) {
		boolean isConnect = isNetworkConnected();
		if (!isConnect) {
			return;
		}
		HttpEntity entity;
		try {
			entity = new StringEntity(data.toString(), HTTP.UTF_8);
			mClient.post(context, url, entity, "application/json",
					makeWrapperHandler(url, responseHandler));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * put request with parameters
	 * 
	 * @param url
	 * @param params
	 * @param responseHandler
	 */
	public static void put(String url, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		mClient.put(url, params, responseHandler);
	}

	/**
	 * put request with JSON body data
	 * 
	 * @param context
	 * @param url
	 * @param data
	 * @param responseHandler
	 */
	public static void put(Context context, String url, String data,
			AsyncHttpResponseHandler responseHandler) {
		HttpEntity entity;
		try {
			entity = new StringEntity(data.toString(), HTTP.UTF_8);
			mClient.put(context, url, entity, "application/json",
					responseHandler);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param url
	 * @param params
	 * @param responseHandler
	 */
	public static void delete(String url, RequestParams params,
			AsyncHttpResponseHandler responseHandler) {
		mClient.delete(url, responseHandler);
	}

	private static boolean isNetworkConnected() {
		if (!NetworkUtil.getInstance(AppApplication.get()).isConnect()) {
			Toast.makeText(AppApplication.get(), "No Internet Connection",
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	/**
	 * Make wrapper handler for log request/response
	 * 
	 * @param originHandler
	 * @return
	 */
	private static TextHttpResponseHandler makeWrapperHandler(final String url,
			final TextHttpResponseHandler originHandler) {
		TextHttpResponseHandler wrapperHandler = new TextHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers,
					String responseString) {
				Log.d("response success for request " + url + ": "
						+ responseString);
				if (originHandler != null) {
					originHandler
							.onSuccess(statusCode, headers, responseString);
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					String responseString, Throwable throwable) {
				Log.d("response failure for request " + url + ": "
                        + responseString);
                if (mHandleError != null) {
                    mHandleError.handleError(statusCode, headers,
                            responseString, throwable);
                }
                if (originHandler != null) {
                    originHandler.onFailure(statusCode, headers,
							responseString, throwable);
				}
			}

			@Override
			public void onStart() {
				Log.d("start request " + url);
				if (originHandler != null) {
					originHandler.onStart();
				}
			}

			@Override
			public void onFinish() {
				Log.d("finish request " + url);
				if (originHandler != null) {
					originHandler.onFinish();
				}
			}
		};
		return wrapperHandler;
	}
}