package com.jupitech.swiftvibes.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.configs.ApiConfig;
import com.jupitech.swiftvibes.listeners.OnItemLongClick;
import com.jupitech.swiftvibes.models.PostModel;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.widgets.MessageRowItem;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by brucenguyen on 10/16/14.
 */
public class MessageAdapter extends ArrayAdapter<PostModel> {

    UserModel mUserModel;

    OnItemLongClick mListener;

    public MessageAdapter(Context context, int resource, List<PostModel> objects, OnItemLongClick onItemLongClick) {
        super(context, resource, objects);
        this.mListener = onItemLongClick;
        mUserModel = UserModel.getUser();
    }

    public MessageAdapter(Context context, int resource, OnItemLongClick onItemLongClick) {
        super(context, resource);
        this.mListener = onItemLongClick;
        mUserModel = UserModel.getUser();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final PostModel post = getItem(position);
        final int p = position;

        ViewHolder holder;

        if(convertView == null){

            convertView = new MessageRowItem(getContext());


            holder = new ViewHolder();
            holder.mMessageText = ((MessageRowItem)convertView).mMessageText;
            holder.mMeText = ((MessageRowItem)convertView).mMeText;
            holder.mNameText = ((MessageRowItem)convertView).mNameText;
            holder.mStatusImage = ((MessageRowItem)convertView).mStatusImage;
            holder.mThumbImage = ((MessageRowItem)convertView).mThumbImage;
            holder.mContainer = ((MessageRowItem)convertView).mContainer;

            convertView.setTag(holder);
        }else{

            holder = (ViewHolder)convertView.getTag();
        }

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.onItemLongClick(p, post);
                return true;
            }
        });

        String imageUrl = String.format(ApiConfig.GET_THUMNAIL_IMAGE, post.getCode());
        ImageLoader.getInstance().displayImage(imageUrl, holder.mThumbImage);

        holder.mMessageText.setText(post.getMessage());

        //
        if(!post.isMe()){
            holder.mMeText.setVisibility(View.GONE);
            holder.mNameText.setText(post.getFromName());
        }else{
            holder.mMeText.setVisibility(View.VISIBLE);
            holder.mNameText.setText(post.getToName());
        }

        if(post.getByMe() != null && post.getByMe().equalsIgnoreCase("1")){
            if(post.getStatus().equalsIgnoreCase("1")) {
                holder.mContainer.setBackgroundColor(Color.WHITE);
                holder.mStatusImage.setImageResource(R.drawable.icon_seensent);
            }else if(post.getStatus().equalsIgnoreCase("0")){
                holder.mContainer.setBackgroundColor(Color.WHITE);
                holder.mStatusImage.setImageResource(0);
            }else if(post.getStatus().equalsIgnoreCase("2")){
                holder.mContainer.setBackgroundColor(Color.WHITE);
                holder.mStatusImage.setImageResource(R.drawable.icon_seenheard);
            }

        }else if(post.getByMe() != null && post.getByMe().equalsIgnoreCase("0")){

            if(post.getStatus().equalsIgnoreCase("1")) {
                holder.mContainer.setBackgroundColor((Color.parseColor("#def5f6")));
                holder.mStatusImage.setImageResource(R.drawable.icon_loa);

            }else if(post.getStatus().equalsIgnoreCase("2")){
                holder.mContainer.setBackgroundColor(Color.WHITE);
                holder.mStatusImage.setImageResource(0);
            }
        }else{
            if(!post.isMe()){
                if(post.getStatus().equalsIgnoreCase("0")){
                    holder.mContainer.setBackgroundColor((Color.parseColor("#beeced")));
                }else{
                    holder.mContainer.setBackgroundColor(Color.WHITE);
                }
            }else{
                holder.mContainer.setBackgroundColor(Color.WHITE);
            }
            holder.mStatusImage.setImageResource(0);
        }


        return convertView;
    }


    static class ViewHolder{
        public ImageView mThumbImage;
        public ImageView mStatusImage;
        public TextView mMeText;
        public TextView mNameText;
        public TextView mMessageText;
        public LinearLayout mContainer;
    }
}
