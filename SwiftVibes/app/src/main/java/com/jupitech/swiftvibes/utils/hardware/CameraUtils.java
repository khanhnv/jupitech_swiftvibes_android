package com.jupitech.swiftvibes.utils.hardware;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.util.Log;
import android.view.Surface;

import java.util.List;

public class CameraUtils {

	public static boolean isFrontCamera() {
		return Camera.getNumberOfCameras() > 1;
	}

	public static int getCameraDisplayOrientation(Activity activity,
			int cameraId, Camera camera) {
		Camera.CameraInfo info = new Camera.CameraInfo();
		Camera.getCameraInfo(cameraId, info);
		int rotation = activity.getWindowManager().getDefaultDisplay()
				.getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;
		case Surface.ROTATION_90:
			degrees = 90;
			break;
		case Surface.ROTATION_180:
			degrees = 180;
			break;
		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		Log.d("CAMERA_PREVIEW", "result = " + result);
		return result;
	}

	public static void setCameraDisplayOrientation(Activity activity,
			int cameraId, Camera camera) {
		Camera.CameraInfo info = new Camera.CameraInfo();
		Camera.getCameraInfo(cameraId, info);
		int rotation = activity.getWindowManager().getDefaultDisplay()
				.getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;
		case Surface.ROTATION_90:
			degrees = 90;
			break;
		case Surface.ROTATION_180:
			degrees = 180;
			break;
		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		Log.d("CAMERA_PREVIEW", "result = " + result);
		camera.setDisplayOrientation(result);
	}

	public static Camera getCameraInstance(Activity activity, int cameraId) {
		Camera c = null;
		try {
			c = Camera.open(cameraId); // attempt to get a Camera instance
			int degrees = getCameraDisplayOrientation(activity, cameraId, c);
			Camera.Parameters p = c.getParameters();
			p.setRotation(degrees);
			c.setParameters(p);
			c.setDisplayOrientation(degrees);
		} catch (Exception e) {
			// Camera is not available (in use or does not exist)
			e.printStackTrace();
		}
		return c; // returns null if camera is unavailable
	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open(); // attempt to get a Camera instance
			Camera.Parameters p = c.getParameters();
			p.set("orientation", "landscape");
			p.setRotation(90);

			List<Camera.Size> sizeList = p.getSupportedPreviewSizes();
			for (Camera.Size size : sizeList) {
				Log.e("CameraSize", size.width + "-----" + size.height);
			}
			c.setParameters(p);
		} catch (Exception e) {
			// Camera is not available (in use or does not exist)
			e.printStackTrace();
		}
		return c; // returns null if camera is unavailable
	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance(Camera currCamera) {
		try {
			if (Camera.open() == null) {
				currCamera.reconnect();
			} else {
				currCamera = Camera.open();
			}

			Camera.Parameters p = currCamera.getParameters();
			p.set("orientation", "landscape");
			p.setRotation(90);

			// Camera.Size bestSize = null;
			// List<Camera.Size> sizeList = p.getSupportedPreviewSizes();
			// bestSize = sizeList.get(0);
			//
			// p.setPictureSize(bestSize.width, bestSize.height);
			// p.setPreviewSize(bestSize.width, bestSize.height);
			currCamera.setParameters(p);
		} catch (Exception e) {
			// Camera is not available (in use or does not exist)
			e.printStackTrace();
		}
		return currCamera; // returns null if camera is unavailable
	}

	/** Check if this device has a camera */
	public static boolean checkCameraHardware(Context context) {
		return context.getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_CAMERA);
	}
}
