package com.jupitech.swiftvibes.providers;

import android.content.Context;
import android.telephony.TelephonyManager;

/**
 * Class contains some common method of phone number.
 * 
 * @author dizaii
 * 
 */
public class MyPhoneNumber {
	private TelephonyManager telephonyManager;

	public MyPhoneNumber(Context context) {
		this.telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
	}

	/**
	 * Method used to get country code in SIM
	 * 
	 * @return : country code
	 */
	public String getCountryCode() {
		String countryIso = telephonyManager.getSimCountryIso();
		return Country.getByISO(countryIso).number;
	}

	/**
	 * Method use to get MCC of sim
	 * 
	 * @return
	 */
	public String getMCC() {
		return telephonyManager.getSimOperator();
	}

	/**
	 * Method use to normalize phone number If the number is 9 digits or smaller, add the country code to it
	 * 
	 * @param phoneNumber
	 *            : phone number without country code, '00', '+'
	 * @return : phone number with country code
	 */
	public String normalizePhoneNumber(String phoneNumber) {
		if (phoneNumber.length() <= 9) {
			phoneNumber = getCountryCode() + phoneNumber;
		}
		return phoneNumber.trim();
	}
}
