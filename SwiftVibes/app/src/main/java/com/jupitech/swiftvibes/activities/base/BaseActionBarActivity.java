package com.jupitech.swiftvibes.activities.base;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.android.gcm.GCMRegistrar;
import com.jupitech.swiftvibes.AppApplication;
import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.NavigationActivity;
import com.jupitech.swiftvibes.dialogs.AlertDialogFragment;
import com.jupitech.swiftvibes.dialogs.ProgressDialogFragment;
import com.jupitech.swiftvibes.gcm.CommonUtilities;
import com.jupitech.swiftvibes.gcm.GCMUtil;
import com.jupitech.swiftvibes.listeners.AlertListener;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.socials.facebook.FacebookUtil;
import com.jupitech.swiftvibes.utils.Log;
import com.jupitech.swiftvibes.utils.hardware.WakeLocker;
import com.jupitech.swiftvibes.utils.preference.PreferenceUtil;
import com.jupitech.swiftvibes.utils.text.StringUtil;
import com.jupitech.swiftvibes.widgets.CustomTextView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Timer;
import java.util.TimerTask;

public abstract class BaseActionBarActivity extends FragmentActivity implements
        OnClickListener {

    protected final long TIMEOUT = 30000; // 30 seconds

    protected static BaseActionBarActivity mSelf;

    // Utils
    protected PreferenceUtil mPreferenceManager;
    protected Handler mHandler = new Handler();

    // contain user info, used to check login
    protected UserModel mUserModel;

    // contain all of global variable
    protected AppApplication mApplication;

    // fragment manager
    protected FragmentManager mFragmentManager;

    // Image Loader
    protected ImageLoader mImageLoader = ImageLoader.getInstance();
    protected DisplayImageOptions mOptions;

    // Custom Dialog
    protected AlertDialogFragment mAlertDialog;
    protected ProgressDialogFragment mProgressDialog;

    // navigation bar
    protected CustomTextView mTitleText;
    protected CustomTextView mLeftText;
    protected CustomTextView mRightText;
    protected ImageView mLeftImage;
    protected ImageView mRightImage;
    protected View mLoadingLayout;

    // GCM
    protected String mRegisterId;
    protected ProgressDialog mSpinner = null;
    protected Timer mGcmTimeoutTimer = null;

    protected FacebookUtil mFacebookUtil;


    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Switch to fullscreen view, getting rid of the status bar as well.
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // hide the keyboard everytime the activty starts
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mSelf = BaseActionBarActivity.this;

        mPreferenceManager = PreferenceUtil
                .getInstance(getApplicationContext());

        mUserModel = mPreferenceManager.getUserInfo();

        mApplication = (AppApplication) getApplication();

        mFragmentManager = getSupportFragmentManager();

        /** register broadcast receiver */
        LocalBroadcastManager.getInstance(mSelf).registerReceiver(mGCMReceiver,
                new IntentFilter(GCMUtil.ACTION_BROADCAST_REGISTRATION_RESULT));
        LocalBroadcastManager.getInstance(mSelf).registerReceiver(mGCMReceiver,
                new IntentFilter(GCMUtil.ACTION_BROADCAST_MESSAGE));

        mOptions = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.noimage)
                .showImageOnFail(R.drawable.noimage).cacheInMemory(true)
                .cacheOnDisk(true).bitmapConfig(Bitmap.Config.RGB_565).build();

        mFacebookUtil = FacebookUtil.newInstance(this);
        mFacebookUtil.onCreate(bundle);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mFacebookUtil.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mApplication.isBackPress()) {
            mApplication.setBackPress(false);
        }

        mFacebookUtil.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mApplication.setAppRunning(true);
    }

    @Override
    protected void onPause() {
        super.onPause();

        mApplication.setAppRunning(false);
    }

    @Override
    protected void onStop() {
        super.onStop();

        mApplication.setAppRunning(false);

        mFacebookUtil.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        mFacebookUtil.onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(mSelf).unregisterReceiver(
                mGCMReceiver);
    }

    @Override
    public void onBackPressed() {

        mApplication.setBackPress(true);

        super.onBackPressed();

        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            this.finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    /**
     * Init all model when onCreate activity here
     */
    protected abstract void initModels();

    /**
     * Init all views when onCreate activity here
     */
    protected abstract void initViews();

    /**
     * Remove previous show dialog fragment by tag
     *
     * @param tag tag of dialog fragment
     */
    protected void removePreviousDialog(String tag) {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction. We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        Fragment prev = mFragmentManager.findFragmentByTag(tag);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.commit();
    }

    /**
     * set visible button
     *
     * @param showOrHide
     */
    private void setVisibleView(View view, boolean showOrHide) {
        view.setVisibility(showOrHide ? View.VISIBLE : View.INVISIBLE);
    }

    /**
     * set visible left button
     *
     * @param showOrHide : true if show, else hide is false
     */
    public void setVisibleLeftTextView(boolean showOrHide) {
        setVisibleView(mLeftText, showOrHide);
    }

    /**
     * set visible right button
     *
     * @param showOrHide : true if show, else hide is false
     */
    public void setVisibleRightTextView(boolean showOrHide) {
        setVisibleView(mRightText, showOrHide);
    }

    /**
     * @param showOrHide
     */
    public void setVisibleLeftImage(boolean showOrHide) {
        setVisibleView(mLeftImage, showOrHide);
    }

    /**
     * @param showOrHide
     */
    public void setVisibleRightImage(boolean showOrHide) {
        setVisibleView(mRightImage, showOrHide);
    }

    /**
     * @param showOrHide
     */
    public void setVisibleTitleText(boolean showOrHide) {
        setVisibleView(mTitleText, showOrHide);
    }

    /**
     * @param textView
     * @param text
     * @param drawable
     */
    private void setBackgroundTextView(CustomTextView textView, String text,
                                       int drawable) {
        textView.setBackgroundResource(drawable);
        if (text.length() > 0) {
            textView.setText(text);
        }
    }

    /**
     * @param imageView
     * @param drawable
     */
    private void setBackgroundImage(ImageView imageView, int drawable) {
        imageView.setImageResource(drawable);
    }

    /**
     * @param text
     * @param drawable
     */
    public void setBackgroundLeftButton(String text, int drawable) {
        setBackgroundTextView(mLeftText, text, drawable);
    }

    /**
     * @param text
     * @param drawable
     */
    public void setBackgroundRightButton(String text, int drawable) {
        setBackgroundTextView(mRightText, text, drawable);
    }


    /**
     * @param drawable
     */
    public void setBackgroundLeftImage(int drawable) {
        setBackgroundImage(mLeftImage, drawable);
    }

    /**
     * @param drawable
     */
    public void setBackgroundRightImage(int drawable) {
        setBackgroundImage(mRightImage, drawable);
    }

    /**
     * @param text
     */
    public void setTitleText(String text) {
        if (!StringUtil.isEmpty(text)) {
            mTitleText.setText(text);
        }
    }

    /**
     * clear all fragment in backstack
     */
    public void clearBackStackFragment() {
        int length = mFragmentManager.getBackStackEntryCount();
        for (int i = 0; i < length; i++) {
            // remove each fragment in back statck
            mFragmentManager.popBackStack();
        }
    }

    /**
     * register GCM. This method is called onCreate activity
     */
    public void registerGCM() {

        try {
            // Make sure the device has the proper dependencies.
            GCMRegistrar.checkDevice(this);
            // Make sure the manifest was properly set - comment out this line
            // while developing the app, then uncomment it when it's ready.
            GCMRegistrar.checkManifest(this);
        } catch (Exception e) {

            // if check device fail, we show pop-up for user
            e.printStackTrace();
            showAlertDialog("This device does not support GCM.");
            return;
        }

        final String regId = GCMRegistrar.getRegistrationId(this);
        mPreferenceManager.setRegisterId(regId);
        Log.d("regId = " + regId);
        if (regId.length() == 0) {
            // Automatically registers application on startup.

            mSpinner = new ProgressDialog(mSelf);
            mSpinner.setMessage("Registering push notification service...");
            mSpinner.setCancelable(false);
            mSpinner.show();

            GCMRegistrar.register(this, CommonUtilities.SENDER_ID);

            // show alert register fail GCM when time out (30 seconds)
            mGcmTimeoutTimer = new Timer();
            mGcmTimeoutTimer.schedule(new StopGcmRegistrationTimerTask(),
                    TIMEOUT);

        } else {
            // Device is already registered on GCM, check server.
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                // register in our server successfully, skips registration.
                Log.d(getString(R.string.already_registered));
            } else {
                // do register in our server
                // registerDeviceToServer(mTeamModel.mAccessCode, regId);
                Log.d("register: " + regId);
            }
        }
    }

    /**
     * unregister GCM. This method is called on destroy activity
     */
    public void unregisterGCM() {
        try {
            GCMRegistrar.setRegisteredOnServer(mSelf, false);
            GCMRegistrar.onDestroy(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param context
     * @param intent
     */
    private void processRegistrationResult(Context context, Intent intent) {
        if (mGcmTimeoutTimer != null) {
            mGcmTimeoutTimer.cancel();
            mGcmTimeoutTimer = null;
        }

        if (mSpinner != null && mSpinner.isShowing()) {
            try {
                mSpinner.dismiss();
                mSpinner = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Bundle bundle = intent.getExtras();
        boolean isRegistrationSuccess = bundle
                .getBoolean(GCMUtil.EXTRA_REGISTRATION_SUCCESS);
        String message = bundle.getString(GCMUtil.EXTRA_REGISTRATION_CONTENT);

        if (isRegistrationSuccess) {
            /* get registration id */

            mPreferenceManager.setRegisterId(message);

            // registerDeviceToServer(mTeamModel.mAccessCode, message);
        } else {
            /* register failed after MAX_ATTEMPTS tries. Show errors message */
            if (message.equalsIgnoreCase(GCMUtil.ERR_INVALID_SENDER)) {
                showAlertDialog(
                        context.getString(R.string.MSG_GCM_INVALID_SENDER));
            } else if (message.equalsIgnoreCase(GCMUtil.ERR_ACCOUNT_MISSING)) {
                showAlertDialog(
                        context.getString(R.string.MSG_GCM_ACCOUNT_MISSING));
            } else if (message
                    .equalsIgnoreCase(GCMUtil.ERR_AUTHENTICATION_FAILED)) {
                showAlertDialog(

                        context.getString(R.string.MSG_GCM_AUTHENTICATION_FAILED));
            } else if (message.equalsIgnoreCase(GCMUtil.ERR_INVALID_PARAMETERS)) {
                showAlertDialog(
                        context.getString(R.string.MSG_GCM_INVALID_PARAMETERS));
            } else if (message
                    .equalsIgnoreCase(GCMUtil.ERR_PHONE_REGISTRATION_ERROR)) {
                showAlertDialog(

                        context.getString(R.string.MSG_GCM_PHONE_REGISTRATION_ERROR));
            } else if (message
                    .equalsIgnoreCase(GCMUtil.ERR_SERVICE_NOT_AVAILABLE)) {
                showAlertDialog(

                        context.getString(R.string.MSG_GCM_SERVICE_NOT_AVAILABLE));
            } else {
                showAlertDialog(message);
            }
        }
    }

    /**
     * @param context
     * @param intent
     */
    private void processMessageResult(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle == null)
            return;

        Log.d("DataNofitication: " + bundle.toString());
        String unRead = bundle.getString("number_unread_message");
        mPreferenceManager.setUnReadMessage(unRead);
        Log.d("UnRead: " + unRead);

        String message = bundle.getString("message");
        generateNotification(mSelf, message);
    }

    @SuppressWarnings("deprecation")
    public void generateNotification(Context context, String message) {
        Log.d("show notification");
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        String title = context.getString(R.string.app_name);
        Intent notificationIntent = new Intent(context, NavigationActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, 0,
                notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);
    }

    /**
     * Switch content tab
     *
     * @param fragment
     */
    public void switchContent(int contentId, Fragment fragment) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(contentId, fragment);
        transaction.commit();
    }

    /**
     * Switch content tab, fragment will add to back stack
     *
     * @param fragment
     */
    public void switchContent(int contentId, Fragment fragment, boolean addToBackstack) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(contentId, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    /**
     * show alert message
     *
     * @param message
     */
    public void showAlertDialog(String message) {
        // clear all state previous
        removePreviousDialog("alert_dialog");
        mAlertDialog = null;
        // create dialog
        mAlertDialog = AlertDialogFragment.newInstance(
                getString(R.string.app_name), message,
                getString(android.R.string.ok), getString(android.R.string.ok));
        mAlertDialog.showOnlyOneButton(true);
        // show dialog
        mAlertDialog.show(mFragmentManager, "alert_dialog");
    }

    /**
     * Show confirm dialog. The dialog contains left + right button
     *
     * @param title
     * @param message
     * @param leftText
     * @param rightText
     * @param listener
     */
    public void showConfirmDialog(String title, String message,
                                  String leftText, String rightText, AlertListener listener) {
        // clear all state previous
        removePreviousDialog("alert_dialog");
        mAlertDialog = null;
        // create dialog
        mAlertDialog = AlertDialogFragment.newInstance(title, message,
                leftText, rightText, listener);
        // show dialog
        mAlertDialog.show(mFragmentManager, "alert_dialog");
    }

    /**
     * Show loading dialog.
     *
     * @param title
     * @param message
     */
    public void showLoadingDialog(String title, String message) {
        removePreviousDialog("progress_dialog");
        mProgressDialog = null;
        mProgressDialog = ProgressDialogFragment.newInstance(title, message);
        mProgressDialog.show(mFragmentManager, "progress_dialog");
    }

    /**
     * timer task to hide wait dialog when gcm registration timeout
     */
    class StopGcmRegistrationTimerTask extends TimerTask {
        @Override
        public void run() {
            Looper.prepare();
            if (mSpinner != null && mSpinner.isShowing()) {
                mSpinner.dismiss();
                mSpinner = null;
            }

            mPreferenceManager.setRegisterId("");

            showAlertDialog(
                    getString(R.string.not_able_register_gcm));
            Looper.loop();
            Looper.myLooper().quit();
        }
    }

    /**
     * Broadcast Receiver: listen message from GCM intent service to got
     * registration Id/error, then call API to notify server to update device
     * token
     */
    public BroadcastReceiver mGCMReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            // get action
            String action = intent.getAction();
            Log.d("action: " + action);

            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());

            if (action.equals(GCMUtil.ACTION_BROADCAST_REGISTRATION_RESULT)) {
                Log.d("action broadcast registration result");
                processRegistrationResult(context, intent);

            } else if (action.equals(GCMUtil.ACTION_BROADCAST_MESSAGE)) {
                Log.d("action broadcast message");
                processMessageResult(context, intent);
            }

            // Releasing wake lock
            WakeLocker.release();
        }

    };

    @Override
    public void onClick(View v) {
    }

    protected void initActionBar(){
        mLeftImage = (ImageView)findViewById(R.id.left_image);
        mRightImage = (ImageView)findViewById(R.id.right_image);
        mLeftText = (CustomTextView)findViewById(R.id.left_text);
        mRightText = (CustomTextView)findViewById(R.id.right_text);
        mTitleText = (CustomTextView)findViewById(R.id.title_text);
    }
}
