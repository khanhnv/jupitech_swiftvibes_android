package com.jupitech.swiftvibes.databases.sqlite;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.jupitech.swiftvibes.models.UserModel;

import java.sql.SQLException;

public class Connection extends DatabaseHelper {

    private static Connection instance;

    private Dao<UserModel, Integer> userDao = null;

    public Connection(Context context) {
        super(context);
    }

    public static Connection getInstance(Context context) {
        if (instance == null) {
            instance = new Connection(context);
        }
        return instance;
    }

    @Override
    public void close() {
        super.close();
        userDao = null;
    }

    public Dao<UserModel, Integer> getUserDao() throws SQLException {
        if (userDao == null) {
            userDao = getDao(UserModel.class);
        }
        return userDao;
    }
}
