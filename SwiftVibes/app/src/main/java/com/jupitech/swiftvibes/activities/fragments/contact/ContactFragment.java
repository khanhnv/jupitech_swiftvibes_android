package com.jupitech.swiftvibes.activities.fragments.contact;

import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jupitech.swiftvibes.AppApplication;
import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.activities.fragments.base.BaseFragment;
import com.jupitech.swiftvibes.adapters.ContactAdapter;
import com.jupitech.swiftvibes.models.ContactModel;
import com.jupitech.swiftvibes.providers.contact.ContactUtil;
import com.jupitech.swiftvibes.utils.bitmap.resource.AsyncTask;

import java.util.ArrayList;
import java.util.List;

public class ContactFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemClickListener {

    ListView mListView;
    ContactAdapter mAdapter;
    List<ContactModel> mContactModels = new ArrayList<ContactModel>();

    public ContactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        initModels();
    }

    @Override
    protected void initModels() {
    }

    @Override
    protected void initViews(View view) {
        mListView = (ListView)view.findViewById(R.id.list_view);

        View empty = view.findViewById(android.R.id.empty);
        mListView.setEmptyView(empty);

        mAdapter = new ContactAdapter(getActivity(), 0);
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switchFragment(ContactDetailFragment.newInstance(mAdapter.getItem(position)), true);
            }
        });

        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                showProgress();
                ArrayList<ContactModel> contactModels = ContactUtil.getListContact(AppApplication.get());
                if(contactModels != null){
                    mContactModels.addAll(contactModels);
                }
                return null;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                mAdapter.clear();
                mAdapter.addAll(mContactModels);
                mAdapter.notifyDataSetChanged();
                hideProgress();
            }
        }.execute();



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_contact, container, false);
        initViews(view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
