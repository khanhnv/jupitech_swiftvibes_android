package com.jupitech.swiftvibes.widgets;

import android.content.Context;
import android.widget.TextView;

import com.jupitech.swiftvibes.R;

/**
 * Created by brucenguyen on 10/14/14.
 */
public class FriendsRoxItem extends BaseLinearLayout{

    public TextView mNameText;

    public FriendsRoxItem(Context context) {
        super(context);
        initLayout(context, R.layout.item_friends);

        mNameText = (TextView)findViewById(R.id.name_text);
    }
}
