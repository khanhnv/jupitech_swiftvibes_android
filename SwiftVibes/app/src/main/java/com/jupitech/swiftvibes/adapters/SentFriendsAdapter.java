package com.jupitech.swiftvibes.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jupitech.swiftvibes.R;
import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.widgets.SentFriendsRoxItem;

import java.util.List;

/**
 * Created by brucenguyen on 10/14/14.
 */
public class SentFriendsAdapter extends ArrayAdapter<UserModel> {

    public SentFriendsAdapter(Context context, int resource, List<UserModel> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        UserModel userModel = getItem(position);

        if(convertView == null){
            convertView = new SentFriendsRoxItem(getContext());

            holder = new ViewHolder();
            holder.mNameText = ((SentFriendsRoxItem)convertView).mNameText;
            holder.mCheckImage = ((SentFriendsRoxItem)convertView).mCheckImage;

            convertView.setTag(holder);
        }else{

            holder = (ViewHolder)convertView.getTag();
        }

        holder.mNameText.setText(userModel.getName());

        if(userModel.isChecked()){
            holder.mCheckImage.setImageResource(R.drawable.icon_selected);
        }else{
            holder.mCheckImage.setImageResource(R.drawable.icon_select);
        }

        return convertView;
    }


    static class ViewHolder{
        TextView mNameText;
        ImageView mCheckImage;
    }
}
