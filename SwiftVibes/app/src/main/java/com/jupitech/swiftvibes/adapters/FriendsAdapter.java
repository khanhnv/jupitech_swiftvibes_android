package com.jupitech.swiftvibes.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.jupitech.swiftvibes.models.UserModel;
import com.jupitech.swiftvibes.widgets.FriendsRoxItem;

import java.util.List;

/**
 * Created by brucenguyen on 10/14/14.
 */
public class FriendsAdapter extends ArrayAdapter<UserModel> {

    public FriendsAdapter(Context context, int resource, List<UserModel> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        UserModel userModel = getItem(position);

        if(convertView == null){
            convertView = new FriendsRoxItem(getContext());

            holder = new ViewHolder();
            holder.mNameText = ((FriendsRoxItem)convertView).mNameText;

            convertView.setTag(holder);
        }else{

            holder = (ViewHolder)convertView.getTag();
        }

        holder.mNameText.setText(userModel.getName());

        return convertView;
    }


    static class ViewHolder{
        TextView mNameText;
    }
}
