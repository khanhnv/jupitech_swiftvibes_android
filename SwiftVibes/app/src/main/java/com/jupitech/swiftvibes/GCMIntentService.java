/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jupitech.swiftvibes;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.jupitech.swiftvibes.activities.MainActivity;
import com.jupitech.swiftvibes.configs.Key;
import com.jupitech.swiftvibes.gcm.CommonUtilities;
import com.jupitech.swiftvibes.gcm.GCMUtil;

import java.util.Timer;
import java.util.TimerTask;


/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

    private static final String TAG = "GCMIntentService";

    static int mAttemps = 0;
    long mBackOff = GCMUtil.BACKOFF_MILLI_SECONDS;
    boolean mRegistered = false;
    Timer mTimer = null;

    public GCMIntentService() {
        super(CommonUtilities.SENDER_ID);
    }

    /**
     * Handle register to GCM server successfully
     *
     * @param context
     * @param registrationId
     */
    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);

        mRegistered = true;
        mAttemps = 0;
        mBackOff = GCMUtil.BACKOFF_MILLI_SECONDS;
        stopTimer();

		/*
		 * broadcast registrationId
		 */
        GCMUtil.broadcastRegistrationResult(context, GCMUtil.TYPE_REGISTER,
                true, registrationId);

    }

    /**
     * Handle unregister to GCM server successfully
     *
     * @param context
     * @param registrationId
     */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");

        mRegistered = false;
        stopTimer();

        if (GCMRegistrar.isRegisteredOnServer(context)) {

            GCMUtil.broadcastUnRegistratoinResult(context);

        } else {
            // This callback results from the call to unregister made on
            // ServerUtilities when the registration to the server failed.
            Log.i(TAG, "Ignoring unregister callback");
        }

    }

    /**
     * Handle message from GCM server
     */
    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");

        Bundle bundle = intent.getExtras();

        if (bundle != null) {

            Log.d(TAG, "Received message: " + bundle.toString());

            GCMUtil.processGcmMessage(context, bundle);
        }
    }

    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        // String message = getString(R.string.gcm_deleted, total);
        // displayMessage(context, message);
        // // notifies user
        // generateNotification(context, message);
    }

    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);

        if (!mRegistered) {

			/* Error when trying to register */
            mAttemps++;

            if (mAttemps >= GCMUtil.MAX_ATTEMPTS) {

				/* reach limitation of retries */
                GCMUtil.broadcastRegistrationResult(context,
                        GCMUtil.TYPE_REGISTER_UNREGISTER_ERROR, false, errorId);

                mAttemps = 0;

                mBackOff = GCMUtil.BACKOFF_MILLI_SECONDS;

                return;
            }

            stopTimer();

            mTimer = new Timer(TAG);
            mTimer.schedule(new MyTimerTask(context), mBackOff);

            /** increase backoff exponentially */
            mBackOff *= 2;
        } else {
			/* Error when trying to unregister */
        }
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);

        if (!mRegistered) {

            mAttemps++;

            if (mAttemps >= GCMUtil.MAX_ATTEMPTS) {
                GCMUtil.broadcastRegistrationResult(context,
                        GCMUtil.TYPE_REGISTER_UNREGISTER_ERROR, false, errorId);
                mAttemps = 0;
                mBackOff = GCMUtil.BACKOFF_MILLI_SECONDS;
                return false;
            }

            stopTimer();

            mTimer = new Timer(TAG);
            mTimer.schedule(new MyTimerTask(context), mBackOff);

            /** increase backoff exponentially */
            mBackOff *= 2;
        } else {
            /** Error when trying to unregister */
            return false;
        }

        return false;
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     *
     * @param context
     * @param type
     * @param title
     * @param message
     */
    @SuppressWarnings("deprecation")
    public static void generateNotification(Context context, int type,
                                            String title, String message) {
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        if (title == null || title.length() == 0) {
            title = context.getString(R.string.app_name);
        }
        Intent notificationIntent = new Intent(context, MainActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, 0,
                notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.defaults |= Notification.DEFAULT_ALL;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(Key.NOTIFICATION_ID, notification);
    }

    public static void removeNotification(Context context) {
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(Key.NOTIFICATION_ID);
    }

    class MyTimerTask extends TimerTask {
        Context ctx;

        public MyTimerTask(Context c) {
            this.ctx = c;
        }

        @Override
        public void run() {
            Log.d(TAG, "MyTimerTask: " + "Registration retrying...");
            GCMRegistrar.register(ctx, CommonUtilities.SENDER_ID);
        }
    }

    void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

}
